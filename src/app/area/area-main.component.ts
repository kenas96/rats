import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-area',
    template: '<router-outlet></router-outlet>'
})

export class AreaMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}