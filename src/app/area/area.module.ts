import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { AreaListComponent } from './list/area-list.component'
import { AreaDetailComponent } from './detail/area-detail.component'
import { AreaService } from './area.service'
import { AreaRoutingModule } from './area-routing.module'
import { AreaMainComponent } from './area-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        HttpModule,
        RouterModule,
        AreaRoutingModule
    ],
    exports: [],
    declarations: [
        AreaListComponent,
        AreaDetailComponent,
        AreaMainComponent
    ],
    providers: [
        AreaService,
        HttpService,
        AuthGuard,
        Alert
    ],
})
export class AreaModule { }
