import { Component, OnInit, Input } from '@angular/core'
import { Area } from './../area.model'

@Component({
    selector: 'area-detail',
    templateUrl: './area-detail.component.html',
    styleUrls: ['./area-detail.component.css']
})

export class AreaDetailComponent implements OnInit {
    constructor() {
    }

    @Input() id: string;
    @Input() area: Area;

    ngOnInit() {
    }
}