import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { AreaMainComponent } from './area-main.component'
import { AreaListComponent } from './list/area-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: AreaMainComponent,
        children: [
            {
                path: '',
                component: AreaListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: AreaListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class AreaRoutingModule { }
