import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class AreaService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('areas?' + query_string)
    }

    getDetail(id) {
        return this.service.get('areas/' + id)
    }
}