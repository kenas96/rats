import { Component, OnInit, Input } from '@angular/core'
import { Warehouse } from './../warehouse.model'

@Component({
    selector: 'warehouse-detail',
    templateUrl: './warehouse-detail.component.html',
    styleUrls: ['./warehouse-detail.component.css']
})

export class WarehouseDetailComponent implements OnInit {
    constructor(
    ) { }

    @Input() id: string
    @Input() warehouse: Warehouse

    ngOnInit() {
    }
}