import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class WarehouseService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('warehouses?' + query_string)
    }

    getDetail(id) {
        return this.service.get('warehouses/' + id)
    }

}