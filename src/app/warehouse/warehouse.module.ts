import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { WarehouseListComponent } from './list/warehouse-list.component'
import { WarehouseDetailComponent } from './detail/warehouse-detail.component'
import { WarehouseService } from './warehouse.service'
import { WarehouseRoutingModule } from './warehouse-routing.module'
import { WarehouseMainComponent } from './warehouse-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        HttpModule,
        RouterModule,
        WarehouseRoutingModule
    ],
    exports: [],
    declarations: [
        WarehouseListComponent,
        WarehouseDetailComponent,
        WarehouseMainComponent
    ],
    providers: [
        WarehouseService,
        HttpService,
        AuthGuard,
        Alert
    ],
})
export class WarehouseModule { }
