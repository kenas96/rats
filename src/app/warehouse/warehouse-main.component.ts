import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-warehouse',
    template: '<router-outlet></router-outlet>'
})

export class WarehouseMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}