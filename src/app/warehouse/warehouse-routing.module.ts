import { NgModule, Component } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { WarehouseMainComponent } from './warehouse-main.component'
import { WarehouseListComponent } from './list/warehouse-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: WarehouseMainComponent,
        children: [
            {
                path: '',
                component: WarehouseListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: WarehouseListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class WarehouseRoutingModule { }
