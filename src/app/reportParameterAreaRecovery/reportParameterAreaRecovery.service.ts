import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class ReportParameterAreaRecoveryService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('webuser/report/parameterarearecovery?' + query_string)
    }

    downloadReport(query_string) {
        return this.service.get('webuser/report/parameterarearecovery/generate?' + query_string)
    }

    downloadFullReport() {
        //maximum rows in excel
        return this.service.get('webuser/report/parameterarearecovery/generate?page_size=1048576')
    }

}