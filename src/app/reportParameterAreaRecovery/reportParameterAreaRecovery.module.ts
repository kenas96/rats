import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { ReportParameterAreaRecoveryListComponent } from './list/reportParameterAreaRecovery-list.component'
import { ReportParameterAreaRecoveryService } from './reportParameterAreaRecovery.service'
import { ReportParameterAreaRecoveryRoutingModule } from './reportParameterAreaRecovery-routing.module'
import { ReportParameterAreaRecoveryMainComponent } from './reportParameterAreaRecovery-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        HttpModule,
        RouterModule,
        ReportParameterAreaRecoveryRoutingModule
    ],
    exports: [],
    declarations: [
        ReportParameterAreaRecoveryListComponent,
        ReportParameterAreaRecoveryMainComponent
    ],
    providers: [
        ReportParameterAreaRecoveryService,
        HttpService,
        AuthGuard,
        Alert
    ],
})
export class ReportParameterAreaRecoveryModule { }
