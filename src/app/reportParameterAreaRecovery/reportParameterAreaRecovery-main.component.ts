import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'main-reportParameterAreaRecovery',
    template: '<router-outlet></router-outlet>'
})

export class ReportParameterAreaRecoveryMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}