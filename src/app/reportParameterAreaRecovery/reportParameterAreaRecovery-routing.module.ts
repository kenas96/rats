import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportParameterAreaRecoveryMainComponent } from './reportParameterAreaRecovery-main.component';
import { ReportParameterAreaRecoveryListComponent } from './list/reportParameterAreaRecovery-list.component';
import { AuthGuard } from '../pages/pages-guard.provider';


const routes: Routes = [
    {
        path: '',
        component: ReportParameterAreaRecoveryMainComponent,
        children: [
            {
                path: '',
                component: ReportParameterAreaRecoveryListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: ReportParameterAreaRecoveryListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class ReportParameterAreaRecoveryRoutingModule { }
