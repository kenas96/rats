import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-externalRecovery',
    template: '<router-outlet></router-outlet>'
})

export class ExternalRecoveryMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}