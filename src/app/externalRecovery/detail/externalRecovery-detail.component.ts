import { Component, OnInit, Input, ViewChild } from '@angular/core'
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms'
import { ExternalRecovery, ExternalRecoveryContent } from './../externalRecovery.model'
import { ExternalRecoveryService } from './../externalRecovery.service'
import { Alert } from '../../control/util'
import { Validator } from '../../control/validator'
import 'rxjs/add/operator/debounceTime'
import swal from 'sweetalert2'

@Component({
    selector: 'externalRecovery-detail',
    templateUrl: './externalRecovery-detail.component.html',
    styleUrls: ['./externalRecovery-detail.component.css']
})

export class ExternalRecoveryDetailComponent implements OnInit {
    constructor(
        private service: ExternalRecoveryService,
        private alert: Alert,
        private validator: Validator,
        private fb: FormBuilder
    ) { }

    @Input() id: string
    @Input() externalRecovery: ExternalRecovery
    @Input() state: number
    @Input() displayData: Function
    @Input() searchMOU: Function
    @Input() searchMOUAfterAdd: Function
    @Input() externalRecoveryForm: FormGroup

    @Input() columns = []
    @Input() listCriteria = []
    @Input() data = {
        rows: [],
        mous: []
    }
    @Input() dataSelected = {
        rows: []
    }
    @Input() option = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: null,
        disNext: null,
        order: { column: 'id', ascending: true }
    }

    stateContent: number = 1
    externalRecoveryContent: ExternalRecoveryContent
    externalRecoveryContentForm: FormGroup
    viewCallback: Function
    editCallback: Function
    deleteCallback: Function
    searchMOUCallback: Function
    searchMOUAfterAddCallback: Function

    @Input() permission = {}
    //@ViewChild('mouselect') public mouselect: any

    // public selectedMou(value: any): void {
    //     this.externalRecoveryForm.patchValue({
    //         search_mou: value.text
    //     })
    // }

    search() {
        this.dataSelected.rows = []
        let tmp_query_all = [
            { param: "page_size", value: 1000000 },
            { param: "page_offset", value: 0 },
            { param: "order_column", value: "id" },
            { param: "order_ascending", value: true }
        ]
        let que_str_all = tmp_query_all.map(function (el) {
            return el.param + '=' + el.value
        }).join('&')
        this.service.getMOU(que_str_all, this.externalRecoveryForm.get('search_mou').value).then(
            res => {
                for (var i = 0; i < res.data.external_recoveries.length; i++) {
                    if (res.data.external_recoveries[i].checked) {
                        this.dataSelected.rows.push(res.data.external_recoveries[i])
                    }
                }
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
            }
        )
        this.searchMOU()
        //this.mouselect.active = []
        this.externalRecoveryForm.patchValue({
            search_mou: ''
        })
    }

    ngOnInit() {
        this.externalRecoveryContent = new ExternalRecoveryContent()
        this.viewCallback = this.view.bind(this)
        this.editCallback = this.edit.bind(this)
        this.deleteCallback = this.remove.bind(this)
        this.searchMOUCallback = this.searchMOU.bind(this)
        this.searchMOUAfterAddCallback = this.searchMOUAfterAdd.bind(this)
        this.externalRecoveryContentForm = this.fb.group({
            request_id: '',
            id: '',
            mou_id: '',
            name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
            dob: ['', [Validators.required, Validators.pattern('^\\d{4}-\\d{2}-\\d{2}$')]],
            ktp: ['', [Validators.required, Validators.minLength(16), Validators.maxLength(17), this.validator.isNumber]],
            email: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')]],
            handphone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(20), this.validator.isNumber]],
            surat_penunjukan: ['', [Validators.required, Validators.maxLength(50)]],
            tanggal_penunjukan: ['', [Validators.required, Validators.pattern('^\\d{4}-\\d{2}-\\d{2}$')]],
            mro: ['', [Validators.required, Validators.maxLength(50)]],
            is_active: ''
        })
    }

    focusField() {
        setTimeout(() => {
            $("#contentname").focus();
        }, 500)
    }

    populateData() {
        var stringActive: string
        if (this.externalRecoveryContent.is_active === true) {
            stringActive = "active"
        } else {
            stringActive = "notactive"
        }

        this.externalRecoveryContentForm.patchValue({
            request_id: this.externalRecoveryContent.request_id,
            id: this.externalRecoveryContent.id,
            mou_id: this.externalRecoveryContent.mou_id,
            name: this.externalRecoveryContent.name,
            dob: this.externalRecoveryContent.dob,
            ktp: this.externalRecoveryContent.ktp,
            email: this.externalRecoveryContent.email,
            handphone: this.externalRecoveryContent.handphone,
            surat_penunjukan: this.externalRecoveryContent.surat_penunjukan,
            tanggal_penunjukan: this.externalRecoveryContent.tanggal_penunjukan,
            mro: this.externalRecoveryContent.mro,
            is_active: stringActive
        })
    }

    addNew() {
        this.stateContent = 1
        this.externalRecoveryContent = new ExternalRecoveryContent()
        this.externalRecoveryContentForm.reset()
        this.focusField()
        this.externalRecoveryContent.mou_id = this.externalRecovery.id
        this.externalRecoveryContentForm.patchValue({
            mou_id: this.externalRecoveryContent.mou_id
        })
        $('#modal-contentDetail').modal()
    }

    view(obj) {
        this.stateContent = 0;
        this.externalRecoveryContent = obj
        this.externalRecoveryContentForm.reset()
        this.populateData()
        $('#modal-contentDetail').modal()
    }

    edit(obj) {
        this.stateContent = -1
        this.externalRecoveryContent = JSON.parse(JSON.stringify(obj))
        this.externalRecoveryContentForm.reset()
        this.focusField()
        this.populateData()
        $('#modal-contentDetail').modal()
    }

    remove(obj) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.alert.loading_start()
                this.service.delete(obj.id).then(
                    res => {
                        this.alert.success_alert('Your data has been removed!', '', this.searchMOU())
                    },
                    err => {
                        if (err.http_status === 422) {
                            this.alert.error_alert(err.message)
                        } else {
                            this.alert.warn_alert(err.message)
                        }
                    }
                );
            }
        })

    }

    converParseAbleFormateDate(formatDate) {
        let d = formatDate.split("/");
        if (d[0].length == 1) d[0] = "0" + d[0];
        if (d[1].length == 1) d[1] = "0" + d[1];
        let y = d.splice(-1)[0];
        d.splice(0, 0, y);
        let date = d.join("-");
        return date;
    }

    save() {
        swal({
            title: 'Are you sure?',
            text: "Your data will change permanently!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.alert.loading_start()
                let obj = Object.assign({}, this.externalRecovery, this.externalRecoveryForm.value)
                obj.search_mou = undefined
                obj.start_date = this.converParseAbleFormateDate(obj.start_date);
                obj.end_date = this.converParseAbleFormateDate(obj.end_date);
                obj.request_id = Date.now()
                obj.external_recoveries = this.dataSelected.rows
                this.service.submitMOU(obj, obj.id).then(
                    res => {
                        this.alert.success_alert('Your data has been added', this.id, this.displayData())
                    },
                    err => {
                        if (err.http_status === 422) {
                            this.alert.error_alert(err.message)
                        } else {
                            this.alert.warn_alert(err.message)
                        }
                    }
                );
            }
        })
    }
}
