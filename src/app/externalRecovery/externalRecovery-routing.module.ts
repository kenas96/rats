import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ExternalRecoveryMainComponent } from './externalRecovery-main.component'
import { ExternalRecoveryListComponent } from './list/externalRecovery-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: ExternalRecoveryMainComponent,
        children: [
            {
                path: '',
                component: ExternalRecoveryListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: ExternalRecoveryListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class ExternalRecoveryRoutingModule { }
