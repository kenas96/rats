import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { ExternalRecoveryListComponent } from './list/externalRecovery-list.component'
import { ExternalRecoveryDetailComponent } from './detail/externalRecovery-detail.component'
import { ExternalRecoveryContentDetailComponent } from './contentDetail/externalRecovery-contentDetail.component'
import { ExternalRecoveryService } from './externalRecovery.service'
import { ExternalRecoveryRoutingModule } from './externalRecovery-routing.module'
import { ExternalRecoveryMainComponent } from './externalRecovery-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Validator } from '../control/validator'
import { Alert } from '../control/util'
import { SelectModule } from 'ng2-select'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule,
        ExternalRecoveryRoutingModule,
        SelectModule
    ],
    exports: [],
    declarations: [
        ExternalRecoveryListComponent,
        ExternalRecoveryDetailComponent,
        ExternalRecoveryContentDetailComponent,
        ExternalRecoveryMainComponent
    ],
    providers: [
        ExternalRecoveryService,
        HttpService,
        AuthGuard,
        Validator,
        Alert
    ],
})
export class ExternalRecoveryModule { }
