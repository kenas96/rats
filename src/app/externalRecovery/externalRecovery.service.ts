import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class ExternalRecoveryService {

    constructor(private service: HttpService) { }


    getPaging(query_string) {
        return this.service.get('webuser/mou/externalrecoveries?' + query_string)
    }

    getMOU(query_string, id) {
        return this.service.get('webuser/mou/' + id + '/externalrecoveries?' + query_string)
    }

    submitMOU(data, id) {
        return this.service.post('webuser/mou/' + id + '/externalrecoveries', data)
    }

    insertExtenalRecovery(data) {
        return this.service.post('webuser/externalrecoveries', data)
    }

    updateExternalRecovery(data, id) {
        return this.service.put('webuser/externalrecoveries/' + id, data)
    }

    getActiveMOU() {
        return this.service.get('mous?is_active=true')
    }

    delete(id) {
        return this.service.delete('webuser/externalrecoveries/' + id)
    }


}