export class ExternalRecovery {
    request_id: number
    id: string
    internal_recovery_id: number
    name: string
    start_date: any
    end_date: any
    pic_name: string
    pic_position: string
    pic_handphone: string
    category: string
    is_active: boolean

    constructor(data: any = {}) {
        this.request_id = data.request_id
        this.id = data.id
        this.internal_recovery_id = data.internal_recovery_id
        this.name = data.name
        this.start_date = data.start_date
        this.end_date = data.end_date
        this.pic_name = data.pic_name
        this.pic_position = data.pic_position
        this.pic_handphone = data.pic_handphone
        this.category = data.category
        this.is_active = data.is_active
    }
}

export class ExternalRecoveryContent {
    request_id: number
    id: string
    mou_id: string
    name: string
    dob: string
    ktp: string
    email: string
    handphone: string
    surat_penunjukan: string
    tanggal_penunjukan: string
    mro: string
    is_active: boolean
    is_shadow: boolean

    constructor(data: any = {}) {
        this.request_id = data.request_id
        this.id = data.id
        this.mou_id = data.mou_id
        this.name = data.name
        this.dob = data.dob
        this.ktp = data.ktp
        this.email = data.email
        this.handphone = data.handphone
        this.surat_penunjukan = data.surat_penunjukan
        this.tanggal_penunjukan = data.tanggal_penunjukan
        this.mro = data.mro
        this.is_active = data.is_active
        this.is_shadow = data.is_shadow
    }
}