import { Component, OnInit, Input } from '@angular/core'
import { FormGroup, AbstractControl } from '@angular/forms'
import { ExternalRecoveryContent } from './../externalRecovery.model'
import { ExternalRecoveryService } from './../externalRecovery.service'
import { Alert } from '../../control/util'
import 'rxjs/add/operator/debounceTime'

@Component({
    selector: 'externalRecovery-contentDetail',
    templateUrl: './externalRecovery-contentDetail.component.html',
    styleUrls: ['./externalRecovery-contentDetail.component.css']
})

export class ExternalRecoveryContentDetailComponent implements OnInit {
    constructor(
        private service: ExternalRecoveryService,
        private alert: Alert,
    ) { }

    @Input() id: string
    @Input() externalRecoveryContent: ExternalRecoveryContent
    @Input() stateContent: number
    @Input() displayData: Function
    @Input() displayDataAfterAdd: Function
    @Input() externalRecoveryContentForm: FormGroup


    nameMessage: string
    handphoneMessage: string
    ktpMessage: string
    emailMessage: string
    dobMessage: string
    suratMessage: string
    tanggalMessage: string
    mroMessage: string

    private validationMessage = {
        required: "Please fill this field.",
        minlength: "Min. character allowed are 3 characters.",
        maxlength: "Max. character allowed are 50 characters.",
        string: "Please enter a valid character format.",
        pattern: "Please enter a valid email address."
    }

    private validationMessageHandphone = {
        required: "Please fill this field.",
        minlength: "Min. character allowed are 10 characters.",
        maxlength: "Max. character allowed are 20 characters.",
        number: "Please enter a valid number format."
    }

    private validationMessageKtp = {
        required: "Please fill this field.",
        minlength: "Min. character allowed are 16 characters.",
        maxlength: "Max. character allowed are 17 characters.",
        number: "Please enter a valid number format."
    }

    private validationMessageDate = {
        required: "Please fill this field.",
        pattern: "Please enter a valid date format."
    }

    ngOnInit() {
        const nameControl = this.externalRecoveryContentForm.get('name')
        nameControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageName(nameControl)
        )

        const handphoneControl = this.externalRecoveryContentForm.get('handphone');
        handphoneControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageHandphone(handphoneControl)
        )

        const ktpControl = this.externalRecoveryContentForm.get('ktp');
        ktpControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageKtp(ktpControl)
        )

        const emailControl = this.externalRecoveryContentForm.get('email');
        emailControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageEmail(emailControl)
        )

        const dobControl = this.externalRecoveryContentForm.get('dob');
        dobControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageDob(dobControl)
        )

        const suratControl = this.externalRecoveryContentForm.get('surat_penunjukan');
        suratControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageSurat(suratControl)
        )

        const tanggalControl = this.externalRecoveryContentForm.get('tanggal_penunjukan');
        tanggalControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageTanggal(tanggalControl)
        )

        const mroControl = this.externalRecoveryContentForm.get('mro');
        mroControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageMro(mroControl)
        )
    }

    setMessageName(c: AbstractControl): void {
        this.nameMessage = ''
        if (this.stateContent == 0) {
            this.externalRecoveryContentForm.get('name').disable()
        } else {
            this.externalRecoveryContentForm.get('name').enable()
            if ((c.touched || c.dirty) && c.errors) {
                this.nameMessage = Object.keys(c.errors).map(key =>
                    this.validationMessage[key]
                )[0]
            }
        }
    }

    setMessageHandphone(c: AbstractControl): void {
        this.handphoneMessage = ''
        if (this.stateContent == 0) {
            this.externalRecoveryContentForm.get('handphone').disable()
        } else {
            this.externalRecoveryContentForm.get('handphone').enable()
            if ((c.touched || c.dirty) && c.errors) {
                this.handphoneMessage = Object.keys(c.errors).map(key =>
                    this.validationMessageHandphone[key]
                )[0]
            }
        }
    }

    setMessageKtp(c: AbstractControl): void {
        this.ktpMessage = ''
        if (this.stateContent == 0) {
            this.externalRecoveryContentForm.get('ktp').disable()
        } else {
            this.externalRecoveryContentForm.get('ktp').enable()
            if ((c.touched || c.dirty) && c.errors) {
                this.ktpMessage = Object.keys(c.errors).map(key =>
                    this.validationMessageKtp[key]
                )[0]
            }
        }
    }

    setMessageEmail(c: AbstractControl): void {
        this.emailMessage = ''
        if (this.stateContent == 0) {
            this.externalRecoveryContentForm.get('email').disable()
        } else {
            this.externalRecoveryContentForm.get('email').enable()
            if ((c.touched || c.dirty) && c.errors) {
                this.emailMessage = Object.keys(c.errors).map(key =>
                    this.validationMessage[key]
                )[0]
            }
        }
    }

    setMessageDob(c: AbstractControl): void {
        this.dobMessage = ''
        if (this.stateContent == 0) {
            this.externalRecoveryContentForm.get('dob').disable()
        } else {
            this.externalRecoveryContentForm.get('dob').enable()
            if ((c.touched || c.dirty) && c.errors) {
                this.dobMessage = Object.keys(c.errors).map(key =>
                    this.validationMessageDate[key]
                )[0]
            }
        }
    }

    setMessageSurat(c: AbstractControl): void {
        this.suratMessage = ''
        if (this.stateContent == 0) {
            this.externalRecoveryContentForm.get('surat_penunjukan').disable()
        } else {
            this.externalRecoveryContentForm.get('surat_penunjukan').enable()
            if ((c.touched || c.dirty) && c.errors) {
                this.suratMessage = Object.keys(c.errors).map(key =>
                    this.validationMessage[key]
                )[0]
            }
        }
    }

    setMessageTanggal(c: AbstractControl): void {
        this.tanggalMessage = ''
        if (this.stateContent == 0) {
            this.externalRecoveryContentForm.get('tanggal_penunjukan').disable()
        } else {
            this.externalRecoveryContentForm.get('tanggal_penunjukan').enable()
            if ((c.touched || c.dirty) && c.errors) {
                this.tanggalMessage = Object.keys(c.errors).map(key =>
                    this.validationMessageDate[key]
                )[0]
            }
        }
    }

    setMessageMro(c: AbstractControl): void {
        this.mroMessage = ''
        if (this.stateContent == 0) {
            this.externalRecoveryContentForm.get('mro').disable()
        } else {
            this.externalRecoveryContentForm.get('mro').enable()
            if ((c.touched || c.dirty) && c.errors) {
                this.mroMessage = Object.keys(c.errors).map(key =>
                    this.validationMessage[key]
                )[0]
            }
        }
    }

    compareDate(date) {
        var day = date[8] + date[9]
        var month = date[5] + date[6]
        month = +month - 1
        var year = date[0] + date[1] + date[2] + date[3]

        var today = new Date()
        var new_date = new Date(+year, month, +day)

        var today_time = today.getTime() / 1000
        var new_date_time = new_date.getTime() / 1000

        if (today_time - new_date_time < 86400) {
            return false
        } else {
            return true
        }
    }

    save() {
        this.alert.loading_start()
        let obj = Object.assign({}, this.externalRecoveryContent, this.externalRecoveryContentForm.value)
        obj.is_active_status = undefined
        obj.request_id = Date.now()
        if (obj.is_active == "notactive") {
            obj.is_active = false
        } else {
            obj.is_active = true
        }
        if (this.compareDate(obj.dob)) {
            // if (this.compareDate(obj.tanggal_penunjukan)) {
            //console.log(obj)
            if (this.stateContent == 1) {
                //add
                obj.id = undefined
                obj.is_shadow = false
                this.service.insertExtenalRecovery(obj).then(
                    res => {
                        this.alert.success_alert('Your data has been added', this.id, this.displayDataAfterAdd())
                    },
                    err => {
                        if (err.http_status === 422) {
                            this.alert.error_alert(err.message)
                        } else {
                            this.alert.warn_alert(err.message)
                        }
                    }
                );
            }
            else {
                //edit
                this.service.updateExternalRecovery(obj, obj.id).then(
                    res => {
                        this.alert.success_alert('Your data has been saved', this.id, this.displayData())
                    },
                    err => {
                        if (err.http_status === 422) {
                            this.alert.error_alert(err.message)
                        } else {
                            this.alert.warn_alert(err.message)
                        }
                    }
                )
            }
            // } else {
            //     this.alert.error_alert("Please input tanggal penunjukan before this day!")
            // }
        } else {
            this.alert.error_alert("Please input DOB before this day!")
        }


    }
}
