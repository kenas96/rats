import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { ExternalRecoveryService } from '../externalRecovery.service'
import { ExternalRecovery } from '../externalRecovery.model'
import { Alert } from '../../control/util'

@Component({
    selector: 'externalRecovery-list',
    templateUrl: './externalRecovery-list.component.html',
    styleUrls: ['./externalRecovery-list.component.css']
})

export class ExternalRecoveryListComponent implements OnInit {
    constructor(
        private service: ExternalRecoveryService,
        private fb: FormBuilder,
        private alert: Alert
    ) { }

    adminType: string = ''
    state: number = 1
    externalRecovery: ExternalRecovery
    externalRecoveryForm: FormGroup
    lengthData: number = 0
    lengthDataInnerTable: number = 0
    displayCallback: Function
    searchMOUCallback: Function
    searchMOUAfterAddCallback: Function
    viewCallback: Function
    editCallback: Function
    deleteCallback: Function

    data = {
        rows: []
    }

    dataInnerTable = {
        rows: [],
        mous: [
            {
                id: '',
                text: ''
            }
        ],
    }

    dataSelected = {
        rows: []
    }

    option: any = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: false,
        disNext: false,
        order: { column: 'id', ascending: true }
    }

    optionInnerTable: any = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: false,
        disNext: false,
        order: { column: 'id', ascending: true }
    }

    columns = [
        { text: 'MOU NO', field: 'id', value: 'id' },
        { text: 'MOU Name', field: 'name', value: 'name' },
        { text: 'Category', field: 'category', value: 'category' },
        { text: 'PIC Name', field: 'pic_name', value: 'pic_name' },
        { text: 'Start Date', field: 'start_date', value: 'start_date' },
        { text: 'End Date', field: 'end_date', value: 'end_date' }
    ]

    columnsInnerTable = [
        { text: 'Name', field: 'name', value: 'name' },
        { text: 'KTP', field: 'ktp', value: 'ktp' },
        { text: 'Email', field: 'email', value: 'email' },
        { text: 'Handphone', field: 'handphone', value: 'handphone' },
        { text: 'MRO', field: 'mro', value: 'mro' },
        { text: 'Active Status', field: 'is_active_string', value: 'is_active' }
    ]

    listCriteria = [
        { text: 'MOU Name', value: 'name' },
        { text: 'Category', value: 'category' },
        { text: 'PIC Name', value: 'pic_name' }
    ]

    listCriteriaInnerTable = [
        { text: 'Name', value: 'name' },
        { text: 'KTP', value: 'ktp' },
        { text: 'Handphone', value: 'handphone' }
    ]

    permission: any = {
        create: true,
        view: true,
        update: false,
        delete: false,
        upload: false
    }

    permissionInnerTable: any = {
        create: true,
        view: true,
        update: true,
        delete: true,
        upload: false
    }

    CheckBtnPrevNext() {
        if (this.option.page === 1) {
            this.option.disPrev = true
        } else {
            this.option.disPrev = false;
        }
        if ((this.option.page === 1 || this.option.page > 1) && this.lengthData <= this.option.pageSize) {
            this.option.disNext = true;
        } else {
            this.option.disNext = false;
        }
    }

    CheckBtnPrevNextInnerTable() {
        if (this.optionInnerTable.page === 1) {
            this.optionInnerTable.disPrev = true
        } else {
            this.optionInnerTable.disPrev = false;
        }
        if ((this.optionInnerTable.page === 1 || this.optionInnerTable.page > 1) &&
            this.lengthDataInnerTable <= this.optionInnerTable.pageSize) {
            this.optionInnerTable.disNext = true;
        } else {
            this.optionInnerTable.disNext = false;
        }
    }

    ngOnInit() {
        this.externalRecovery = new ExternalRecovery()
        this.displayCallback = this.displayData.bind(this)
        this.searchMOUCallback = this.searchMOU.bind(this)
        this.searchMOUAfterAddCallback = this.searchMOUAfterAdd.bind(this)
        this.viewCallback = this.view.bind(this)
        this.editCallback = this.edit.bind(this)
        this.deleteCallback = this.remove.bind(this)
        this.externalRecoveryForm = this.fb.group({
            request_id: '',
            id: ['', [Validators.required]],
            internal_recovery_id: '',
            name: [{ value: '', disabled: true }],
            start_date: [{ value: '', disabled: true }],
            end_date: [{ value: '', disabled: true }],
            pic_name: [{ value: '', disabled: true }],
            pic_position: [{ value: '', disabled: true }],
            pic_handphone: [{ value: '', disabled: true }],
            category: [{ value: '', disabled: true }],
            is_active: '',
            search_mou: ''
        })
        //admin permission validation
        let user = localStorage.getItem('auth-user')
        let decoded = atob(user)
        for (var i = 0; i < decoded.length; i++) {
            if (decoded[i] === ':') {
                this.adminType = decoded.substr(0, i)
                break
            }
        }
        if (this.adminType !== 'Super Admin') {
            //this.permission.delete = false
            this.permissionInnerTable.delete = false
        }

        //additional permission
        if (this.adminType === 'Reco Admin' || this.adminType === 'Warehouse Admin') {
            this.permission.create = false
        }
    }

    ngAfterViewInit() {
        this.displayData()
    }

    loadingAnimationGridStart() {
        document.getElementById('dataGrid').style.display = "none"
        document.getElementById('loadingGrid').style.display = "block"
    }

    loadingAnimationGridStop() {
        document.getElementById('dataGrid').style.display = "block"
        document.getElementById('loadingGrid').style.display = "none"
    }

    parseData(data) {
        for (let entry of data) {
            if (entry['start_date'] !== null) {
                entry['start_date'] = new Date(entry['start_date']).toLocaleDateString()
            }
            if (entry['end_date'] !== null) {
                entry['end_date'] = new Date(entry['end_date']).toLocaleDateString()
            }
        }
    }

    parseDataInnerTable(data) {
        for (let entry of data) {
            entry['dob'] = new Date(entry['dob'] + 25200000)
            //entry['dob'].setDate(entry['dob'].getDate() + 1);
            //console.log(entry['dob'].toISOString())
            entry['dob'] = entry['dob'].toISOString().substr(0, 10);

            entry['tanggal_penunjukan'] = new Date(entry['tanggal_penunjukan'] + 25200000)
            // entry['tanggal_penunjukan'].setDate(entry['tanggal_penunjukan'].getDate() + 1);
            entry['tanggal_penunjukan'] = entry['tanggal_penunjukan'].toISOString().substr(0, 10)
            if (entry['is_active']) {
                entry['is_active_string'] = "Active"
            } else {
                entry['is_active_string'] = "Not Active"
            }

            if (entry['is_shadow']) {
                entry['noEdit'] = true
                entry['noDelete'] = true
            }
        }

        setTimeout(() => {
            for (var i = 0; i < this.dataSelected.rows.length; i++) {
                $('#checkbox' + this.dataSelected.rows[i].id).prop('checked', true);
            }
        }, 500)
    }

    parseDataInnerTableAfterAdd(data) {
        for (let entry of data) {
            entry['dob'] = new Date(entry['dob'] + 25200000)
            // entry['dob'].setDate(entry['dob'].getDate() + 1);
            entry['dob'] = entry['dob'].toISOString().substr(0, 10);

            entry['tanggal_penunjukan'] = new Date(entry['tanggal_penunjukan'] + 25200000)
            //entry['tanggal_penunjukan'].setDate(entry['tanggal_penunjukan'].getDate() + 1);
            entry['tanggal_penunjukan'] = entry['tanggal_penunjukan'].toISOString().substr(0, 10)
            if (entry['is_active']) {
                entry['is_active_string'] = "Active"
            } else {
                entry['is_active_string'] = "Not Active"
            }

            if (entry['is_shadow']) {
                entry['noEdit'] = true
                entry['noDelete'] = true
            }
        }

        setTimeout(() => {
            for (var i = 0; i < this.dataSelected.rows.length; i++) {
                $('#checkbox' + this.dataSelected.rows[i].id).prop('checked', true);
            }
        }, 500)
    }

    displayData() {
        this.loadingAnimationGridStart()
        let tmp_query = [
            { param: "page_size", value: parseInt(this.option.pageSize) + 1 },
            { param: "page_offset", value: parseInt(this.option.pageOffset) },
            { param: "order_column", value: this.option.order.column },
            { param: "order_ascending", value: this.option.order.ascending }
        ];
        if (Object.keys(this.option.criteria).length > 0) {
            tmp_query.push({ param: this.option.criteria.text, value: this.option.criteria.value });
        }
        let que_str = tmp_query.map(function (el) {
            return el.param + '=' + el.value;
        }).join('&');
        this.service.getPaging(que_str).then(
            res => {
                if (Array.isArray(res.data)) {
                    this.lengthData = res.data.length;
                    this.data.rows = res.data;
                    if (this.lengthData > this.option.pageSize) {
                        let idx = this.lengthData - 1;
                        this.data.rows.splice(idx, 1);
                    }
                } else {
                    this.data.rows = [res.data];
                }
                this.CheckBtnPrevNext();
                this.parseData(this.data.rows)
                this.loadingAnimationGridStop()
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
                this.loadingAnimationGridStop()
            }
        )
        this.service.getActiveMOU().then(
            res => {
                if (Array.isArray(res.data)) {
                    this.dataInnerTable.mous = []
                    for (let mou of res.data) {
                        mou['text'] = mou['id']
                        this.dataInnerTable.mous.push(mou)
                    }
                } else {
                    this.dataInnerTable.mous = [res.data]
                }
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
            }
        )
    }

    getDetail() {
        this.alert.loading_start()
        let tmp_query = [
            { param: "page_size", value: parseInt(this.optionInnerTable.pageSize) + 1 },
            { param: "page_offset", value: parseInt(this.optionInnerTable.pageOffset) },
            { param: "order_column", value: this.optionInnerTable.order.column },
            { param: "order_ascending", value: this.optionInnerTable.order.ascending }
        ];

        if (Object.keys(this.optionInnerTable.criteria).length > 0) {
            tmp_query.push({ param: this.optionInnerTable.criteria.text, value: this.optionInnerTable.criteria.value });
        }
        let que_str = tmp_query.map(function (el) {
            return el.param + '=' + el.value
        }).join('&')
        this.service.getMOU(que_str, this.externalRecovery.id).then(
            res => {
                if (Array.isArray(res.data.external_recoveries)) {
                    this.lengthDataInnerTable = res.data.external_recoveries.length
                    this.dataInnerTable.rows = res.data.external_recoveries
                    if (this.lengthDataInnerTable > this.optionInnerTable.pageSize) {
                        let idx = this.lengthDataInnerTable - 1
                        this.dataInnerTable.rows.splice(idx, 1)
                    }

                    //some additional func
                    this.externalRecovery.pic_handphone = res.data.pic_handphone
                    this.externalRecoveryForm.patchValue({
                        pic_handphone: this.externalRecovery.pic_handphone
                    })

                } else {
                    this.dataInnerTable.rows = [res.data]
                }
                this.CheckBtnPrevNextInnerTable()
                this.parseDataInnerTable(this.dataInnerTable.rows)
                this.alert.loading_stop()
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
            }
        )
    }

    searchMOU() {
        this.alert.loading_start()
        if (this.state === 0) {
            //view custom
            this.dataInnerTable.rows = []
            let tmp_query_all = [
                { param: "page_size", value: 1000000 },
                { param: "page_offset", value: 0 },
                { param: "order_column", value: this.optionInnerTable.order.column },
                { param: "order_ascending", value: this.optionInnerTable.order.ascending }
            ]
            if (Object.keys(this.optionInnerTable.criteria).length > 0) {
                tmp_query_all.push({ param: this.optionInnerTable.criteria.text, value: this.optionInnerTable.criteria.value });
            }
            let que_str_all = tmp_query_all.map(function (el) {
                return el.param + '=' + el.value
            }).join('&')
            this.service.getMOU(que_str_all, this.externalRecovery.id).then(
                res => {
                    for (var i = 0; i < res.data.external_recoveries.length; i++) {
                        if (res.data.external_recoveries[i].checked) {
                            this.dataInnerTable.rows.push(res.data.external_recoveries[i])
                        }
                    }
                    this.alert.loading_stop()
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                }
            )
        } else {
            let tmp_query = [
                { param: "page_size", value: parseInt(this.optionInnerTable.pageSize) + 1 },
                { param: "page_offset", value: parseInt(this.optionInnerTable.pageOffset) },
                { param: "order_column", value: this.optionInnerTable.order.column },
                { param: "order_ascending", value: this.optionInnerTable.order.ascending }
            ]
            var id = this.externalRecoveryForm.get('search_mou').value
            var select: any = this.dataInnerTable.mous.find(x => x.id.toLowerCase() == id.toLowerCase())
            if (select === undefined) {

            } else {
                if (select.category.toLowerCase() === 'shadow') {
                    tmp_query.push({ param: "is_shadow", value: true })
                } else {
                    tmp_query.push({ param: "is_shadow", value: false })
                }

                if (Object.keys(this.optionInnerTable.criteria).length > 0) {
                    tmp_query.push({ param: this.optionInnerTable.criteria.text, value: this.optionInnerTable.criteria.value });
                }
                let que_str = tmp_query.map(function (el) {
                    return el.param + '=' + el.value
                }).join('&')

                this.service.getMOU(que_str, this.externalRecoveryForm.get('search_mou').value).then(
                    res => {
                        if (Array.isArray(res.data.external_recoveries)) {
                            this.externalRecovery = res.data
                            if (this.externalRecovery.start_date !== null) {
                                this.externalRecovery.start_date = new Date(this.externalRecovery.start_date - 25200000).toLocaleDateString()
                            }
                            if (this.externalRecovery.end_date !== null) {
                                this.externalRecovery.end_date = new Date(this.externalRecovery.end_date - 25200000).toLocaleDateString()
                            }
                            this.populateData()
                            this.lengthDataInnerTable = res.data.external_recoveries.length
                            this.dataInnerTable.rows = res.data.external_recoveries

                            if (this.lengthDataInnerTable > this.optionInnerTable.pageSize) {
                                let idx = this.lengthDataInnerTable - 1
                                this.dataInnerTable.rows.splice(idx, 1)
                            }
                        } else {
                            this.dataInnerTable.rows = [res.data];
                        }
                        this.CheckBtnPrevNextInnerTable()
                        this.parseDataInnerTable(this.dataInnerTable.rows)
                        this.alert.loading_stop()
                    },
                    err => {
                        if (err.http_status === 422) {
                            this.alert.error_alert(err.message)
                        } else {
                            this.alert.warn_alert(err.message)
                        }
                    }
                )
            }
        }
    }

    searchMOUAfterAdd() {
        this.alert.loading_start()
        let tmp_query = [
            { param: "page_size", value: parseInt(this.optionInnerTable.pageSize) + 1 },
            { param: "page_offset", value: parseInt(this.optionInnerTable.pageOffset) },
            { param: "order_column", value: this.optionInnerTable.order.column },
            { param: "order_ascending", value: this.optionInnerTable.order.ascending }
        ]
        var id = this.externalRecoveryForm.get('search_mou').value
        var select: any = this.dataInnerTable.mous.find(x => x.id.toLowerCase() == id.toLowerCase())
        if (select === undefined) {

        } else {
            if (select.category.toLowerCase() === 'shadow') {
                tmp_query.push({ param: "is_shadow", value: true })
            } else {
                tmp_query.push({ param: "is_shadow", value: false })
            }

            if (Object.keys(this.optionInnerTable.criteria).length > 0) {
                tmp_query.push({ param: this.optionInnerTable.criteria.text, value: this.optionInnerTable.criteria.value });
            }
            let que_str = tmp_query.map(function (el) {
                return el.param + '=' + el.value
            }).join('&')
            this.service.getMOU(que_str, this.externalRecoveryForm.get('search_mou').value).then(
                res => {
                    if (Array.isArray(res.data.external_recoveries)) {
                        this.externalRecovery = res.data
                        if (this.externalRecovery.start_date !== null) {
                            this.externalRecovery.start_date = new Date(this.externalRecovery.start_date - 25200000).toLocaleDateString()
                        }
                        if (this.externalRecovery.end_date !== null) {
                            this.externalRecovery.end_date = new Date(this.externalRecovery.end_date - 25200000).toLocaleDateString()
                        }
                        this.populateData()
                        this.lengthDataInnerTable = res.data.external_recoveries.length
                        this.dataInnerTable.rows = res.data.external_recoveries

                        if (this.lengthDataInnerTable > this.optionInnerTable.pageSize) {
                            let idx = this.lengthDataInnerTable - 1
                            this.dataInnerTable.rows.splice(idx, 1)
                        }
                    } else {
                        this.dataInnerTable.rows = [res.data];
                    }
                    this.CheckBtnPrevNextInnerTable()
                    this.parseDataInnerTableAfterAdd(this.dataInnerTable.rows)
                    this.alert.loading_stop()
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                }
            )
        }
    }

    focusField() {
        setTimeout(() => {
            $("#searchmou").focus();
        }, 500)
    }

    populateData() {
        this.externalRecoveryForm.patchValue({
            request_id: this.externalRecovery.request_id,
            id: this.externalRecovery.id,
            internal_recovery_id: this.externalRecovery.internal_recovery_id,
            name: this.externalRecovery.name,
            start_date: this.externalRecovery.start_date,
            end_date: this.externalRecovery.end_date,
            pic_name: this.externalRecovery.pic_name,
            pic_position: this.externalRecovery.pic_position,
            pic_handphone: this.externalRecovery.pic_handphone,
            category: this.externalRecovery.category,
            is_active: this.externalRecovery.is_active,
            search_mou: this.externalRecovery.id
        })
    }

    resetOption() {
        this.optionInnerTable.page = 1
        this.optionInnerTable.pageOffset = 0
        this.optionInnerTable.pageSize = 10
        this.optionInnerTable.criteria = {}
        this.optionInnerTable.disPrev = false
        this.optionInnerTable.disNext = false
        this.optionInnerTable.order = { column: 'id', ascending: true }

        this.externalRecoveryForm.reset()
        this.dataInnerTable.rows = []
        this.dataSelected.rows = []
        this.optionInnerTable.criteria.value = ''
        $('#search_in').val("")
    }

    addNew() {
        this.resetOption()
        this.state = 1
        this.externalRecovery = new ExternalRecovery()
        this.focusField()
        $('#modal-detail').modal()
        $('input:checkbox').prop('checked', false)
    }

    view(obj) {
        this.resetOption()
        this.state = 0
        this.externalRecovery = obj
        this.externalRecoveryForm.patchValue({
            search_mou: ''
        })
        let tmp_query_all = [
            { param: "page_size", value: 1000000 },
            { param: "page_offset", value: 0 },
            { param: "order_column", value: "id" },
            { param: "order_ascending", value: true }
        ]
        let que_str_all = tmp_query_all.map(function (el) {
            return el.param + '=' + el.value
        }).join('&')

        this.alert.loading_start()
        this.service.getMOU(que_str_all, this.externalRecovery.id).then(
            res => {
                //some additional func
                this.externalRecovery.pic_handphone = res.data.pic_handphone
                this.externalRecoveryForm.patchValue({
                    pic_handphone: this.externalRecovery.pic_handphone
                })
                this.populateData()
                for (var i = 0; i < res.data.external_recoveries.length; i++) {
                    if (res.data.external_recoveries[i].checked) {
                        this.dataInnerTable.rows.push(res.data.external_recoveries[i])
                    }
                }
                this.parseDataInnerTable(this.dataInnerTable.rows)
                this.alert.loading_stop()
                $('#modal-detail').modal()
                $('input:checkbox').prop('checked', false)

            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
            }
        )
    }

    edit(obj) {

    }

    remove(obj) {

    }
}
