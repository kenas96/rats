import { Component, OnInit, Input, ViewChild } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { ParamAreaRecovery, ParamAreaRecoveryContent } from './../paramAreaRecovery.model'
import { ParamAreaRecoveryService } from './../paramAreaRecovery.service'
import { Alert } from '../../control/util'
import 'rxjs/add/operator/debounceTime'
import swal from 'sweetalert2'

@Component({
    selector: 'paramAreaRecovery-detail',
    templateUrl: './paramAreaRecovery-detail.component.html',
    styleUrls: ['./paramAreaRecovery-detail.component.css']
})

export class ParamAreaRecoveryDetailComponent implements OnInit {
    constructor(
        private service: ParamAreaRecoveryService,
        private alert: Alert
    ) { }

    @Input() id: string
    @Input() paramAreaRecovery: ParamAreaRecovery
    @Input() state: number
    @Input() displayData: Function
    @Input() getDetail: Function
    @Input() paramAreaRecoveryForm: FormGroup

    @Input() columns = []
    @Input() listCriteria = []
    @Input() data = {
        rows: [],
        areas: [],
        area_recoveries: [],
    }
    @Input() dataSelected = {
        rows: []
    }
    @Input() option = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: null,
        disNext: null,
        order: { column: 'id', ascending: true }
    }

    paramAreaRecoveryContent: ParamAreaRecoveryContent
    viewCallback: Function
    editCallback: Function
    deleteCallback: Function
    getDetailCallback: Function
    @ViewChild('areaselect') public areaselect: any
    @ViewChild('arearecoveryselect') public arearecoveryselect: any

    @Input() permission = {}

    ngOnInit() {
        this.paramAreaRecoveryContent = new ParamAreaRecoveryContent()
        this.viewCallback = this.view.bind(this)
        this.editCallback = this.edit.bind(this)
        this.deleteCallback = this.remove.bind(this)
        this.getDetailCallback = this.getDetail.bind(this)
    }

    public selectedArea(value: any): void {
        this.paramAreaRecovery.area_id = value.id
        this.alert.loading_start()
        this.service.getAreaRecovery(value.id).then(
            res => {
                if (Array.isArray(res.data)) {
                    this.data.area_recoveries = []
                    for (let area_recovery of res.data) {
                        area_recovery['id'] = area_recovery['area_recovery_id']
                        area_recovery['text'] = area_recovery['area_recovery_name']
                        this.data.area_recoveries.push(area_recovery)
                    }
                } else {
                    this.data.areas = [res.data]
                }
                this.alert.loading_stop()
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
                this.alert.loading_stop()
            }
        )
    }

    public selectedAreaRecovery(value: any): void {
        this.paramAreaRecovery.area_recovery_id = value.id
        this.paramAreaRecoveryForm.patchValue({
            search_area: this.paramAreaRecovery.area_id,
            search_area_recovery: this.paramAreaRecovery.area_recovery_id
        })
    }

    search() {
        this.dataSelected.rows = []
        let tmp_query_all = [
            { param: "page_size", value: 1000000 },
            { param: "page_offset", value: 0 },
            { param: "order_column", value: "id" },
            { param: "order_ascending", value: true }
        ]
        let que_str_all = tmp_query_all.map(function (el) {
            return el.param + '=' + el.value
        }).join('&')
        this.service.getDetail(que_str_all, this.paramAreaRecovery.area_id, this.paramAreaRecovery.area_recovery_id).then(
            res => {
                for (var i = 0; i < res.data.branches.length; i++) {
                    if (res.data.branches[i].checked) {
                        this.dataSelected.rows.push(res.data.branches[i])
                    }
                }
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
            }
        )
        this.getDetail()
        this.areaselect.active = []
        this.arearecoveryselect.active = []
        this.paramAreaRecoveryForm.patchValue({
            search_area: '',
            search_area_recovery: ''
        })
    }

    view(obj) {
        this.paramAreaRecoveryContent = obj
        $('#modal-contentDetail').modal()
    }

    edit(obj) {

    }

    remove(obj) {

    }
    save() {
        swal({
            title: 'Are you sure?',
            text: "Your data will change permanently!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.alert.loading_start()
                let obj = Object.assign({}, this.paramAreaRecovery, this.paramAreaRecoveryForm.value)
                obj.search_nik = undefined
                obj.request_id = Date.now()
                obj.branches = this.dataSelected.rows
                console.log(obj)
                this.service.submit(obj).then(
                    res => {
                        this.alert.success_alert('Your data has been saved', this.id, this.displayData())
                    },
                    err => {
                        if (err.http_status === 422) {
                            this.alert.error_alert(err.message)
                        } else {
                            this.alert.warn_alert(err.message)
                        }
                    }
                )
            }
        })

    }
}