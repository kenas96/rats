import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class ParamAreaRecoveryService {

    constructor(private service: HttpService) { }


    getPaging(query_string) {
        return this.service.get('webuser/parameterarearecovery?' + query_string)
    }

    getArea() {
        return this.service.get('areas')
    }

    getAreaRecovery(area_id) {
        return this.service.get('area_recoveries?area_id=' + area_id+"&is_active=true")
    }

    getDetail(query_string, area_id, area_recovery_id) {
        return this.service.get('webuser/parameterarearecovery/mapping?area_id=' + area_id + '&area_recovery_id=' +
            area_recovery_id + '&' + query_string)
    }

    submit(data) {
        return this.service.post('webuser/parameterarearecovery/mapping', data)
    }

}
