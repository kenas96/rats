import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ParamAreaRecoveryMainComponent } from './paramAreaRecovery-main.component'
import { ParamAreaRecoveryListComponent } from './list/paramAreaRecovery-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: ParamAreaRecoveryMainComponent,
        children: [
            {
                path: '',
                component: ParamAreaRecoveryListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: ParamAreaRecoveryListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class ParamAreaRecoveryRoutingModule { }
