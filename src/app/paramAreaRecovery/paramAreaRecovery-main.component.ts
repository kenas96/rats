import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-paramAreaRecovery',
    template: '<router-outlet></router-outlet>'
})

export class ParamAreaRecoveryMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}