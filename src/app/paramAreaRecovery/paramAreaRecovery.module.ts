import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { ParamAreaRecoveryListComponent } from './list/paramAreaRecovery-list.component'
import { ParamAreaRecoveryDetailComponent } from './detail/paramAreaRecovery-detail.component'
import { ParamAreaRecoveryContentDetailComponent } from './contentDetail/paramAreaRecovery-contentDetail.component'
import { ParamAreaRecoveryService } from './paramAreaRecovery.service'
import { ParamAreaRecoveryRoutingModule } from './paramAreaRecovery-routing.module'
import { ParamAreaRecoveryMainComponent } from './paramAreaRecovery-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Validator } from '../control/validator'
import { Alert } from '../control/util'
import { SelectModule } from 'ng2-select'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule,
        ParamAreaRecoveryRoutingModule,
        SelectModule
    ],
    exports: [],
    declarations: [
        ParamAreaRecoveryListComponent,
        ParamAreaRecoveryContentDetailComponent,
        ParamAreaRecoveryMainComponent,
        ParamAreaRecoveryDetailComponent
    ],
    providers: [
        ParamAreaRecoveryService,
        HttpService,
        AuthGuard,
        Validator,
        Alert
    ],
})
export class ParamAreaRecoveryModule { }
