export class ParamAreaRecovery {
    area_id: string
    area_name: string
    area_recovery_id: number
    area_recovery_name: string

    constructor(data: any = {}) {
        this.area_id = data.area_id
        this.area_name = data.area_name
        this.area_recovery_id = data.area_recovery_id
        this.area_recovery_name = data.area_recovery_name
    }
}

export class ParamAreaRecoveryContent {
    area_id: string
    area_name: string
    area_recovery_id: number
    area_recovery_name: string
    id: string
    name: string

    constructor(data: any = {}) {
        this.area_id = data.area_id
        this.area_name = data.area_name
        this.area_recovery_id = data.area_recovery_id
        this.area_recovery_name = data.area_recovery_name
        this.id = data.id
        this.name = data.name
    }
}