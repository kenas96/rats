import { Component, OnInit, Input } from '@angular/core'
import { ParamAreaRecoveryContent } from './../paramAreaRecovery.model'

@Component({
    selector: 'paramAreaRecovery-contentDetail',
    templateUrl: './paramAreaRecovery-contentDetail.component.html',
    styleUrls: ['./paramAreaRecovery-contentDetail.component.css']
})

export class ParamAreaRecoveryContentDetailComponent implements OnInit {
    constructor() { }

    @Input() id: string
    @Input() paramAreaRecoveryContent: ParamAreaRecoveryContent

    ngOnInit() {

    }
}