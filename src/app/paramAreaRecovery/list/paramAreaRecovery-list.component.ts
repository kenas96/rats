import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { ParamAreaRecoveryService } from '../paramAreaRecovery.service'
import { ParamAreaRecovery } from '../paramAreaRecovery.model'
import { Alert } from '../../control/util'
import swal from 'sweetalert2'


@Component({
    selector: 'paramAreaRecovery-list',
    templateUrl: './paramAreaRecovery-list.component.html',
    styleUrls: ['./paramAreaRecovery-list.component.css']
})

export class ParamAreaRecoveryListComponent implements OnInit {
    constructor(
        private service: ParamAreaRecoveryService,
        private fb: FormBuilder,
        private alert: Alert
    ) { }

    adminType: string = ''
    state: number = 1
    paramAreaRecovery: ParamAreaRecovery
    paramAreaRecoveryForm: FormGroup
    lengthData: number = 0
    lengthDataInnerTable: number = 0
    displayCallback: Function
    getDetailCallback: Function
    viewCallback: Function
    editCallback: Function
    deleteCallback: Function

    data = {
        rows: []
    }

    dataInnerTable = {
        rows: [],
        areas: [
            {
                id: '',
                text: ''
            }
        ],
        area_recoveries: [
            {
                id: '',
                text: ''
            }
        ]
    }

    dataSelected = {
        rows: []
    }

    option: any = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: false,
        disNext: false,
        order: { column: 'area_id', ascending: true }
    }

    optionInnerTable: any = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: false,
        disNext: false,
        order: { column: 'id', ascending: true }
    }

    columns = [
        { text: 'Area Name', field: 'area_name', value: 'area_name' },
        { text: 'Area Recovery Name', field: 'area_recovery_name', value: 'area_recovery_name' }
    ]

    columnsInnerTable = [
        { text: 'ID', field: 'id', value: 'id' },
        { text: 'Name', field: 'name', value: 'name' }
    ]

    listCriteria = [
        { text: 'Area Name', value: 'area_name' },
        { text: 'Area Recovery Name', value: 'area_recovery_name' }
    ]

    listCriteriaInnerTable = [
        { text: 'Name', value: 'name' },
    ]

    permission: any = {
        create: true,
        view: true,
        update: false,
        delete: false,
        upload: false
    }

    permissionInnerTable: any = {
        create: false,
        view: true,
        update: false,
        delete: false,
        upload: false
    }

    CheckBtnPrevNext() {
        if (this.option.page === 1) {
            this.option.disPrev = true
        } else {
            this.option.disPrev = false
        }
        if ((this.option.page === 1 || this.option.page > 1) && this.lengthData <= this.option.pageSize) {
            this.option.disNext = true
        } else {
            this.option.disNext = false
        }
    }

    CheckBtnPrevNextInnerTable() {
        if (this.optionInnerTable.page === 1) {
            this.optionInnerTable.disPrev = true
        } else {
            this.optionInnerTable.disPrev = false
        }
        if ((this.optionInnerTable.page === 1 || this.optionInnerTable.page > 1) &&
            this.lengthDataInnerTable <= this.optionInnerTable.pageSize) {
            this.optionInnerTable.disNext = true
        } else {
            this.optionInnerTable.disNext = false
        }
    }

    ngOnInit() {
        this.paramAreaRecovery = new ParamAreaRecovery()
        this.displayCallback = this.displayData.bind(this)
        this.getDetailCallback = this.getDetail.bind(this)
        this.viewCallback = this.view.bind(this)
        this.editCallback = this.edit.bind(this)
        this.deleteCallback = this.remove.bind(this)
        this.paramAreaRecoveryForm = this.fb.group({
            area_id: ['', [Validators.required]],
            area_name: [{ value: '', disabled: true }],
            area_recovery_id: ['', [Validators.required]],
            area_recovery_name: [{ value: '', disabled: true }],
            search_area: '',
            search_area_recovery: ''
        })

        //admin permission validation
        let user = localStorage.getItem('auth-user')
        let decoded = atob(user)
        for (var i = 0; i < decoded.length; i++) {
            if (decoded[i] === ':') {
                this.adminType = decoded.substr(0, i)
                break
            }
        }

        //additional permission
        if (this.adminType === 'Reco Admin' || this.adminType === 'Warehouse Admin') {
            this.permission.create = false
        }
    }

    ngAfterViewInit() {
        this.displayData()
    }

    loadingAnimationGridStart() {
        document.getElementById('dataGrid').style.display = "none"
        document.getElementById('loadingGrid').style.display = "block"
    }

    loadingAnimationGridStop() {
        document.getElementById('dataGrid').style.display = "block"
        document.getElementById('loadingGrid').style.display = "none"
    }

    parseData(data) {
    }

    parseDataInnerTable(data) {
        setTimeout(() => {
            for (var i = 0; i < this.dataSelected.rows.length; i++) {
                $('#checkbox' + this.dataSelected.rows[i].id).prop('checked', true)
            }

            //checked motor mobile durable
            if (this.state === 0) {
                for (var i = 0; i < this.dataInnerTable.rows.length; i++) {
                    if (this.dataInnerTable.rows[i].ableObjectMotor) {
                        $('#motor' + this.dataInnerTable.rows[i].id).prop('checked', true)
                    }
                    if (this.dataInnerTable.rows[i].ableObjectMobile) {
                        $('#mobile' + this.dataInnerTable.rows[i].id).prop('checked', true)
                    }
                    if (this.dataInnerTable.rows[i].ableObjectDurable) {
                        $('#durable' + this.dataInnerTable.rows[i].id).prop('checked', true)
                    }
                }
            } else {
                for (var i = 0; i < this.dataSelected.rows.length; i++) {
                    if (this.dataSelected.rows[i].ableObjectMotor) {
                        $('#motor' + this.dataSelected.rows[i].id).prop('checked', true)
                    }
                    if (this.dataSelected.rows[i].ableObjectMobile) {
                        $('#mobile' + this.dataSelected.rows[i].id).prop('checked', true)
                    }
                    if (this.dataSelected.rows[i].ableObjectDurable) {
                        $('#durable' + this.dataSelected.rows[i].id).prop('checked', true)
                    }
                }
            }
        }, 500)
    }

    displayData() {
        this.loadingAnimationGridStart()
        let tmp_query = [
            { param: "page_size", value: parseInt(this.option.pageSize) + 1 },
            { param: "page_offset", value: parseInt(this.option.pageOffset) },
            { param: "order_column", value: this.option.order.column },
            { param: "order_ascending", value: this.option.order.ascending }
        ]
        if (Object.keys(this.option.criteria).length > 0) {
            tmp_query.push({ param: this.option.criteria.text, value: this.option.criteria.value })
        }
        let que_str = tmp_query.map(function (el) {
            return el.param + '=' + el.value
        }).join('&')
        this.service.getPaging(que_str).then(
            res => {
                if (Array.isArray(res.data)) {
                    this.lengthData = res.data.length
                    this.data.rows = res.data
                    if (this.lengthData > this.option.pageSize) {
                        let idx = this.lengthData - 1
                        this.data.rows.splice(idx, 1)
                    }
                } else {
                    this.data.rows = [res.data]
                }
                this.CheckBtnPrevNext()
                this.parseData(this.data.rows)
                this.loadingAnimationGridStop()
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
                this.loadingAnimationGridStop()
            }
        )
        this.service.getArea().then(
            res => {
                if (Array.isArray(res.data)) {
                    this.dataInnerTable.areas = []
                    this.dataInnerTable.area_recoveries = []
                    for (let area of res.data) {
                        area['text'] = area['name']
                        this.dataInnerTable.areas.push(area)
                    }
                } else {
                    this.dataInnerTable.areas = [res.data]
                }
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
            }
        )
    }

    getDetail() {
        this.alert.loading_start()
        if (this.state === 0) {
            //view custom
            this.dataInnerTable.rows = []
            let tmp_query_all = [
                { param: "page_size", value: 1000000 },
                { param: "page_offset", value: 0 },
                { param: "order_column", value: this.optionInnerTable.order.column },
                { param: "order_ascending", value: this.optionInnerTable.order.ascending }
            ]
            if (Object.keys(this.optionInnerTable.criteria).length > 0) {
                tmp_query_all.push({ param: this.optionInnerTable.criteria.text, value: this.optionInnerTable.criteria.value });
            }
            let que_str_all = tmp_query_all.map(function (el) {
                return el.param + '=' + el.value
            }).join('&')
            this.service.getDetail(que_str_all, this.paramAreaRecovery.area_id, this.paramAreaRecovery.area_recovery_id).then(
                res => {
                    for (var i = 0; i < res.data.branches.length; i++) {
                        if (res.data.branches[i].checked) {
                            this.dataInnerTable.rows.push(res.data.branches[i])
                        }
                    }
                    this.parseDataInnerTable(this.dataInnerTable.rows)
                    this.alert.loading_stop()
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                }
            )
        } else {
            let tmp_query = [
                { param: "page_size", value: parseInt(this.optionInnerTable.pageSize) + 1 },
                { param: "page_offset", value: parseInt(this.optionInnerTable.pageOffset) },
                { param: "order_column", value: this.optionInnerTable.order.column },
                { param: "order_ascending", value: this.optionInnerTable.order.ascending }
            ]
            if (Object.keys(this.optionInnerTable.criteria).length > 0) {
                tmp_query.push({ param: this.optionInnerTable.criteria.text, value: this.optionInnerTable.criteria.value });
            }
            let que_str = tmp_query.map(function (el) {
                return el.param + '=' + el.value
            }).join('&')
            this.service.getDetail(que_str, this.paramAreaRecovery.area_id, this.paramAreaRecovery.area_recovery_id).then(
                res => {
                    if (Array.isArray(res.data.branches)) {
                        this.paramAreaRecovery = res.data
                        this.populateData()
                        this.lengthDataInnerTable = res.data.branches.length
                        this.dataInnerTable.rows = res.data.branches

                        if (this.lengthDataInnerTable > this.optionInnerTable.pageSize) {
                            let idx = this.lengthDataInnerTable - 1
                            this.dataInnerTable.rows.splice(idx, 1)
                        }
                    } else {
                        this.dataInnerTable.rows = [res.data]
                    }
                    this.CheckBtnPrevNextInnerTable()
                    this.parseDataInnerTable(this.dataInnerTable.rows)
                    this.alert.loading_stop()
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                }
            )
        }

    }

    focusField() {
        setTimeout(() => {
            $("#searcharea").focus()
        }, 500)
    }

    populateData() {
        this.paramAreaRecoveryForm.patchValue({
            area_id: this.paramAreaRecovery.area_id,
            area_name: this.paramAreaRecovery.area_name,
            area_recovery_id: this.paramAreaRecovery.area_recovery_id,
            area_recovery_name: this.paramAreaRecovery.area_recovery_name,
            search_area: '',
            search_area_recovery: ''
        })
    }

    resetOption() {
        this.optionInnerTable.page = 1
        this.optionInnerTable.pageOffset = 0
        this.optionInnerTable.pageSize = 10
        this.optionInnerTable.criteria = {}
        this.optionInnerTable.disPrev = false
        this.optionInnerTable.disNext = false
        this.optionInnerTable.order = { column: 'id', ascending: true }

        this.paramAreaRecoveryForm.reset()
        this.dataInnerTable.rows = []
        this.dataSelected.rows = []
        this.optionInnerTable.criteria.value = ''
        $('#search_in').val("")
    }

    addNew() {
        this.resetOption()
        this.state = 1
        this.paramAreaRecovery = new ParamAreaRecovery()
        this.focusField()
        $('#modal-detail').modal()
        $('input:checkbox').prop('checked', false)
    }

    view(obj) {
        this.resetOption()
        this.state = 0
        this.paramAreaRecovery = obj

        let tmp_query_all = [
            { param: "page_size", value: 1000000 },
            { param: "page_offset", value: 0 },
            { param: "order_column", value: "id" },
            { param: "order_ascending", value: true }
        ]
        let que_str_all = tmp_query_all.map(function (el) {
            return el.param + '=' + el.value
        }).join('&')

        this.alert.loading_start()
        this.service.getDetail(que_str_all, this.paramAreaRecovery.area_id, this.paramAreaRecovery.area_recovery_id).then(
            res => {
                this.paramAreaRecovery = res.data
                this.populateData()
                for (var i = 0; i < res.data.branches.length; i++) {
                    if (res.data.branches[i].checked) {
                        this.dataInnerTable.rows.push(res.data.branches[i])
                        // this.dataSelected.rows.push(res.data.branches[i])
                    }
                }
                this.parseDataInnerTable(this.dataInnerTable.rows)
                this.alert.loading_stop()
                $('#modal-detail').modal()
                $('input:checkbox').prop('checked', false)

            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
            }
        )
    }

    edit(obj) {
    }

    remove(obj) {
    }
}