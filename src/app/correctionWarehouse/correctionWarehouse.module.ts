import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { CorrectionWarehouseListComponent } from './list/correctionWarehouse-list.component'
import { CorrectionWarehouseDetailComponent } from './detail/correctionWarehouse-detail.component'
import { CorrectionWarehouseService } from './correctionWarehouse.service'
import { CorrectionWarehouseRoutingModule } from './correctionWarehouse-routing.module'
import { CorrectionWarehouseMainComponent } from './correctionWarehouse-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Validator } from '../control/validator'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule,
        CorrectionWarehouseRoutingModule
    ],
    exports: [],
    declarations: [
        CorrectionWarehouseListComponent,
        CorrectionWarehouseDetailComponent,
        CorrectionWarehouseMainComponent
    ],
    providers: [
        CorrectionWarehouseService,
        HttpService,
        AuthGuard,
        Validator,
        Alert
    ],
})
export class CorrectionWarehouseModule { }
