import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { CorrectionWarehouseService } from '../correctionWarehouse.service'
import { CorrectionWarehouse } from '../correctionWarehouse.model'
import { Alert } from '../../control/util'

@Component({
    selector: 'correctionWarehouse-list',
    templateUrl: './correctionWarehouse-list.component.html',
    styleUrls: ['./correctionWarehouse-list.component.css']
})

export class CorrectionWarehouseListComponent implements OnInit {
    constructor(
        private service: CorrectionWarehouseService,
        private fb: FormBuilder,
        private alert: Alert
    ) { }

    adminType: string = ''
    correctionWarehouse: CorrectionWarehouse
    correctionWarehouseForm: FormGroup
    lengthData: number = 0
    displayCallback: Function
    viewCallback: Function
    editCallback: Function
    deleteCallback: Function

    data = {
        rows: []
    }

    dataWarehouse = {
        warehouse: {
            rows: [],
            pic: []
        },
        remote_warehouse: {
            rows: [],
            pic: []
        },
        cabang: {
            rows: [],
            pic: []
        },
    }

    option: any = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: false,
        disNext: false,
        order: { column: 'id', ascending: true }
    }

    columns = [
        { text: 'Mitra Name', field: 'mitra_name', value: 'mitra_name' },
        { text: 'Warehouse Name', field: 'warehouse_name', value: 'warehouse_name' },
        { text: 'Warehouse PIC', field: 'warehouse_pic_name', value: 'warehouse_pic_name' },
        { text: 'No Contract', field: 'no_kontrak', value: 'no_kontrak' },
        { text: 'No SK', field: 'no_sk', value: 'no_sk' },
        { text: 'Status', field: 'assignment_status', value: 'assignment_status' },
        { text: 'Assigned Date', field: 'assigned_date', value: 'assigned_date' }
    ]

    listCriteria = [
        { text: 'Mitra Name', value: 'mitra_name' },
        { text: 'Warehouse Name', value: 'warehouse_name' },
        { text: 'Warehouse PIC', value: 'warehouse_pic_name' },
        { text: 'No Contract', value: 'no_kontrak' },
        { text: 'No SK', value: 'no_sk' },
    ]

    permission: any = {
        create: false,
        view: false,
        update: true,
        delete: false,
        upload: false
    }

    CheckBtnPrevNext() {
        if (this.option.page === 1) {
            this.option.disPrev = true
        } else {
            this.option.disPrev = false
        }
        if ((this.option.page === 1 || this.option.page > 1) && this.lengthData <= this.option.pageSize) {
            this.option.disNext = true
        } else {
            this.option.disNext = false
        }
    }

    ngOnInit() {
        this.correctionWarehouse = new CorrectionWarehouse()
        this.displayCallback = this.displayData.bind(this)
        this.viewCallback = this.view.bind(this)
        this.editCallback = this.edit.bind(this)
        this.deleteCallback = this.remove.bind(this)
        this.correctionWarehouseForm = this.fb.group({
            id: '',
            reason: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(255)]],
            to_warehouse_id: ['', [Validators.required]],
            to_warehouse_type_id: ['', [Validators.required]],
            to_pic_id: ['', [Validators.required]]
        })

        //admin permission validation
        let user = localStorage.getItem('auth-user')
        let decoded = atob(user)
        for (var i = 0; i < decoded.length; i++) {
            if (decoded[i] === ':') {
                this.adminType = decoded.substr(0, i)
                break
            }
        }

        //additional permission
        if (this.adminType === 'Reco Admin' || this.adminType === 'Warehouse Admin') {
            this.permission.update = false
        }
    }

    ngAfterViewInit() {
        this.displayData()
    }

    loadingAnimationGridStart() {
        document.getElementById('dataGrid').style.display = "none"
        document.getElementById('loadingGrid').style.display = "block"
    }

    loadingAnimationGridStop() {
        document.getElementById('dataGrid').style.display = "block"
        document.getElementById('loadingGrid').style.display = "none"
    }

    resetDataWarehouse() {
        this.dataWarehouse = {
            warehouse: {
                rows: [],
                pic: []
            },
            remote_warehouse: {
                rows: [],
                pic: []
            },
            cabang: {
                rows: [],
                pic: []
            },
        }
    }

    displayData() {
        this.loadingAnimationGridStart()
        let tmp_query = [
            { param: "page_size", value: parseInt(this.option.pageSize) + 1 },
            { param: "page_offset", value: parseInt(this.option.pageOffset) },
            { param: "order_column", value: this.option.order.column },
            { param: "order_ascending", value: this.option.order.ascending }
        ]
        if (Object.keys(this.option.criteria).length > 0) {
            tmp_query.push({ param: this.option.criteria.text, value: this.option.criteria.value })
        }
        let que_str = tmp_query.map(function (el) {
            return el.param + '=' + el.value
        }).join('&')
        this.service.getPaging(que_str).
            then(
                res => {
                    if (Array.isArray(res.data)) {
                        this.lengthData = res.data.length
                        this.data.rows = res.data
                        if (this.lengthData > this.option.pageSize) {
                            let idx = this.lengthData - 1
                            this.data.rows.splice(idx, 1)
                        }
                    } else {
                        this.data.rows = [res.data]
                    }
                    this.CheckBtnPrevNext()
                    this.loadingAnimationGridStop()
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                    this.loadingAnimationGridStop()
                }
            )
        this.service.getAllWarehouse().then(
            res => {
                if (res.meta.status) {
                    this.resetDataWarehouse()
                    for (let data of res.data) {
                        if (data.warehouse_type_id === 1) {
                            //type warehouse
                            var exist = false
                            for (var i = 0; i < this.dataWarehouse.warehouse.rows.length; i++) {
                                if (this.dataWarehouse.warehouse.rows[i].warehouse_id == data.warehouse_id) {
                                    exist = true
                                    break
                                }
                            }
                            if (!exist) {
                                this.dataWarehouse.warehouse.rows.push(data)
                            }
                            this.dataWarehouse.warehouse.pic.push(data)

                        }
                        else if (data.warehouse_type_id === 2) {
                            //type remote warehouse
                            var exist = false
                            for (var i = 0; i < this.dataWarehouse.remote_warehouse.rows.length; i++) {
                                if (this.dataWarehouse.remote_warehouse.rows[i].warehouse_id == data.warehouse_id) {
                                    exist = true
                                    break
                                }
                            }
                            if (!exist) {
                                this.dataWarehouse.remote_warehouse.rows.push(data)
                            }
                            this.dataWarehouse.remote_warehouse.pic.push(data)

                        } else {
                            //type cabang
                            var exist = false
                            for (var i = 0; i < this.dataWarehouse.cabang.rows.length; i++) {
                                if (this.dataWarehouse.cabang.rows[i].warehouse_id == data.warehouse_id) {
                                    exist = true
                                    break
                                }
                            }
                            if (!exist) {
                                this.dataWarehouse.cabang.rows.push(data)
                            }
                            this.dataWarehouse.cabang.pic.push(data)
                        }
                    }
                }
                else {
                    this.alert.error_alert(res.meta.message)
                }
            },
            err => {
                this.alert.warning_alert()
            }
        )
    }

    view(obj) {

    }

    edit(obj) {
        this.correctionWarehouse = obj
        this.correctionWarehouseForm.reset()
        $('#modal-detail').modal()
    }

    remove(obj) {
    }
}
