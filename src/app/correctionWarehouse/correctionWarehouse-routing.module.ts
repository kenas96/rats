import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { CorrectionWarehouseMainComponent } from './correctionWarehouse-main.component'
import { CorrectionWarehouseListComponent } from './list/correctionWarehouse-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: CorrectionWarehouseMainComponent,
        children: [
            {
                path: '',
                component: CorrectionWarehouseListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: CorrectionWarehouseListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class CorrectionWarehouseRoutingModule { }
