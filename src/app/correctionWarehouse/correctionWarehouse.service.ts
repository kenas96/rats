import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class CorrectionWarehouseService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('webuser/changewarehouse?' + query_string);
    }

    getWarehouse(type_id) {
        return this.service.get('warehouse/' + type_id);
    }

    getAllWarehouse() {
        return this.service.get('warehouse');
    }

    changeWarehouse(data, id) {
        return this.service.put('webuser/changewarehouse/' + id, data);
    }

}