import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-correctionWarehouse',
    template: '<router-outlet></router-outlet>'
})

export class CorrectionWarehouseMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}