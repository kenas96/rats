import { Component, OnInit, Input } from '@angular/core'
import { FormGroup, AbstractControl } from '@angular/forms'
import { CorrectionWarehouse } from './../correctionWarehouse.model'
import { CorrectionWarehouseService } from './../correctionWarehouse.service'
import { Alert } from '../../control/util'
import 'rxjs/add/operator/debounceTime'
import swal from 'sweetalert2'

@Component({
    selector: 'correctionWarehouse-detail',
    templateUrl: './correctionWarehouse-detail.component.html',
    styleUrls: ['./correctionWarehouse-detail.component.css']
})

export class CorrectionWarehouseDetailComponent implements OnInit {
    constructor(
        private service: CorrectionWarehouseService,
        private alert: Alert
    ) { }

    @Input() id: string
    @Input() correctionWarehouse: CorrectionWarehouse
    @Input() displayData: Function
    @Input() correctionWarehouseForm: FormGroup
    @Input() dataWarehouse = {
        warehouse: {
            rows: [],
            pic: []
        },
        remote_warehouse: {
            rows: [],
            pic: []
        },
        cabang: {
            rows: [],
            pic: []
        },
    }
    picSelected = []
    reasonMessage: string
    changeWarehouse: CorrectionWarehouse

    private validationMessage = {
        required: "Please fill this field.",
        minlength: "Min. character allowed are 3 characters.",
        maxlength: "Max. character allowed are 255 characters."
    }

    ngOnInit() {
        const reasonControl = this.correctionWarehouseForm.get('reason')
        reasonControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageReason(reasonControl)
        )

        const warehouseTypeControl = this.correctionWarehouseForm.get('to_warehouse_type_id')
        warehouseTypeControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageWarehouseType(warehouseTypeControl)
        )

        const warehouseControl = this.correctionWarehouseForm.get('to_warehouse_id')
        warehouseControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageWarehouse(warehouseControl)
        )
    }

    setMessageReason(c: AbstractControl): void {
        this.reasonMessage = ''
        if ((c.touched || c.dirty) && c.errors) {
            this.reasonMessage = Object.keys(c.errors).map(key =>
                this.validationMessage[key]
            )[0]
        }
    }

    setMessageWarehouseType(c: AbstractControl): void {
        this.correctionWarehouseForm.patchValue({
            to_warehouse_id: '',
            to_pic_id: ''
        })
    }

    setMessageWarehouse(c: AbstractControl): void {
        if (this.correctionWarehouseForm.get('to_warehouse_id').value !== null && this.correctionWarehouseForm.get('to_warehouse_id').value !== '') {
            this.picSelected = []
            if (this.correctionWarehouseForm.get('to_warehouse_type_id').value === 'warehouse') {
                for (let pic of this.dataWarehouse.warehouse.pic) {
                    if (pic['warehouse_id'] === this.correctionWarehouseForm.get('to_warehouse_id').value) {
                        this.picSelected.push(pic)
                    }
                }
            } else if (this.correctionWarehouseForm.get('to_warehouse_type_id').value === 'remote_warehouse') {
                for (let pic of this.dataWarehouse.remote_warehouse.pic) {
                    if (pic['warehouse_id'] === this.correctionWarehouseForm.get('to_warehouse_id').value) {
                        this.picSelected.push(pic)
                    }
                }
            } else {
                for (let pic of this.dataWarehouse.cabang.pic) {
                    if (pic['warehouse_id'] === this.correctionWarehouseForm.get('to_warehouse_id').value) {
                        this.picSelected.push(pic)
                    }
                }
            }
            this.correctionWarehouseForm.patchValue({
                to_pic_id: ''
            })

        }
    }

    save() {
        swal({
            title: 'Are you sure?',
            text: "Your data will change permanently!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.alert.loading_start()
                this.changeWarehouse = new CorrectionWarehouse
                this.changeWarehouse.id = this.correctionWarehouse.id
                this.changeWarehouse.reason = this.correctionWarehouseForm.get('reason').value
                this.changeWarehouse.to_warehouse_id = this.correctionWarehouseForm.get('to_warehouse_id').value
                this.changeWarehouse.to_pic_id = this.correctionWarehouseForm.get('to_pic_id').value

                if (this.correctionWarehouseForm.get('to_warehouse_type_id').value === 'warehouse') {
                    this.changeWarehouse.to_warehouse_type_id = 1
                } else if (this.correctionWarehouseForm.get('to_warehouse_type_id').value === 'remote_warehouse') {
                    this.changeWarehouse.to_warehouse_type_id = 2
                } else if (this.correctionWarehouseForm.get('to_warehouse_type_id').value === 'cabang') {
                    this.changeWarehouse.to_warehouse_type_id = 3
                }
                console.log(this.changeWarehouse)
                console.log(this.changeWarehouse.id)
                this.service.changeWarehouse(this.changeWarehouse, this.changeWarehouse.id).then(
                    res => {
                        if (res.meta.status) {
                            this.alert.success_alert('The warehouse has been changed', this.id, this.displayData())
                        }
                        else {
                            this.alert.error_alert(res.meta.message)
                        }
                    },
                    err => {
                        this.alert.warning_alert()
                    }
                )
            }
        })
    }
}