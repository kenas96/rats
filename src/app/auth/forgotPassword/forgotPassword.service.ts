import { Injectable } from '@angular/core';
import { HttpService } from '../../http.service';

@Injectable()
export class ForgotPasswordService {

    constructor(private service: HttpService) { }

    forgot(data) {
        return this.service.post('forgotpassword', data);
    }
}
