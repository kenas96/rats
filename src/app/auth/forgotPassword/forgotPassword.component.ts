import { Component, OnInit, Input } from '@angular/core'
import { FormGroup, AbstractControl } from '@angular/forms'
import { ForgotPasswordService } from './forgotPassword.service'
import { Alert } from '../../control/util'
import 'rxjs/add/operator/debounceTime'
import { AppConfig } from '../../config/app.config';

@Component({
    selector: 'forgot-password',
    templateUrl: './forgotPassword.component.html',
    styleUrls: ['./forgotPassword.component.css']
})

export class ForgotPasswordComponent implements OnInit {
    constructor(
        private service: ForgotPasswordService,
        private alert: Alert,
        private config: AppConfig) {
        this.base = this.config.getConfig('base_url')
    }

    @Input() id: string
    @Input() admin
    @Input() initAdminData: Function
    @Input() forgotPasswordForm: FormGroup
    base: string
    emailMessage: string

    private validationMessage = {
        required: "Please fill this field.",
        pattern: "Please enter a valid emaill address."
    }

    ngOnInit() {
        const emailControl = this.forgotPasswordForm.get('email')
        emailControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageEmail(emailControl)
        )
    }

    setMessageEmail(c: AbstractControl): void {
        this.emailMessage = ''
        if ((c.touched || c.dirty) && c.errors) {
            this.emailMessage = Object.keys(c.errors).map(key =>
                this.validationMessage[key]
            )[0]
        }
    }

    save() {
        this.alert.loading_start()
        let obj = Object.assign({}, this.admin, this.forgotPasswordForm.value)
        obj["link"] = this.base
        this.service.forgot(obj).then(
            res => {
                this.alert.success_alert('Send recovery email successed', this.id, this.initAdminData())
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
            }
        )
    }
}
