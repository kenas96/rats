import { Injectable } from '@angular/core'
import { HttpService } from '../../http.service'

@Injectable()
export class LoginService {

    constructor(private service: HttpService) { }

    login(data) {
        return this.service.postLogin('webuser/login', data)
    }

    logout(data) {
        return this.service.post('webuser/logout', data)
    }

}