import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { LoginService } from './login.service'
import { Admin, Login } from '../../pages/admin.model'
import { JwtHelper } from 'angular2-jwt'
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms'
import { Alert } from '../../control/util'
import 'rxjs/add/operator/debounceTime'

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
    constructor(
        private router: Router,
        private service: LoginService,
        private fb: FormBuilder,
        private alert: Alert
    ) { }

    admin: Admin
    loginObj: Login
    initAdminDataCallback: Function
    jwtHelper: JwtHelper = new JwtHelper()
    forgotPasswordForm: FormGroup
    loginForm: FormGroup
    usernameMessage: string
    passwordMessage: string

    private validationMessage = {
        required: "Please fill this field."
    }

    ngOnInit() {
        this.initAdminData()
        this.initAdminDataCallback = this.initAdminData.bind(this)
        this.forgotPasswordForm = this.fb.group({
            email: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+')]]
        })
        this.loginForm = this.fb.group({
            username: ['', [Validators.required]],
            password: ['', [Validators.required]]
        })

        const usernameControl = this.loginForm.get('username')
        usernameControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageUsername(usernameControl)
        )

        const passwordControl = this.loginForm.get('password')
        passwordControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessagePassword(passwordControl)
        )

        setTimeout(() => {
            $("#username").focus()
        }, 500)
    }

    setMessageUsername(c: AbstractControl): void {
        this.usernameMessage = ''
        if ((c.touched || c.dirty) && c.errors) {
            this.usernameMessage = Object.keys(c.errors).map(key =>
                this.validationMessage[key]
            )[0]
        }
    }

    setMessagePassword(c: AbstractControl): void {
        this.passwordMessage = ''
        if ((c.touched || c.dirty) && c.errors) {
            this.passwordMessage = Object.keys(c.errors).map(key =>
                this.validationMessage[key]
            )[0]
        }
    }

    initAdminData() {
        this.admin = new Admin()
        this.loginObj = new Login()
    }

    focusField() {
        setTimeout(() => {
            $("#email").focus()
        }, 500)
    }

    forgot() {
        this.forgotPasswordForm.reset()
        this.initAdminData()
        this.focusField()
        $('#forgot-password').modal()
    }

    login() {
        let obj = Object.assign({}, this.loginObj, this.loginForm.value)
        this.alert.loading_login_start()
        this.service.login(obj).then(
            res => {
                if (res.data.access_token == "") {
                    if (res.data.is_active) {
                        this.alert.error_alert("Invalid username or password")
                    } else {
                        this.alert.error_alert("Account locked. Please contact admin for more information")
                    }
                } else {
                    let varBase64 = res.data.role + ":" + res.data.name + ":" + res.data.email + ":" + res.data.picture
                    let data = btoa(varBase64)

                    this.alert.success_alert('Successfully signed', '', '')
                    localStorage.setItem('auth-token', res.data.access_token)
                    localStorage.setItem('auth-user', data)
                    this.router.navigate(['/pages/dashboard'])
                }
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
            }
        )
    }
}