import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-reportHirarkiExternal',
    template: '<router-outlet></router-outlet>'
})

export class ReportHirarkiExternalMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}