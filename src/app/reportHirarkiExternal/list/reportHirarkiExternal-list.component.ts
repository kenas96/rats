import { Component, OnInit } from '@angular/core'
import { ReportHirarkiExternalService } from '../reportHirarkiExternal.service'
import { Alert } from '../../control/util'
import { AppConfig } from '../../config/app.config';

@Component({
  selector: 'reportHirarkiExternal-list',
  templateUrl: './reportHirarkiExternal-list.component.html',
  styleUrls: ['./reportHirarkiExternal-list.component.css']
})

export class ReportHirarkiExternalListComponent implements OnInit {
  constructor(
    private service: ReportHirarkiExternalService,
    private alert: Alert,
    private config: AppConfig) {
    this.base = this.config.getConfig('base_url')
  }

  lengthData: number = 0
  displayCallback: Function
  downloadReportCallback: Function
  downloadFullReportCallback: Function
  base: string

  data = {
    rows: []
  }

  option: any = {
    page: 1,
    pageOffset: 0,
    pageSize: 10,
    criteria: [{}],
    disPrev: false,
    disNext: false,
    order: { column: 'id', ascending: true }
  }

  columns = [
    { text: 'Name', field: 'name', value: 'name' },
    { text: 'KTP', field: 'ktp', value: 'ktp' },
    { text: 'Handphone', field: 'handphone', value: 'handohone' },
    { text: 'Mou No', field: 'mou_no', value: 'mou_no' },
    { text: 'Mou Name', field: 'mou_name', value: 'mou_name' },
    { text: 'PIC Name', field: 'pic_name', value: 'pic_name' },
    { text: 'PIC Handphone', field: 'pic_handphone', value: 'pic_handphone' }
  ]

  listCriteria = [
    { text: 'Name', value: 'name', filter: '', type: 'text' },
    { text: 'Handphone', value: 'handphone', filter: '', type: 'text' },
    { text: 'KTP', value: 'ktp', filter: '', type: 'text' },
  ]

  CheckBtnPrevNext() {
    if (this.option.page === 1) {
      this.option.disPrev = true
    } else {
      this.option.disPrev = false
    }
    if ((this.option.page === 1 || this.option.page > 1) && this.lengthData <= this.option.pageSize) {
      this.option.disNext = true
    } else {
      this.option.disNext = false
    }
  }

  ngOnInit() {
    this.displayCallback = this.displayData.bind(this)
    this.downloadReportCallback = this.downloadReport.bind(this)
    this.downloadFullReportCallback = this.downloadFullReport.bind(this)
  }

  ngAfterViewInit() {
    this.displayData()
  }

  loadingAnimationGridStart() {
    document.getElementById('dataGrid').style.display = "none"
    document.getElementById('loadingGrid').style.display = "block"
  }

  loadingAnimationGridStop() {
    document.getElementById('dataGrid').style.display = "block"
    document.getElementById('loadingGrid').style.display = "none"
  }

  parseData() {
  }

  refresh() {
    this.listCriteria = [
      { text: 'Name', value: 'name', filter: '', type: 'text' },
      { text: 'Handphone', value: 'handphone', filter: '', type: 'text' },
      { text: 'Ktp', value: 'ktp', filter: '', type: 'text' },
    ]
    this.option.criteria = []
    this.displayData()
  }

  displayData() {
    this.loadingAnimationGridStart()
    let tmp_query = [
      { param: "page_size", value: parseInt(this.option.pageSize) + 1 },
      { param: "page_offset", value: parseInt(this.option.pageOffset) },
      { param: "order_column", value: this.option.order.column },
      { param: "order_ascending", value: this.option.order.ascending }
    ]
    if (Object.keys(this.option.criteria).length > 0) {
      for (let crt of this.option.criteria) {
        //if (crt.value !== "") {
        var exist = false
        for (let query of tmp_query) {
          if (query.param === crt.text) {
            query.value = crt.value
            exist = true
          }
        }
        if (!exist) {
          tmp_query.push({ param: crt.text, value: crt.value })
        }
        // }
      }
    }
    //this.option.criteria = []
    let que_str = tmp_query.map(function (el) {
      return el.param + '=' + el.value
    }).join('&')
    this.service.getPaging(que_str).
      then(
        res => {
          if (Array.isArray(res.data)) {
            this.lengthData = res.data.length
            this.data.rows = res.data
            if (this.lengthData > this.option.pageSize) {
              let idx = this.lengthData - 1
              this.data.rows.splice(idx, 1)
            }
          } else {
            this.data.rows = [res.data]
          }
          this.CheckBtnPrevNext()
          this.parseData()
          this.loadingAnimationGridStop()
        },
        err => {
          if (err.http_status === 422) {
            this.alert.error_alert(err.message)
          } else {
            this.alert.warn_alert(err.message)
          }
          this.loadingAnimationGridStop()
        }
      )
  }

  downloadReport() {
    let tmp_query = [
      { param: "page_size", value: parseInt(this.option.pageSize) },
      { param: "page_offset", value: parseInt(this.option.pageOffset) },
      { param: "order_column", value: this.option.order.column },
      { param: "order_ascending", value: this.option.order.ascending }
    ]
    if (Object.keys(this.option.criteria).length > 0) {
      for (let crt of this.option.criteria) {
        if (crt.value !== "") {
          tmp_query.push({ param: crt.text, value: crt.value })
        }
      }
    }
    this.option.criteria = []
    let que_str = tmp_query.map(function (el) {
      return el.param + '=' + el.value
    }).join('&')
    this.service.downloadReport(que_str).
      then(
        res => {
          window.open(this.base + res.data.url)
        },
        err => {
          if (err.http_status === 422) {
            this.alert.error_alert(err.message)
          } else {
            this.alert.warn_alert(err.message)
          }
        }
      )
  }

  downloadFullReport() {
    this.service.downloadFullReport().then(
      res => {
        window.open(this.base + res.data.url)
      },
      err => {
        if (err.http_status === 422) {
          this.alert.error_alert(err.message)
        } else {
          this.alert.warn_alert(err.message)
        }
      }
    )
  }
}
