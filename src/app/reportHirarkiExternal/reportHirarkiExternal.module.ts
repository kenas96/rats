import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { ReportHirarkiExternalListComponent } from './list/reportHirarkiExternal-list.component'
import { ReportHirarkiExternalService } from './reportHirarkiExternal.service'
import { ReportHirarkiExternalRoutingModule } from './reportHirarkiExternal-routing.module'
import { ReportHirarkiExternalMainComponent } from './reportHirarkiExternal-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        HttpModule,
        RouterModule,
        ReportHirarkiExternalRoutingModule
    ],
    exports: [],
    declarations: [
        ReportHirarkiExternalListComponent,
        ReportHirarkiExternalMainComponent
    ],
    providers: [
        ReportHirarkiExternalService,
        HttpService,
        AuthGuard,
        Alert
    ],
})
export class ReportHirarkiExternalModule { }
