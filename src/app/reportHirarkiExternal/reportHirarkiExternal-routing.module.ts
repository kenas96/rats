import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ReportHirarkiExternalMainComponent } from './reportHirarkiExternal-main.component'
import { ReportHirarkiExternalListComponent } from './list/reportHirarkiExternal-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: ReportHirarkiExternalMainComponent,
        children: [
            {
                path: '',
                component: ReportHirarkiExternalListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: ReportHirarkiExternalListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class ReportHirarkiExternalRoutingModule { }
