import { Component, OnInit, Input } from '@angular/core'

@Component({
    selector: 'notification-recipient',
    templateUrl: './notification-recipient.component.html',
    styles: [`
    .panel-primary > .panel-heading {
        background-color: #3E4551;
    }
  `]
})

export class NotificationRecipientComponent implements OnInit {
    constructor() { }
    ngOnInit() { }

    @Input() id: string
    @Input() notification

}