export class Notification {
    request_id: number
    id: number
    send_by_id: number
    sender_by_name: string
    subject: number
    notification_message: string
    insert_date: number
    recipient: any

    constructor(data: any = {}) {
        this.request_id = data.request_id
        this.id = data.id
        this.send_by_id = data.send_by_id
        this.sender_by_name = data.sender_by_name
        this.subject = data.subject
        this.notification_message = data.notification_message
        this.insert_date = data.insert_date
        this.recipient = data.recipient
    }

}

export class Recipient {
    recipient_id: number
    recipient_name: string
    recipient_user_type_id: number
    recipient_user_type: string

    constructor(data: any = {}) {
        this.recipient_id = data.recipient_id
        this.recipient_name = data.recipient_name
        this.recipient_user_type_id = data.recipient_user_type_id
        this.recipient_user_type = data.recipient_user_type
    }

}