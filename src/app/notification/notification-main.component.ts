import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-notification',
    template: '<router-outlet></router-outlet>'
})

export class NotificationMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}