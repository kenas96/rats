import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { NotificationListComponent } from './list/notification-list.component'
import { NotificationDetailComponent } from './detail/notification-detail.component'
import { NotificationRecipientComponent } from './recipient/notification-recipient.component'
import { NotificationService } from './notification.service'
import { NotificationRoutingModule } from './notification-routing.module'
import { NotificationMainComponent } from './notification-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule,
        NotificationRoutingModule
    ],
    exports: [],
    declarations: [
        NotificationListComponent,
        NotificationDetailComponent,
        NotificationRecipientComponent,
        NotificationMainComponent
    ],
    providers: [
        NotificationService,
        HttpService,
        AuthGuard
    ],
})
export class NotificationModule { }
