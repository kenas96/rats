import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class NotificationService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('webuser/notifications?' + query_string)
    }

    submit(data) {
        return this.service.post('webuser/notifications', data)
    }

    getUser(query_string) {
        return this.service.get('webuser/mobile?' + query_string)
    }
}