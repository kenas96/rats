import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { NotificationService } from '../notification.service'
import { Notification } from '../notification.model'
import { Alert } from '../../control/util'


@Component({
    selector: 'notification-list',
    templateUrl: './notification-list.component.html',
    styleUrls: ['./notification-list.component.css']
})

export class NotificationListComponent implements OnInit {
    constructor(
        private service: NotificationService,
        private fb: FormBuilder,
        private alert: Alert
    ) { }

    adminType: string = ''
    oldID: number = 0
    state: number = 1
    notification: Notification
    notificationForm: FormGroup
    lengthData: number = 0
    lengthDataInnerTable: number = 0
    displayCallback: Function
    getUserCallback: Function
    viewCallback: Function
    editCallback: Function
    deleteCallback: Function
    recipientCallback: Function

    data = {
        rows: []
    }

    dataInnerTable = {
        rows: []
    }

    dataRecipient = {
        recipient: []
    }

    dataSelected = {
        rows: []
    }

    option: any = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: false,
        disNext: false,
        order: { column: 'id', ascending: true }
    }

    optionInnerTable: any = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: false,
        disNext: false,
        order: { column: 'id', ascending: true },
        user_type: 2
    }

    columns = [
        { text: 'Sender', field: 'sender_by_name', value: 'sender_by_name' },
        { text: 'Subject', field: 'subject', value: 'subject' },
        { text: 'Recipient', field: 'NoSort', value: 'NoSort' },
        { text: 'Sent Date', field: 'insert_date', value: 'insert_date' },
    ]

    columnsInnerTable = [
        { text: 'Name', field: 'user_name', value: 'user_name' },
        { text: 'KTP', field: 'user_ktp', value: 'user_ktp' },
        { text: 'Handphone', field: 'user_handphone', value: 'user_handphone' },
        { text: 'Type', field: 'user_type', value: 'user_type' }
    ]

    listCriteria = [
        { text: 'Sender', value: 'sender_by_name' },
        { text: 'Subject', value: 'subject' }
    ]

    listCriteriaInnerTable = [
        { text: 'Name', value: 'user_name' },
        { text: 'KTP', value: 'user_ktp' },
        { text: 'Handphone', value: 'user_handphone' }
    ]

    permission: any = {
        create: true,
        view: true,
        update: false,
        delete: false,
        upload: false
    }

    permissionInnerTable: any = {
        create: false,
        view: false,
        update: false,
        delete: false,
        upload: false
    }

    CheckBtnPrevNext() {
        if (this.option.page === 1) {
            this.option.disPrev = true
        } else {
            this.option.disPrev = false
        }
        if ((this.option.page === 1 || this.option.page > 1) && this.lengthData <= this.option.pageSize) {
            this.option.disNext = true
        } else {
            this.option.disNext = false
        }
    }

    CheckBtnPrevNextInnerTable() {
        if (this.optionInnerTable.page === 1) {
            this.optionInnerTable.disPrev = true
        } else {
            this.optionInnerTable.disPrev = false
        }
        if ((this.optionInnerTable.page === 1 || this.optionInnerTable.page > 1) &&
            this.lengthDataInnerTable <= this.optionInnerTable.pageSize) {
            this.optionInnerTable.disNext = true
        } else {
            this.optionInnerTable.disNext = false
        }
    }

    ngOnInit() {
        this.notification = new Notification()
        this.notification.recipient = []
        this.displayCallback = this.displayData.bind(this)
        this.getUserCallback = this.getUser.bind(this)
        this.viewCallback = this.view.bind(this)
        this.editCallback = this.edit.bind(this)
        this.deleteCallback = this.remove.bind(this)
        this.recipientCallback = this.recipient.bind(this)
        this.notificationForm = this.fb.group({
            id: '',
            send_by_id: '',
            sender_by_name: '',
            subject: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(255)]],
            notification_message: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(255)]],
            recipient: '',
            recipient_type: ''
        })

        //admin permission validation
        let user = localStorage.getItem('auth-user')
        let decoded = atob(user)
        for (var i = 0; i < decoded.length; i++) {
            if (decoded[i] === ':') {
                this.adminType = decoded.substr(0, i)
                break
            }
        }

        //additional permission
        if (this.adminType === 'Reco Admin' || this.adminType === 'Warehouse Admin') {
            this.permission.create = false
        }
    }

    ngAfterViewInit() {
        this.displayData()
    }

    loadingAnimationGridStart() {
        document.getElementById('dataGrid').style.display = "none"
        document.getElementById('loadingGrid').style.display = "block"
    }

    loadingAnimationGridStop() {
        document.getElementById('dataGrid').style.display = "block"
        document.getElementById('loadingGrid').style.display = "none"
    }

    parseData() {
        for (let entry of this.data.rows) {
            entry['insert_date'] = new Date(entry['insert_date'] - 25200000).toLocaleDateString()
        }
    }

    parseDataInnerTable(data) {
        setTimeout(() => {
            for (var i = 0; i < this.dataSelected.rows.length; i++) {
                $('#checkbox' + this.dataSelected.rows[i].id).prop('checked', true);
            }
        }, 500)
    }

    displayData() {
        this.loadingAnimationGridStart()
        let tmp_query = [
            { param: "page_size", value: parseInt(this.option.pageSize) + 1 },
            { param: "page_offset", value: parseInt(this.option.pageOffset) },
            { param: "order_column", value: this.option.order.column },
            { param: "order_ascending", value: this.option.order.ascending }
        ]
        if (Object.keys(this.option.criteria).length > 0) {
            tmp_query.push({ param: this.option.criteria.text, value: this.option.criteria.value })
        }
        let que_str = tmp_query.map(function (el) {
            return el.param + '=' + el.value
        }).join('&')
        this.service.getPaging(que_str).then(
            res => {
                if (Array.isArray(res.data)) {
                    this.lengthData = res.data.length
                    this.data.rows = res.data
                    if (this.lengthData > this.option.pageSize) {
                        let idx = this.lengthData - 1
                        this.data.rows.splice(idx, 1)
                    }
                } else {
                    this.data.rows = [res.data]
                }
                this.CheckBtnPrevNext()
                this.parseData()
                this.loadingAnimationGridStop()
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
                this.loadingAnimationGridStop()
            }
        )
    }

    getUser() {
        this.alert.loading_start()
        let tmp_query = [
            { param: "page_size", value: parseInt(this.optionInnerTable.pageSize) + 1 },
            { param: "page_offset", value: parseInt(this.optionInnerTable.pageOffset) },
            { param: "order_column", value: this.optionInnerTable.order.column },
            { param: "order_ascending", value: this.optionInnerTable.order.ascending }
        ]
        if (Object.keys(this.optionInnerTable.criteria).length > 0) {
            tmp_query.push({ param: this.optionInnerTable.criteria.text, value: this.optionInnerTable.criteria.value });
        }
        tmp_query.push({ param: 'user_type_id', value: this.optionInnerTable.user_type })
        this.dataInnerTable.rows = []
        let que_str = tmp_query.map(function (el) {
            return el.param + '=' + el.value
        }).join('&')
        this.service.getUser(que_str).then(
            res => {
                if (Array.isArray(res.data)) {
                    this.dataInnerTable.rows = res.data
                    this.lengthDataInnerTable = res.data.length

                    if (this.lengthDataInnerTable > this.optionInnerTable.pageSize) {
                        let idx = this.lengthDataInnerTable - 1
                        this.dataInnerTable.rows.splice(idx, 1)
                    }
                } else {
                    this.dataInnerTable.rows = [res.data]
                }
                this.CheckBtnPrevNextInnerTable()
                this.parseDataInnerTable(this.dataInnerTable.rows)
                this.alert.loading_stop()
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
            }
        )
    }

    focusField() {
        setTimeout(() => {
            $("#subject").focus()
        }, 500)
    }

    populateData() {
        this.notificationForm.patchValue({
            id: this.notification.id,
            send_by_id: this.notification.send_by_id,
            sender_by_name: this.notification.sender_by_name,
            subject: this.notification.subject,
            notification_message: this.notification.notification_message,
            insert_date: this.notification.insert_date,
            recipient: this.notification.recipient
        })
    }

    resetOption() {
        this.optionInnerTable.page = 1
        this.optionInnerTable.pageOffset = 0
        this.optionInnerTable.pageSize = 10
        this.optionInnerTable.criteria = {}
        this.optionInnerTable.disPrev = false
        this.optionInnerTable.disNext = false
        this.optionInnerTable.order = { column: 'id', ascending: true }

        this.notificationForm.reset()
        $('#search_in').val("")
    }

    addNew() {
        this.resetOption()
        this.state = 1
        this.focusField()
        $('#modal-detail').modal()
        $('input:checkbox').prop('checked', false)
    }

    view(obj) {
        this.resetOption()
        this.state = 0
        this.notification = obj
        this.populateData()
        $('#modal-detail').modal()
    }

    recipient(obj) {
        this.notification = obj
        $('#modal-recipient').modal()
    }


    edit(obj) {

    }

    remove(obj) {

    }
}