import { Component, OnInit, Input, ViewChild } from '@angular/core'
import { FormGroup, AbstractControl } from '@angular/forms'
import { Notification, Recipient } from './../notification.model'
import { NotificationService } from './../notification.service'
import { Alert } from '../../control/util'
import 'rxjs/add/operator/debounceTime'
import swal from 'sweetalert2'

@Component({
    selector: 'notification-detail',
    templateUrl: './notification-detail.component.html',
    styleUrls: ['./notification-detail.component.css']
})

export class NotificationDetailComponent implements OnInit {
    constructor(
        private service: NotificationService,
        private alert: Alert
    ) { }

    @Input() id: string
    @Input() notification: Notification
    @Input() state: number
    @Input() displayData: Function
    @Input() getUser: Function
    @Input() notificationForm: FormGroup

    @Input() columns = []
    @Input() listCriteria = []
    @Input() data = {
        rows: []
    }
    @Input() dataSelected = {
        rows: []
    }
    @Input() option = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: null,
        disNext: null,
        order: { column: 'id', ascending: true },
        user_type: 2
    }
    @Input() permission = {}

    recipient: Recipient
    dataConverted = {
        rows: []
    }
    getUserCallback: Function
    subjectMessage: string
    notificationMessage: string

    private validationMessage = {
        required: "Please fill this field.",
        minlength: "Min. character allowed are 3 characters.",
        maxlength: "Max. character allowed are 255 characters.",
        string: "Please enter a valid character format.",
        pattern: "Please enter a valid emaill address."
    }

    ngOnInit() {
        const subjectControl = this.notificationForm.get('subject')
        subjectControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageSubject(subjectControl)
        )

        const notificationControl = this.notificationForm.get('notification_message')
        notificationControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageNotification(notificationControl)
        )

        const userTypeControl = this.notificationForm.get('recipient_type')
        userTypeControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageUserType(userTypeControl)
        )

        this.getUserCallback = this.getUser.bind(this)
        this.recipient = new Recipient
    }

    setMessageSubject(c: AbstractControl): void {
        this.subjectMessage = ''
        if ((c.touched || c.dirty) && c.errors) {
            this.subjectMessage = Object.keys(c.errors).map(key =>
                this.validationMessage[key]
            )[0]
        }

        if (this.state == 0) {
            this.notificationForm.get('subject').disable()
        } else {
            this.notificationForm.get('subject').enable()
        }
    }

    setMessageNotification(c: AbstractControl): void {
        this.notificationMessage = ''
        if ((c.touched || c.dirty) && c.errors) {
            this.notificationMessage = Object.keys(c.errors).map(key =>
                this.validationMessage[key]
            )[0]
        }

        if (this.state == 0) {
            this.notificationForm.get('notification_message').disable()
        } else {
            this.notificationForm.get('notification_message').enable()
        }
    }

    setMessageUserType(c: AbstractControl): void {
        if (this.state === 1) {
            if (this.notificationForm.get('recipient_type').value === 'mitra') {
                this.option.user_type = 2
            } else if (this.notificationForm.get('recipient_type').value === 'coordinator') {
                this.option.user_type = 3
            } else if (this.notificationForm.get('recipient_type').value === 'pic_warehouse') {
                this.option.user_type = 4
            }
        }
    }

    search() {
        this.dataSelected.rows = []
        let tmp_query_all = [
            { param: "page_size", value: 1000000 },
            { param: "page_offset", value: 0 },
            { param: "order_column", value: "id" },
            { param: "order_ascending", value: true }
        ]
        let que_str_all = tmp_query_all.map(function (el) {
            return el.param + '=' + el.value
        }).join('&')
        this.service.getUser(que_str_all).then(
            res => {
                for (var i = 0; i < res.data.length; i++) {
                    if (res.data[i].checked) {
                        this.dataSelected.rows.push(res.data[i])
                    }
                }
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
            }
        )
        this.getUser()
    }

    save() {
        swal({
            title: 'Are you sure?',
            text: "Your data will change permanently!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.alert.loading_start()
                let obj = Object.assign({}, this.notification, this.notificationForm.value)
                obj.user_type = undefined
                obj.id = undefined
                obj.send_by_id = undefined
                obj.sender_by_name = undefined
                obj.recipient_type = undefined
                obj.insert_date = undefined

                this.dataConverted.rows = []
                for (let data of this.dataSelected.rows) {
                    this.recipient = new Recipient
                    this.recipient.recipient_id = data['id']
                    this.recipient.recipient_name = data['user_name']
                    this.recipient.recipient_user_type_id = data['user_type_id']
                    this.recipient.recipient_user_type = data['user_type']
                    this.dataConverted.rows.push(this.recipient)
                }
                obj.recipient = this.dataConverted.rows
                this.service.submit(obj).then(
                    res => {
                        this.alert.success_alert('Send notification successed', this.id, this.displayData())
                    },
                    err => {
                        if (err.http_status === 422) {
                            this.alert.error_alert(err.message)
                        } else {
                            this.alert.warn_alert(err.message)
                        }
                    }
                );
            }
        })

    }
}