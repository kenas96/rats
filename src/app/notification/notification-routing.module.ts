import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { NotificationMainComponent } from './notification-main.component'
import { NotificationListComponent } from './list/notification-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: NotificationMainComponent,
        children: [
            {
                path: '',
                component: NotificationListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: NotificationListComponent,
                canActivate: [AuthGuard]
            },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class NotificationRoutingModule { }
