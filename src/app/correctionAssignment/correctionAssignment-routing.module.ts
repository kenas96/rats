import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { CorrectionAssignmentMainComponent } from './correctionAssignment-main.component'
import { CorrectionAssignmentListComponent } from './list/correctionAssignment-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: CorrectionAssignmentMainComponent,
        children: [
            {
                path: '',
                component: CorrectionAssignmentListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: CorrectionAssignmentListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class CorrectionAssignmentRoutingModule { }
