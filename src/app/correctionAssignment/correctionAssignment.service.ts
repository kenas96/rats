import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class CorrectionAssignmentService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('webuser/cancelassignment?' + query_string)
    }

    cancelAssignment(data, id) {
        return this.service.put('webuser/cancelassignment/' + id, data)
    }

}