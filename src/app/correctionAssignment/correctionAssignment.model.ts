export class CorrectionAssignment {
    id: number
    mitra_name: string
    mitra_ktp: string
    mitra_handphone: string
    unit_branch_name: string
    unit_area_name: string
    no_sk: string
    no_kontrak: string
    assignment_status: any
    warehouse_name: string
    warehouse_type: string
    warehouse_pic_name: string
    warehouse_pic_nik: string
    format_address: string
    koordinator_name: string
    koordinator_nik: string
    assigned_date: any
    serah_terima_date: any
    completed_date: any
    reason: string

    constructor(data: any = {}) {
        this.id = data.id
        this.mitra_name = data.mitra_name
        this.mitra_ktp = data.mitra_ktp
        this.mitra_handphone = data.mitra_handphone
        this.unit_branch_name = data.unit_branch_name
        this.unit_area_name = data.unit_area_name
        this.no_sk = data.no_sk
        this.no_kontrak = data.no_kontrak
        this.assignment_status = data.assignment_status
        this.warehouse_name = data.warehouse_name
        this.warehouse_type = data.warehouse_type
        this.warehouse_pic_name = data.warehouse_pic_name
        this.warehouse_pic_nik = data.warehouse_pic_nik
        this.format_address = data.format_address
        this.koordinator_name = data.koordinator_name
        this.koordinator_nik = data.koordinator_nik
        this.assigned_date = data.assigned_date
        this.serah_terima_date = data.serah_terima_date
        this.completed_date = data.completed_date
        this.reason = data.reason
    }
}

