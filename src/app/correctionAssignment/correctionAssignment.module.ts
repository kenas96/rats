import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { CorrectionAssignmentListComponent } from './list/correctionAssignment-list.component'
import { CorrectionAssignmentDetailComponent } from './detail/correctionAssignment-detail.component'
import { CorrectionAssignmentService } from './correctionAssignment.service'
import { CorrectionAssignmentRoutingModule } from './correctionAssignment-routing.module'
import { CorrectionAssignmentMainComponent } from './correctionAssignment-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Validator } from '../control/validator'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule,
        CorrectionAssignmentRoutingModule
    ],
    exports: [],
    declarations: [
        CorrectionAssignmentListComponent,
        CorrectionAssignmentDetailComponent,
        CorrectionAssignmentMainComponent
    ],
    providers: [
        CorrectionAssignmentService,
        HttpService,
        AuthGuard,
        Validator,
        Alert
    ],
})
export class CorrectionAssignmentModule { }
