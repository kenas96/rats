import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-correctionAssignment',
    template: '<router-outlet></router-outlet>'
})

export class CorrectionAssignmentMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}