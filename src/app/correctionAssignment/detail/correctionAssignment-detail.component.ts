import { Component, OnInit, Input } from '@angular/core'
import { FormGroup, AbstractControl } from '@angular/forms'
import { CorrectionAssignment } from './../correctionAssignment.model'
import { CorrectionAssignmentService } from './../correctionAssignment.service'
import { Alert } from '../../control/util'
import 'rxjs/add/operator/debounceTime'
import swal from 'sweetalert2'

@Component({
    selector: 'correctionAssignment-detail',
    templateUrl: './correctionAssignment-detail.component.html',
    styleUrls: ['./correctionAssignment-detail.component.css']
})

export class CorrectionAssignmentDetailComponent implements OnInit {
    constructor(
        private service: CorrectionAssignmentService,
        private alert: Alert,
    ) { }

    @Input() id: string
    @Input() correctionAssignment: CorrectionAssignment
    @Input() displayData: Function
    @Input() correctionAssignmentForm: FormGroup
    reasonMessage: string
    cancelAssignment: CorrectionAssignment

    private validationMessage = {
        required: "Please fill this field.",
        minlength: "Min. character allowed are 3 characters.",
        maxlength: "Max. character allowed are 255 characters."
    }

    ngOnInit() {
        const reasonControl = this.correctionAssignmentForm.get('reason')
        reasonControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageReason(reasonControl)
        )
    }

    setMessageReason(c: AbstractControl): void {
        this.reasonMessage = ''
        if ((c.touched || c.dirty) && c.errors) {
            this.reasonMessage = Object.keys(c.errors).map(key =>
                this.validationMessage[key]
            )[0]
        }
    }

    save() {
        swal({
            title: 'Are you sure?',
            text: "Your data will change permanently!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.alert.loading_start()
                this.cancelAssignment = new CorrectionAssignment
                this.cancelAssignment.id = this.correctionAssignment.id
                this.cancelAssignment.reason = this.correctionAssignmentForm.get('reason').value
                this.service.cancelAssignment(this.cancelAssignment, this.cancelAssignment.id).then(
                    res => {
                        if (res.meta.status) {
                            this.alert.success_alert('The assignment has been canceled', this.id, this.displayData())
                        }
                        else {
                            this.alert.error_alert(res.meta.message)
                        }
                    },
                    err => {
                        this.alert.warning_alert()
                    }
                )
            }
        })
    }
}