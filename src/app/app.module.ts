import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ControlModule } from './control/control.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { ForgotPasswordComponent } from './auth/forgotPassword/forgotPassword.component';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { HttpService } from './http.service';
import { LoginService } from './auth/login/login.service';
import { ForgotPasswordService } from './auth/forgotPassword/forgotPassword.service';
import { AuthGuard } from './pages/pages-guard.provider';
import { Alert } from './control/util';
import { Validator } from './control/validator';
import { APP_INITIALIZER } from '@angular/core';
import { AppConfig } from './config/app.config';


export function initConfig(config: AppConfig) {
  return () => config.load();
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ForgotPasswordComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    RouterModule,
    ControlModule
  ],
  providers: [
    HttpService,
    LoginService,
    ForgotPasswordService,
    AuthGuard,
    Alert,
    Validator,
    AppConfig,
    {
      provide: APP_INITIALIZER,
      useFactory: initConfig,
      deps: [AppConfig],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
