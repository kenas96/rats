import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-internalRecovery',
    template: '<router-outlet></router-outlet>'
})

export class InternalRecoveryMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}