import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { InternalRecoveryService } from '../internalRecovery.service'
import { InternalRecovery } from '../internalRecovery.model'
import { Validator } from '../../control/validator'
import { Alert } from '../../control/util'
import swal from 'sweetalert2'

@Component({
    selector: 'internalRecovery-list',
    templateUrl: './internalRecovery-list.component.html',
    styleUrls: ['./internalRecovery-list.component.css']
})

export class InternalRecoveryListComponent implements OnInit {
    constructor(
        private service: InternalRecoveryService,
        private fb: FormBuilder,
        private validator: Validator,
        private alert: Alert
    ) { }

    adminType: string = ''
    state: number = 1
    internalRecovery: InternalRecovery
    internalRecoveryForm: FormGroup
    lengthData: number = 0
    displayCallback: Function
    viewCallback: Function
    editCallback: Function
    deleteCallback: Function

    data = {
        rows: [],
        internal_recoveries: [
            {
                id: '',
                text: '',
                nik: '',
                email: '',
                handphone: '',
                position_id: ''
            }
        ],
    }

    option: any = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: false,
        disNext: false,
        order: { column: 'id', ascending: true }
    }

    columns = [
        { text: 'NIK', field: 'nik', value: 'nik' },
        { text: 'Name', field: 'name', value: 'name' },
        { text: 'Position', field: 'position', value: 'position_id' },
        { text: 'Handphone', field: 'handphone', value: 'handphone' },
        { text: 'Status', field: 'is_active_string', value: 'is_active' }
    ]

    listCriteria = [
        { text: 'NIK', value: 'nik' },
        { text: 'Name', value: 'name' },
        { text: 'Handphone', value: 'handphone' }
    ]

    permission: any = {
        create: true,
        view: true,
        update: true,
        delete: false,
        upload: false
    }

    CheckBtnPrevNext() {
        if (this.option.page === 1) {
            this.option.disPrev = true
        } else {
            this.option.disPrev = false
        }
        if ((this.option.page === 1 || this.option.page > 1) && this.lengthData <= this.option.pageSize) {
            this.option.disNext = true
        } else {
            this.option.disNext = false
        }
    }

    ngOnInit() {
        this.internalRecovery = new InternalRecovery()
        this.displayCallback = this.displayData.bind(this)
        this.viewCallback = this.view.bind(this)
        this.editCallback = this.edit.bind(this)
        this.deleteCallback = this.remove.bind(this)
        this.internalRecoveryForm = this.fb.group({
            id: '',
            position_id: ['', [Validators.required]],
            position: [{ value: '', disabled: true }, Validators.required],
            nik: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(16)]],
            name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
            handphone: ['', [Validators.required, this.validator.isNumber, Validators.minLength(10), Validators.maxLength(20)]],
            email: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')]],
            is_active: '',
            search_internal: ''
        })

        //admin permission validation
        let user = localStorage.getItem('auth-user')
        let decoded = atob(user)
        for (var i = 0; i < decoded.length; i++) {
            if (decoded[i] === ':') {
                this.adminType = decoded.substr(0, i)
                break
            }
        }
        if (this.adminType !== 'Super Admin') {
            this.permission.delete = false
        }

        //additional permission
        if (this.adminType === 'Reco Admin' || this.adminType === 'Warehouse Admin') {
            this.permission.update = false
            this.permission.create = false
        }
    }

    ngAfterViewInit() {
        this.displayData()
    }

    loadingAnimationGridStart() {
        document.getElementById('dataGrid').style.display = "none"
        document.getElementById('loadingGrid').style.display = "block"
    }

    loadingAnimationGridStop() {
        document.getElementById('dataGrid').style.display = "block"
        document.getElementById('loadingGrid').style.display = "none"
    }

    parseData(data) {
        for (let entry of data) {
            if (entry['is_active'] === true) {
                entry['is_active_string'] = "Active"
            } else {
                entry['is_active_string'] = "Not Active"
            }
            entry['position'] = entry['position_name'] || "-"
        }
    }

    displayData() {
        this.loadingAnimationGridStart()
        let tmp_query = [
            { param: "page_size", value: parseInt(this.option.pageSize) + 1 },
            { param: "page_offset", value: parseInt(this.option.pageOffset) },
            { param: "order_column", value: this.option.order.column },
            { param: "order_ascending", value: this.option.order.ascending }
        ]
        if (Object.keys(this.option.criteria).length > 0) {
            tmp_query.push({ param: this.option.criteria.text, value: this.option.criteria.value })
        }
        let que_str = tmp_query.map(function (el) {
            return el.param + '=' + el.value
        }).join('&')
        this.service.getPaging(que_str).then(
            res => {
                if (Array.isArray(res.data)) {
                    this.lengthData = res.data.length
                    this.data.rows = res.data
                    if (this.lengthData > this.option.pageSize) {
                        let idx = this.lengthData - 1
                        this.data.rows.splice(idx, 1)
                    }
                } else {
                    this.data.rows = [res.data]
                }
                this.CheckBtnPrevNext()
                this.parseData(this.data.rows)
                this.loadingAnimationGridStop()
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
                this.loadingAnimationGridStop()
            }
        )
        this.service.getAllInternal().then(
            res => {
                if (Array.isArray(res.data)) {
                    this.data.internal_recoveries = []
                    for (let internal of res.data) {
                        internal['text'] = internal['name']
                        this.data.internal_recoveries.push(internal)
                    }
                } else {
                    this.data.internal_recoveries = [res.data]
                }
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
            }
        )
    }

    focusField() {
        setTimeout(() => {
            $("#name").focus()
        }, 500)
    }

    populateData() {
        var stringActive: string
        if (this.internalRecovery.is_active === true) {
            stringActive = "active"
        } else {
            stringActive = "notactive"
        }

        this.internalRecoveryForm.patchValue({
            id: this.internalRecovery.id,
            position_id: this.internalRecovery.position_id,
            position: this.internalRecovery.position,
            nik: this.internalRecovery.nik,
            name: this.internalRecovery.name,
            handphone: this.internalRecovery.handphone,
            email: this.internalRecovery.email,
            is_active: stringActive,
            search_internal: this.internalRecovery.name
        })
    }

    addNew() {
        this.state = 1
        this.internalRecovery = new InternalRecovery()
        this.internalRecoveryForm.reset()
        this.focusField()
        $('#modal-detail').modal()
    }

    view(obj) {
        this.state = 0
        this.internalRecovery = obj
        this.internalRecoveryForm.reset()
        this.populateData()
        $('#modal-detail').modal()
    }

    edit(obj) {
        this.state = -1
        this.internalRecovery = JSON.parse(JSON.stringify(obj))
        this.internalRecoveryForm.reset()
        this.focusField()
        this.populateData()
        $('#modal-detail').modal()
    }

    remove(obj) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                this.alert.loading_start()
                this.service.delete(obj.id).then(
                    res => {
                        this.alert.success_alert('Your data has been removed!', '', this.displayData())
                    },
                    err => {
                        if (err.http_status === 422) {
                            this.alert.error_alert(err.message)
                        } else {
                            this.alert.warn_alert(err.message)
                        }
                    }
                )
            }
        })
    }
}
