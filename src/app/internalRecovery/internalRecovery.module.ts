import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { InternalRecoveryListComponent } from './list/internalRecovery-list.component'
import { InternalRecoveryDetailComponent } from './detail/internalRecovery-detail.component'
import { InternalRecoveryService } from './internalRecovery.service'
import { InternalRecoveryRoutingModule } from './internalRecovery-routing.module'
import { InternalRecoveryMainComponent } from './internalRecovery-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Validator } from '../control/validator'
import { Alert } from '../control/util'
import { SelectModule } from 'ng2-select'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule,
        InternalRecoveryRoutingModule,
        SelectModule
    ],
    exports: [],
    declarations: [
        InternalRecoveryListComponent,
        InternalRecoveryDetailComponent,
        InternalRecoveryMainComponent
    ],
    providers: [
        InternalRecoveryService,
        HttpService,
        AuthGuard,
        Validator,
        Alert
    ],
})
export class InternalRecoveryModule { }
