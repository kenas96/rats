import { Component, OnInit, Input } from '@angular/core'
import { FormGroup, AbstractControl } from '@angular/forms'
import { InternalRecovery } from './../internalRecovery.model'
import { InternalRecoveryService } from './../internalRecovery.service'
import { Alert } from '../../control/util'
import 'rxjs/add/operator/debounceTime'

@Component({
    selector: 'internalRecovery-detail',
    templateUrl: './internalRecovery-detail.component.html',
    styleUrls: ['./internalRecovery-detail.component.css']
})

export class InternalRecoveryDetailComponent implements OnInit {
    constructor(
        private service: InternalRecoveryService,
        private alert: Alert,
    ) { }

    @Input() id: string
    @Input() internalRecovery: InternalRecovery
    @Input() state: number
    @Input() displayData: Function
    @Input() internalRecoveryForm: FormGroup
    @Input() data = {
        rows: [],
        internal_recoveries: []
    }
    nameMessage: string
    nikMessage: string
    emailMessage: string
    handphoneMessage: string
    positionMessage: string
    selectPositionCallback: Function
    // selectedInternal: any

    private validationMessage = {
        required: "Please fill this field.",
        minlength: "Min. character allowed are 3 characters.",
        maxlength: "Max. character allowed are 50 characters.",
        string: "Please enter a valid character format.",
        pattern: "Please enter a valid email address."
    }

    private validationMessageNumber = {
        required: "Please fill this field.",
        minlength: "Min. character allowed are 10 characters.",
        maxlength: "Max. character allowed are 20 characters.",
        number: "Please enter a valid number format.",
    }

    private validationMessageNik = {
        required: "Please fill this field.",
        minlength: "Min. character allowed are 3 characters.",
        maxlength: "Max. character allowed are 16 characters.",
        string: "Please enter a valid character format.",
        pattern: "Please enter a valid email address."
    }

    // public selectedInternalRecovery(value: any): void {
    //     this.selectedInternal = this.data.internal_recoveries.find(x => x.id == value.id)
    //     this.internalRecoveryForm.patchValue({
    //         name: this.selectedInternal.name,
    //         nik: this.selectedInternal.nik,
    //         //email: this.selectedInternal.email,
    //         //handphone: this.selectedInternal.handphone,
    //         //position_id: this.selectedInternal.position_id,
    //         //position: this.selectedInternal.position_id
    //     })
    // }

    ngOnInit() {
        this.selectPositionCallback = this.selectPosition.bind(this)

        const nameControl = this.internalRecoveryForm.get('name')
        nameControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageName(nameControl)
        )

        const nikControl = this.internalRecoveryForm.get('nik')
        nikControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageNik(nikControl)
        )

        const handphoneControl = this.internalRecoveryForm.get('handphone')
        handphoneControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageHandphone(handphoneControl)
        )

        const emailControl = this.internalRecoveryForm.get('email')
        emailControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageEmail(emailControl)
        )

        const positionControl = this.internalRecoveryForm.get('position')
        positionControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessagePosition(positionControl)
        )
    }

    setMessageName(c: AbstractControl): void {
        this.nameMessage = ''
        if (this.state !== -1) {
            this.internalRecoveryForm.get('name').disable()
        } else {
            this.internalRecoveryForm.get('name').enable()
            if ((c.touched || c.dirty) && c.errors) {
                this.nameMessage = Object.keys(c.errors).map(key =>
                    this.validationMessage[key]
                )[0]
            }
        }
    }

    setMessageNik(c: AbstractControl): void {
        this.nikMessage = ''
        if (this.state !== -1) {
            this.internalRecoveryForm.get('nik').disable()
        } else {
            this.internalRecoveryForm.get('nik').enable()
            if ((c.touched || c.dirty) && c.errors) {
                this.nikMessage = Object.keys(c.errors).map(key =>
                    this.validationMessageNik[key]
                )[0]
            }
        }
    }

    setMessageEmail(c: AbstractControl): void {
        this.emailMessage = ''
        if (this.state == 0) {
            this.internalRecoveryForm.get('email').disable()
        } else {
            this.internalRecoveryForm.get('email').enable()
            if ((c.touched || c.dirty) && c.errors) {
                this.emailMessage = Object.keys(c.errors).map(key =>
                    this.validationMessage[key]
                )[0]
            }
        }
    }

    setMessageHandphone(c: AbstractControl): void {
        this.handphoneMessage = ''
        if (this.state == 0) {
            this.internalRecoveryForm.get('handphone').disable()
        } else {
            this.internalRecoveryForm.get('handphone').enable()
            if ((c.touched || c.dirty) && c.errors) {
                this.handphoneMessage = Object.keys(c.errors).map(key =>
                    this.validationMessageNumber[key]
                )[0]
            }
        }
    }

    setMessagePosition(c: AbstractControl): void {
        this.positionMessage = ''
        if (c.value === undefined) {
            this.positionMessage = "Please select position."
        }
    }

    showPosition() {
        $('#lookup-position').modal()
    }

    selectPosition(obj) {
        this.internalRecoveryForm.patchValue({
            position_id: obj.id,
            position: obj.name
        })
        $('#lookup-position').modal('hide')
    }

    removePosition() {
        this.internalRecoveryForm.patchValue({
            position_id: undefined,
            position: undefined
        })
    }

    search() {
        this.alert.loading_start()
        var name = this.internalRecoveryForm.get('search_internal').value
        var select: any = this.data.internal_recoveries.find(x => x.name.toLowerCase() == name.toLowerCase())

        if (select === undefined) {
            this.alert.error_alert("Data not found")
        } else {
            this.internalRecoveryForm.patchValue({
                name: select.name,
                nik: select.nik
            })
            this.alert.loading_stop()
        }
    }

    save() {
        this.alert.loading_start()
        let obj = Object.assign({}, this.internalRecovery, this.internalRecoveryForm.value)
        obj.is_active_status = undefined
        obj.position = undefined
        if (obj.is_active == "notactive") {
            obj.is_active = false
        } else {
            obj.is_active = true
        }
        if (this.state == 1) {
            //add
            obj.id = undefined
            // obj.name = this.selectedInternal.name
            // obj.nik = this.selectedInternal.nik
            //obj.email = this.selectedInternal.email
            //obj.handphone = this.selectedInternal.handphone
            this.service.insert(obj).then(
                res => {
                    this.alert.success_alert('Your data has been added', this.id, this.displayData())
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                    console.log(err)
                }
            )
        }
        else {
            //edit
            this.service.update(obj, obj.id).then(
                res => {
                    this.alert.success_alert('Your data has been saved', this.id, this.displayData())
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                    console.log(err)
                }
            )

        }
    }
}
