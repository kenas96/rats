export class InternalRecovery {
    id: number
    position_id: number
    position: string
    nik: string
    name: string
    handphone: string
    email: string
    is_active: boolean


    constructor(data: any = {}) {
        this.id = data.id
        this.position_id = data.position_id
        this.position = data.position
        this.nik = data.nik
        this.name = data.name
        this.handphone = data.handphone
        this.email = data.email
        this.is_active = data.is_active
    }
}
