import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { InternalRecoveryMainComponent } from './internalRecovery-main.component'
import { InternalRecoveryListComponent } from './list/internalRecovery-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: InternalRecoveryMainComponent,
        children: [
            {
                path: '',
                component: InternalRecoveryListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: InternalRecoveryListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class InternalRecoveryRoutingModule { }
