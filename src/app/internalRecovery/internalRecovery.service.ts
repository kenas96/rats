import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class InternalRecoveryService {

    constructor(private service: HttpService) { }

    getAllInternal() {
        return this.service.get('internal_recoveries')
    }

    getPaging(query_string) {
        return this.service.get('webuser/internalrecoveriesall?' + query_string)
    }

    insert(data) {
        return this.service.post('webuser/internalrecoveries', data)
    }

    update(data, id) {
        return this.service.put('webuser/internalrecoveries/' + id, data)
    }

    delete(id) {
        return this.service.delete('webuser/internalrecoveries/' + id)
    }

}