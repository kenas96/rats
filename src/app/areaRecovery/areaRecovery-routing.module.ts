import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { AreaRecoveryMainComponent } from './areaRecovery-main.component'
import { AreaRecoveryListComponent } from './list/areaRecovery-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: AreaRecoveryMainComponent,
        children: [
            {
                path: '',
                component: AreaRecoveryListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: AreaRecoveryListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class AreaRecoveryRoutingModule { }
