import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-areaRecovery',
    template: '<router-outlet></router-outlet>'
})

export class AreaRecoveryMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}