import { Component, OnInit, Input } from '@angular/core'
import { FormGroup, AbstractControl } from '@angular/forms'
import { AreaRecovery } from './../areaRecovery.model'
import { AreaRecoveryService } from './../areaRecovery.service'
import { Alert } from '../../control/util'
import { JwtHelper } from 'angular2-jwt'
import 'rxjs/add/operator/debounceTime'

@Component({
  selector: 'areaRecovery-detail',
  templateUrl: './areaRecovery-detail.component.html',
  styleUrls: ['./areaRecovery-detail.component.css']
})

export class AreaRecoveryDetailComponent implements OnInit {
  constructor(
    private service: AreaRecoveryService,
    private alert: Alert,
  ) { }

  @Input() id: string
  @Input() areaRecovery: AreaRecovery
  @Input() state: number
  @Input() displayData: Function
  @Input() areaRecoveryForm: FormGroup
  nameMessage: string
  areaMessage: string
  selectAreaCallback: Function
  jwtHelper: JwtHelper = new JwtHelper()

  private validationMessage = {
    required: "Please fill this field.",
    minlength: "Min. character allowed are 3 characters.",
    maxlength: "Max. character allowed are 50 characters.",
    string: "Please enter a valid character format.",
    pattern: "Please enter a valid emaill address."
  }

  ngOnInit() {
    this.selectAreaCallback = this.selectArea.bind(this)
    const nameControl = this.areaRecoveryForm.get('area_recovery_name')
    nameControl.valueChanges.debounceTime(100).subscribe(value =>
      this.setMessageName(nameControl)
    )

    const areaControl = this.areaRecoveryForm.get('area_name')
    areaControl.valueChanges.debounceTime(100).subscribe(value =>
      this.setMessageArea(areaControl)
    )
  }

  setMessageName(c: AbstractControl): void {
    this.nameMessage = ''
    if ((c.touched || c.dirty) && c.errors) {
      this.nameMessage = Object.keys(c.errors).map(key =>
        this.validationMessage[key]
      )[0]
    }

    if (this.state == 0) {
      this.areaRecoveryForm.get('area_recovery_name').disable()
    } else {
      this.areaRecoveryForm.get('area_recovery_name').enable()
    }
  }

  setMessageArea(c: AbstractControl): void {
    this.areaMessage = ''
    if (c.value === undefined) {
      this.areaMessage = "Please select area."
    }
  }

  showArea() {
    $('#lookup-area').modal()
  }

  selectArea(obj) {
    this.areaRecoveryForm.patchValue({
      area_id: obj.id,
      area_name: obj.name
    })
    $('#lookup-area').modal('hide')
  }

  removeArea() {
    this.areaRecoveryForm.patchValue({
      area_id: undefined,
      area_name: undefined
    })
  }

  save() {
    this.alert.loading_start()
    let obj = Object.assign({}, this.areaRecovery, this.areaRecoveryForm.value)
    obj.request_id = Date.now()
    obj.area_name = undefined
    obj.insert_by_name = undefined
    obj.update_by_name = undefined
    if (obj.is_active == "notactive") {
      obj.is_active = false
    } else {
      obj.is_active = true
    }
    if (this.state == 1) {
      //add
      let user = this.jwtHelper.decodeToken(localStorage.getItem('auth-token'))

      obj.insert_by = user.id
      obj.insert_date = Date.now()
      obj.id = undefined
      obj.name = this.areaRecoveryForm.value['area_recovery_name']
      obj.update_by = undefined
      obj.update_date = undefined
      this.service.insert(obj).then(
        res => {
          this.alert.success_alert('Your data has been added', this.id, this.displayData())
        },
        err => {
          if (err.http_status === 422) {
            this.alert.error_alert(err.message)
          } else {
            this.alert.warn_alert(err.message)
          }
        }
      )
    }
    else {
      //edit
      let user = this.jwtHelper.decodeToken(localStorage.getItem('auth-token'))

      obj.update_by = user.id
      obj.name = this.areaRecoveryForm.value['area_recovery_name']
      obj.update_date = Date.now()
      this.service.update(obj, obj.area_recovery_id).then(
        res => {
          this.alert.success_alert('Your data has been saved', this.id, this.displayData())
        },
        err => {
          if (err.http_status === 422) {
            this.alert.error_alert(err.message)
          } else {
            this.alert.warn_alert(err.message)
          }
        }
      )

    }
  }
}
