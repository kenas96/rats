import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { AreaRecoveryService } from '../areaRecovery.service'
import { AreaRecovery } from '../areaRecovery.model'
import { Alert } from '../../control/util'
import swal from 'sweetalert2'

@Component({
  selector: 'areaRecovery-list',
  templateUrl: './areaRecovery-list.component.html',
  styleUrls: ['./areaRecovery-list.component.css']
})

export class AreaRecoveryListComponent implements OnInit {
  constructor(
    private service: AreaRecoveryService,
    private fb: FormBuilder,
    private alert: Alert
  ) { }

  adminType: string = ''
  state: number = 1
  areaRecovery: AreaRecovery
  areaRecoveryForm: FormGroup
  lengthData: number = 0
  displayCallback: Function
  viewCallback: Function
  editCallback: Function
  deleteCallback: Function

  data = {
    rows: []
  }

  option: any = {
    page: 1,
    pageOffset: 0,
    pageSize: 10,
    criteria: {},
    disPrev: false,
    disNext: false,
    order: { column: 'area_recovery_id', ascending: true }
  }

  columns = [
    { text: 'ID', field: 'area_recovery_id', value: 'area_recovery_id' },
    { text: 'Name', field: 'area_recovery_name', value: 'area_recovery_name' },
    { text: 'Status', field: 'is_active_string', value: 'is_active' }
  ]

  listCriteria = [
    { text: 'Name', value: 'area_recovery_name' },
  ]

  permission: any = {
    create: true,
    view: true,
    update: true,
    delete: true,
    upload: false
  }

  CheckBtnPrevNext() {
    if (this.option.page === 1) {
      this.option.disPrev = true
    } else {
      this.option.disPrev = false
    }
    if ((this.option.page === 1 || this.option.page > 1) && this.lengthData <= this.option.pageSize) {
      this.option.disNext = true
    } else {
      this.option.disNext = false
    }
  }

  ngOnInit() {
    this.areaRecovery = new AreaRecovery()
    this.displayCallback = this.displayData.bind(this)
    this.viewCallback = this.view.bind(this)
    this.editCallback = this.edit.bind(this)
    this.deleteCallback = this.remove.bind(this)
    this.areaRecoveryForm = this.fb.group({
      request_id: '',
      area_recovery_id: '',
      area_recovery_name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      area_id: ['', [Validators.required]],
      area_name: [{ value: '', disabled: true }, Validators.required],
      is_active: ''
    })

    //admin permission validation
    let user = localStorage.getItem('auth-user')
    let decoded = atob(user)
    for (var i = 0; i < decoded.length; i++) {
      if (decoded[i] === ':') {
        this.adminType = decoded.substr(0, i)
        break
      }
    }
    if (this.adminType !== 'Super Admin') {
      this.permission.delete = false
    }

    //additional permission
    if (this.adminType === 'Reco Admin' || this.adminType === 'Warehouse Admin') {
      this.permission.update = false
      this.permission.create = false
    }
  }

  ngAfterViewInit() {
    this.displayData()
  }

  loadingAnimationGridStart() {
    document.getElementById('dataGrid').style.display = "none"
    document.getElementById('loadingGrid').style.display = "block"
  }

  loadingAnimationGridStop() {
    document.getElementById('dataGrid').style.display = "block"
    document.getElementById('loadingGrid').style.display = "none"
  }

  parseData() {
    for (let entry of this.data.rows) {
      if (entry['is_active'] === true) {
        entry['is_active_string'] = "Active"
      } else {
        entry['is_active_string'] = "Not Active"
      }
    }
  }

  displayData() {
    this.loadingAnimationGridStart()
    let tmp_query = [
      { param: "page_size", value: parseInt(this.option.pageSize) + 1 },
      { param: "page_offset", value: parseInt(this.option.pageOffset) },
      { param: "order_column", value: this.option.order.column },
      { param: "order_ascending", value: this.option.order.ascending }
    ]
    if (Object.keys(this.option.criteria).length > 0) {
      tmp_query.push({ param: this.option.criteria.text, value: this.option.criteria.value })
    }
    let que_str = tmp_query.map(function (el) {
      return el.param + '=' + el.value
    }).join('&')
    this.service.getPaging(que_str).
      then(
        res => {
          if (Array.isArray(res.data)) {
            this.lengthData = res.data.length
            this.data.rows = res.data
            if (this.lengthData > this.option.pageSize) {
              let idx = this.lengthData - 1
              this.data.rows.splice(idx, 1)
            }
          } else {
            this.data.rows = [res.data]
          }
          this.CheckBtnPrevNext()
          this.parseData()
          this.loadingAnimationGridStop()
        },
        err => {
          if (err.http_status === 422) {
            this.alert.error_alert(err.message)
          } else {
            this.alert.warn_alert(err.message)
          }
          this.loadingAnimationGridStop()
        }
      )
  }

  focusField() {
    setTimeout(() => {
      $("#arearecoveryname").focus()
    }, 500)
  }

  populateData() {
    var stringActive: string
    if (this.areaRecovery.is_active === true) {
      stringActive = "active"
    } else {
      stringActive = "notactive"
    }

    this.areaRecoveryForm.patchValue({
      request_id: this.areaRecovery.request_id,
      area_recovery_id: this.areaRecovery.area_recovery_id,
      area_recovery_name: this.areaRecovery.area_recovery_name,
      area_id: this.areaRecovery.area_id,
      area_name: this.areaRecovery.area_name,
      is_active: stringActive,
    })
  }

  addNew() {
    this.state = 1
    this.areaRecovery = new AreaRecovery()
    this.areaRecoveryForm.reset()
    this.focusField()
    $('#modal-detail').modal()
  }

  view(obj) {
    this.state = 0
    this.areaRecovery = obj
    this.areaRecoveryForm.reset()
    this.populateData()
    $('#modal-detail').modal()
  }

  edit(obj) {
    this.state = -1
    this.areaRecovery = JSON.parse(JSON.stringify(obj))
    this.areaRecoveryForm.reset()
    this.focusField()
    this.populateData()
    $('#modal-detail').modal()
  }

  remove(obj) {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.alert.loading_start()
        this.service.delete(obj.area_recovery_id).then(
          res => {
            this.alert.success_alert('Your data has been removed!', '', this.displayData())
          },
          err => {
            if (err.http_status === 422) {
              this.alert.error_alert(err.message)
            } else {
              this.alert.warn_alert(err.message)
            }
          }
        )
      }
    })
  }
}
