export class AreaRecovery {
  request_id: number
  area_recovery_id: number
  area_id: number
  area_recovery_name: string
  area_name: string
  is_active: boolean

  constructor(data: any = {}) {
    this.request_id = data.request_id
    this.area_recovery_id = data.id
    this.area_recovery_name = data.name
    this.area_id = data.area_id
    this.area_name = data.area_name
    this.is_active = data.is_active
  }
}
