import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class AreaRecoveryService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('area_recoveries?' + query_string)
    }

    getDetail(id) {
        return this.service.get('area_recoveries/' + id)
    }

    insert(data) {
        return this.service.post('area_recoveries', data)
    }

    update(data, id) {
        return this.service.put('area_recoveries/' + id, data)
    }

    delete(id) {
        return this.service.delete('area_recoveries/' + id)
    }

}