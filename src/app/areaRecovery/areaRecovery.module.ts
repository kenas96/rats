import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { AreaRecoveryListComponent } from './list/areaRecovery-list.component'
import { AreaRecoveryDetailComponent } from './detail/areaRecovery-detail.component'
import { AreaRecoveryService } from './areaRecovery.service'
import { AreaRecoveryRoutingModule } from './areaRecovery-routing.module'
import { AreaRecoveryMainComponent } from './areaRecovery-main.component'
import { AreaService } from './../area/area.service'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Validator } from '../control/validator'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule,
        AreaRecoveryRoutingModule
    ],
    exports: [],
    declarations: [
        AreaRecoveryListComponent,
        AreaRecoveryDetailComponent,
        AreaRecoveryMainComponent
    ],
    providers: [
        AreaRecoveryService,
        AreaService,
        HttpService,
        AuthGuard,
        Validator,
        Alert
    ],
})
export class AreaRecoveryModule { }
