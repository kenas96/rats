import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { ReportInternalRecoveryNonActiveListComponent } from './list/reportInternalRecoveryNonActive-list.component'
import { ReportInternalRecoveryNonActiveService } from './reportInternalRecoveryNonActive.service'
import { ReportInternalRecoveryNonActiveRoutingModule } from './reportInternalRecoveryNonActive-routing.module'
import { ReportInternalRecoveryNonActiveMainComponent } from './reportInternalRecoveryNonActive-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        HttpModule,
        RouterModule,
        ReportInternalRecoveryNonActiveRoutingModule
    ],
    exports: [],
    declarations: [
        ReportInternalRecoveryNonActiveListComponent,
        ReportInternalRecoveryNonActiveMainComponent
    ],
    providers: [
        ReportInternalRecoveryNonActiveService,
        HttpService,
        AuthGuard,
        Alert
    ],
})
export class ReportInternalRecoveryNonActiveModule { }
