import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-reportInternalRecoveryNonActive',
    template: '<router-outlet></router-outlet>'
})

export class ReportInternalRecoveryNonActiveMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}