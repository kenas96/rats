import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ReportInternalRecoveryNonActiveMainComponent } from './reportInternalRecoveryNonActive-main.component'
import { ReportInternalRecoveryNonActiveListComponent } from './list/reportInternalRecoveryNonActive-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: ReportInternalRecoveryNonActiveMainComponent,
        children: [
            {
                path: '',
                component: ReportInternalRecoveryNonActiveListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: ReportInternalRecoveryNonActiveListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class ReportInternalRecoveryNonActiveRoutingModule { }
