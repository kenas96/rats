import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { ReportInternalRecoveryListComponent } from './list/reportInternalRecovery-list.component'
import { ReportInternalRecoveryService } from './reportInternalRecovery.service'
import { ReportInternalRecoveryRoutingModule } from './reportInternalRecovery-routing.module'
import { ReportInternalRecoveryMainComponent } from './reportInternalRecovery-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        HttpModule,
        RouterModule,
        ReportInternalRecoveryRoutingModule
    ],
    exports: [],
    declarations: [
        ReportInternalRecoveryListComponent,
        ReportInternalRecoveryMainComponent
    ],
    providers: [
        ReportInternalRecoveryService,
        HttpService,
        AuthGuard,
        Alert
    ],
})
export class ReportInternalRecoveryModule { }
