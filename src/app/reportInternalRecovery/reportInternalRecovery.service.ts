import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class ReportInternalRecoveryService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('webuser/report/internalrecovery?' + query_string)
    }

    downloadReport(query_string) {
        return this.service.get('webuser/report/internalrecovery/generate?' + query_string)
    }

    downloadFullReport() {
        //maximum rows in excel
        return this.service.get('webuser/report/internalrecovery/generate?page_size=1048576')
    }

}