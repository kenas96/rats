import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ReportInternalRecoveryMainComponent } from './reportInternalRecovery-main.component'
import { ReportInternalRecoveryListComponent } from './list/reportInternalRecovery-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: ReportInternalRecoveryMainComponent,
        children: [
            {
                path: '',
                component: ReportInternalRecoveryListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: ReportInternalRecoveryListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class ReportInternalRecoveryRoutingModule { }
