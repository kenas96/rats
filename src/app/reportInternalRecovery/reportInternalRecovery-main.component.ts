import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-reportInternalRecovery',
    template: '<router-outlet></router-outlet>'
})

export class ReportInternalRecoveryMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}