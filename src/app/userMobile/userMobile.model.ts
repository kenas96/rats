export class UserMobile {
    request_id: number
    id: number
    action_user_type_id: string
    value_id: string
    user_type_id: number
    user_type: string
    user_name: string
    user_email: string
    user_ktp: string
    user_handphone: string
    user_username: string
    is_login: any

    constructor(data: any = {}) {
        this.request_id = data.request_id
        this.id = data.id
        this.action_user_type_id = data.user_type_id
        this.value_id = data.value_id
        this.user_type_id = data.user_type_id
        this.user_type = data.user_type
        this.user_name = data.user_name
        this.user_email = data.user_email
        this.user_ktp = data.user_ktp
        this.user_handphone = data.user_handphone
        this.user_username = data.user_username
        this.is_login = data.is_login
    }
}

export class UserMobileImage {
    file: any
    name: string
    username: string

    constructor(data: any = {}) {
        this.file = data.file
        this.name = data.name
        this.username = data.username
    }
}

