import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { UserMobileListComponent } from './list/userMobile-list.component'
import { UserMobileDetailComponent } from './detail/userMobile-detail.component'
import { UserMobileService } from './userMobile.service'
import { UserMobileRoutingModule } from './userMobile-routing.module'
import { UserMobileMainComponent } from './userMobile-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Validator } from '../control/validator'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule,
        UserMobileRoutingModule
    ],
    exports: [],
    declarations: [
        UserMobileListComponent,
        UserMobileDetailComponent,
        UserMobileMainComponent
    ],
    providers: [
        UserMobileService,
        HttpService,
        AuthGuard,
        Validator,
        Alert
    ],
})
export class UserMobileModule { }
