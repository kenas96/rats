import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { UserMobileMainComponent } from './userMobile-main.component'
import { UserMobileListComponent } from './list/userMobile-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: UserMobileMainComponent,
        children: [
            {
                path: '',
                component: UserMobileListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: UserMobileListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class UserMobileRoutingModule { }
