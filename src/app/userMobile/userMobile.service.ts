import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class UserMobileService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('webuser/mobile?' + query_string)
    }

    // forceLogout(data) {
    //     return this.service.post('mobileuser/logout', data)
    // }

    forceLogout(username, data) {
        return this.service.post('forcelogout/' + username, data)
    }

    postImage(data) {
        return this.service.postForm('mobileuser/picture', data)
    }

    unlock(phoneNumber, data) {
        return this.service.post('unlock/' + phoneNumber, data)
    }
}