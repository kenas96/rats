import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-userMobile',
    template: '<router-outlet></router-outlet>'
})

export class UserMobileMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}