import { Component, OnInit, Input } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { UserMobile, UserMobileImage } from './../userMobile.model'
import { UserMobileService } from './../userMobile.service'
import { Alert } from '../../control/util'
import 'rxjs/add/operator/debounceTime'
import swal from 'sweetalert2'
import { AppConfig } from '../../config/app.config';

@Component({
    selector: 'userMobile-detail',
    templateUrl: './userMobile-detail.component.html',
    styleUrls: ['./userMobile-detail.component.css']
})

export class UserMobileDetailComponent implements OnInit {
    constructor(
        private service: UserMobileService,
        private alert: Alert,
        private config: AppConfig) {
        this.base = this.config.getConfig('base_url')
    }

    @Input() id: string
    @Input() userMobile: UserMobile
    @Input() userMobileImage: UserMobileImage
    @Input() state: number
    @Input() displayData: Function
    @Input() userMobileForm: FormGroup
    base: string

    ngOnInit() {
    }

    onFileChange(event) {
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0]
            if (file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'image/jpg') {
                this.userMobileImage.file = file
                this.userMobileImage.name = file.name
                this.userMobileImage.username = this.userMobile.user_handphone
            } else {
                $("#imageUpload").val('')
                this.alert.warn_alert('Please input jpg/jpeg/png picture format')
                this.userMobileImage.file = undefined
                this.userMobileImage.name = undefined
                this.userMobileImage.username = undefined
            }
        }
    }

    initFormImage() {
        let input = new FormData()
        input.append('name', this.userMobileImage.name)
        input.append('file', this.userMobileImage.file)
        input.append('username', this.userMobileImage.username)
        return input
    }

    save() {
        this.alert.loading_start()
        this.service.postImage(this.initFormImage()).then(
            res => {
                this.alert.success_alert('Your picture has been saved', this.id, this.displayData())

            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
            }
        )
    }

    unlock() {
        if (this.userMobile.is_login === 'Active') {
            this.alert.warn_alert('User still login, please force logout first!')
        } else {
            swal({
                title: 'Are you sure?',
                text: "The device will be unlock!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
            }).then((result) => {
                if (result.value) {
                    this.alert.loading_start()
                    let userObj = new UserMobile()
                    this.service.unlock(this.userMobile.user_handphone, userObj).then(
                        res => {
                            if (res.meta.status) {
                                this.alert.success_alert('Unlock user successed', this.id, this.displayData())
                            }
                            else {
                                this.alert.error_alert(res.meta.message)
                            }
                        },
                        err => {
                            this.alert.warning_alert()
                        }
                    )
                }
            })
        }
    }

    forceLogout() {
        swal({
            title: 'Are you sure?',
            text: "The device will be logout shortly!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.alert.loading_start()
                let userObj = new UserMobile()
                userObj.request_id = Date.now()
                userObj.id = this.userMobile.id
                userObj.is_login = false
                // this.userMobile.request_id = Date.now()
                // this.userMobile.is_login = false
                this.service.forceLogout(this.userMobile.user_handphone, userObj).then(
                    res => {
                        if (res.meta.status) {
                            this.alert.success_alert('Force logout successed', this.id, this.displayData())
                        }
                        else {
                            this.alert.error_alert(res.meta.message)
                        }
                    },
                    err => {
                        this.alert.warning_alert()
                    }
                )
            }
        })
    }
}