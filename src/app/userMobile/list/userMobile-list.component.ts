import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { UserMobileService } from '../userMobile.service'
import { UserMobile, UserMobileImage } from '../userMobile.model'
import { Alert } from '../../control/util'

@Component({
  selector: 'userMobile-list',
  templateUrl: './userMobile-list.component.html',
  styleUrls: ['./userMobile-list.component.css']
})

export class UserMobileListComponent implements OnInit {
  constructor(
    private service: UserMobileService,
    private fb: FormBuilder,
    private alert: Alert
  ) { }

  adminType: string = ''
  state: number = 1
  userMobile: UserMobile
  userMobileImage: UserMobileImage
  userMobileForm: FormGroup
  lengthData: number = 0
  displayCallback: Function
  viewCallback: Function
  editCallback: Function
  deleteCallback: Function

  data = {
    rows: []
  }

  option: any = {
    page: 1,
    pageOffset: 0,
    pageSize: 10,
    criteria: {},
    disPrev: false,
    disNext: false,
    order: { column: 'id', ascending: true }
  }

  columns = [
    { text: 'Name', field: 'user_name', value: 'user_name' },
    { text: 'Type', field: 'user_type', value: 'user_type' },
    { text: 'Username', field: 'user_username', value: 'user_username' },
    { text: 'Email', field: 'user_email', value: 'user_email' },
    { text: 'Login Status', field: 'is_login', value: 'is_login' },
  ]

  listCriteria = [
    { text: 'Name', value: 'user_name' },
    { text: 'Handphone', value: 'user_handphone' },
    { text: 'Username', value: 'user_username' },
  ]

  permission: any = {
    create: false,
    view: true,
    update: true,
    delete: false,
    upload: false
  }

  CheckBtnPrevNext() {
    if (this.option.page === 1) {
      this.option.disPrev = true
    } else {
      this.option.disPrev = false
    }
    if ((this.option.page === 1 || this.option.page > 1) && this.lengthData <= this.option.pageSize) {
      this.option.disNext = true
    } else {
      this.option.disNext = false
    }
  }

  ngOnInit() {
    this.userMobile = new UserMobile()
    this.displayCallback = this.displayData.bind(this)
    this.viewCallback = this.view.bind(this)
    this.editCallback = this.edit.bind(this)
    this.deleteCallback = this.remove.bind(this)
    this.userMobileForm = this.fb.group({
      id: '',
      reason: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(255)]],
    })

    //admin permission validation
    let user = localStorage.getItem('auth-user')
    let decoded = atob(user)
    for (var i = 0; i < decoded.length; i++) {
      if (decoded[i] === ':') {
        this.adminType = decoded.substr(0, i)
        break
      }
    }

    //additional permission
    if (this.adminType === 'Reco Admin' || this.adminType === 'Warehouse Admin') {
      this.permission.update = false
    }
  }

  ngAfterViewInit() {
    this.displayData()
  }

  loadingAnimationGridStart() {
    document.getElementById('dataGrid').style.display = "none"
    document.getElementById('loadingGrid').style.display = "block"

  }

  loadingAnimationGridStop() {
    document.getElementById('dataGrid').style.display = "block"
    document.getElementById('loadingGrid').style.display = "none"

  }

  parseData() {
    for (let entry of this.data.rows) {
      if (entry['is_login']) {
        entry['is_login'] = 'Active'
      } else {
        entry['is_login'] = 'Not Active'
      }
    }
  }

  displayData() {
    this.loadingAnimationGridStart()
    let tmp_query = [
      { param: "page_size", value: parseInt(this.option.pageSize) + 1 },
      { param: "page_offset", value: parseInt(this.option.pageOffset) },
      { param: "order_column", value: this.option.order.column },
      { param: "order_ascending", value: this.option.order.ascending }
    ]
    if (Object.keys(this.option.criteria).length > 0) {
      tmp_query.push({ param: this.option.criteria.text, value: this.option.criteria.value })
    }
    let que_str = tmp_query.map(function (el) {
      return el.param + '=' + el.value
    }).join('&')
    this.service.getPaging(que_str).
      then(
        res => {
          if (Array.isArray(res.data)) {
            this.lengthData = res.data.length
            this.data.rows = res.data
            if (this.lengthData > this.option.pageSize) {
              let idx = this.lengthData - 1
              this.data.rows.splice(idx, 1)
            }
          } else {
            this.data.rows = [res.data]
          }
          this.CheckBtnPrevNext()
          this.parseData()
          this.loadingAnimationGridStop()
        },
        err => {
          if (err.http_status === 422) {
            this.alert.error_alert(err.message)
          } else {
            this.alert.warn_alert(err.message)
          }
          this.loadingAnimationGridStop()
        }
      )
  }

  populateData() {
  }

  addNew() {
  }

  view(obj) {
    this.state = 0
    this.userMobile = obj
    this.userMobileImage = new UserMobileImage()
    this.userMobileForm.reset()
    $("#imageUpload").val('')
    $('#modal-detail').modal()
  }

  edit(obj) {
    this.state = -1
    this.userMobile = obj
    this.userMobileImage = new UserMobileImage()
    this.userMobileForm.reset()
    $("#imageUpload").val('')
    $('#modal-detail').modal()
  }

  remove(obj) {
  }
}
