import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { ReportExternalRecoveryListComponent } from './list/reportExternalRecovery-list.component'
import { ReportExternalRecoveryService } from './reportExternalRecovery.service'
import { ReportExternalRecoveryRoutingModule } from './reportExternalRecovery-routing.module'
import { ReportExternalRecoveryMainComponent } from './reportExternalRecovery-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        HttpModule,
        RouterModule,
        ReportExternalRecoveryRoutingModule
    ],
    exports: [],
    declarations: [
        ReportExternalRecoveryListComponent,
        ReportExternalRecoveryMainComponent
    ],
    providers: [
        ReportExternalRecoveryService,
        HttpService,
        AuthGuard,
        Alert
    ],
})
export class ReportExternalRecoveryModule { }
