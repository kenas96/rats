import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-reportExternalRecovery',
    template: '<router-outlet></router-outlet>'
})

export class ReportExternalRecoveryMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}