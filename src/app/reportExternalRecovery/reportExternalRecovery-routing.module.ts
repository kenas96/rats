import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ReportExternalRecoveryMainComponent } from './reportExternalRecovery-main.component'
import { ReportExternalRecoveryListComponent } from './list/reportExternalRecovery-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: ReportExternalRecoveryMainComponent,
        children: [
            {
                path: '',
                component: ReportExternalRecoveryListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: ReportExternalRecoveryListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class ReportExternalRecoveryRoutingModule { }
