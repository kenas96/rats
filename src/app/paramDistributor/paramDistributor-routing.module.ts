import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ParamDistributorMainComponent } from './paramDistributor-main.component'
import { ParamDistributorListComponent } from './list/paramDistributor-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: ParamDistributorMainComponent,
        children: [
            {
                path: '',
                component: ParamDistributorListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: ParamDistributorListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class ParamDistributorRoutingModule { }
