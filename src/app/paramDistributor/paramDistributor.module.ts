import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { ParamDistributorListComponent } from './list/paramDistributor-list.component'
import { ParamDistributorDetailComponent } from './detail/paramDistributor-detail.component'
import { ParamDistributorContentDetailComponent } from './contentDetail/paramDistributor-contentDetail.component'
import { ParamDistributorService } from './paramDistributor.service'
import { ParamDistributorRoutingModule } from './paramDistributor-routing.module'
import { ParamDistributorMainComponent } from './paramDistributor-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Validator } from '../control/validator'
import { Alert } from '../control/util'
import { SelectModule } from 'ng2-select'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule,
        ParamDistributorRoutingModule,
        SelectModule
    ],
    exports: [],
    declarations: [
        ParamDistributorListComponent,
        ParamDistributorContentDetailComponent,
        ParamDistributorMainComponent,
        ParamDistributorDetailComponent
    ],
    providers: [
        ParamDistributorService,
        HttpService,
        AuthGuard,
        Validator,
        Alert
    ],
})
export class ParamDistributorModule { }
