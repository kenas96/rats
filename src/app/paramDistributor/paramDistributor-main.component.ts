import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-paramDistributor',
    template: '<router-outlet></router-outlet>'
})

export class ParamDistributorMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}