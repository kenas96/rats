import { Component, OnInit, Input, ViewChild } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { ParamDistributor, ParamDistributorContent } from './../paramDistributor.model'
import { ParamDistributorService } from './../paramDistributor.service'
import { Alert } from '../../control/util'
import 'rxjs/add/operator/debounceTime'
import swal from 'sweetalert2'

@Component({
    selector: 'paramDistributor-detail',
    templateUrl: './paramDistributor-detail.component.html',
    styleUrls: ['./paramDistributor-detail.component.css']
})

export class ParamDistributorDetailComponent implements OnInit {
    constructor(
        private service: ParamDistributorService,
        private alert: Alert,
        private fb: FormBuilder
    ) { }

    @Input() id: string
    @Input() paramDistributor: ParamDistributor
    @Input() state: number
    @Input() displayData: Function
    @Input() getDetail: Function
    @Input() paramDistributorForm: FormGroup

    @Input() columns = []
    @Input() listCriteria = []
    @Input() data = {
        rows: [],
        distributors: [],
        param_contents: []
    }
    @Input() dataSelected = {
        rows: []
    }
    @Input() option = {
        page: 1,
        pageOffset: 0,
        pageSize: 1000000,
        criteria: {},
        disPrev: null,
        disNext: null,
        order: { column: 'id', ascending: true }
    }

    paramDistributorContent: ParamDistributorContent
    paramDistributorContentForm: FormGroup
    viewCallback: Function
    editCallback: Function
    deleteCallback: Function
    getDetailCallback: Function
    searchCallback: Function
    stateContent: number = 0
    @ViewChild('distributorselect') public distributorselect: any

    @Input() permission = {}

    ngOnInit() {
        this.paramDistributor = new ParamDistributor()
        this.viewCallback = this.view.bind(this)
        this.editCallback = this.edit.bind(this)
        this.deleteCallback = this.remove.bind(this)
        this.getDetailCallback = this.getDetail.bind(this)
        this.searchCallback = this.search.bind(this)
        this.paramDistributorContentForm = this.fb.group({
            id: '',
            name: '',
            parameter_content_id: ['', [Validators.required]],
            parameter_content_name: [{ value: '', disabled: true }, [Validators.required]],
            parameter_content_description: [{ value: '', disabled: true }]
            //name: [{ value: '', disabled: true }],
        })
    }

    public selectedDistributor(value: any): void {
        this.paramDistributor.id = value.id
        this.paramDistributorForm.patchValue({
            search_distributor: this.paramDistributor.id
        })
    }

    search() {
        this.dataSelected.rows = []
        // let tmp_query_all = [
        //     { param: "page_size", value: 1000000 },
        //     { param: "page_offset", value: 0 },
        //     { param: "order_column", value: "id" },
        //     { param: "order_ascending", value: true }
        // ]
        // let que_str_all = tmp_query_all.map(function (el) {
        //     return el.param + '=' + el.value
        // }).join('&')
        // this.service.getDetail(que_str_all, this.paramDistributor.id).then(
        //     res => {
        //         for (var i = 0; i < res.data.receives.length; i++) {
        //             if (res.data.receives[i].checked) {
        //                 this.dataSelected.rows.push(res.data.receives[i])
        //             }
        //         }
        //     },
        //     err => {
        //         if (err.http_status === 422) {
        //             this.alert.error_alert(err.message)
        //         } else {
        //             this.alert.warn_alert(err.message)
        //         }
        //     }
        // )
        this.getDetail()
        this.distributorselect.active = []
        this.paramDistributorForm.patchValue({
            search_distributor: ''
        })
    }

    focusField() {
        setTimeout(() => {
            $("#contentname").focus()
        }, 500)
    }

    populateData() {
        this.paramDistributorContentForm.patchValue({
            id: this.paramDistributorContent.id,
            name: this.paramDistributorContent.name,
            parameter_content_id: this.paramDistributorContent.parameter_content_id,
            parameter_content_name: this.paramDistributorContent.parameter_content_name,
            parameter_content_description: this.paramDistributorContent.parameter_content_description
        })
    }

    addNew() {
        this.stateContent = 1
        this.paramDistributorContent = new ParamDistributorContent()
        this.paramDistributorContentForm.reset()
        this.focusField()
        $('#modal-contentDetail').modal()
    }

    view(obj) {
        this.stateContent = 0
        this.paramDistributorContent = obj
        this.paramDistributorContentForm.reset()
        this.populateData()
        $('#modal-contentDetail').modal()
    }

    edit(obj) {
        this.stateContent = -1
        this.paramDistributorContent = JSON.parse(JSON.stringify(obj))
        this.paramDistributorContentForm.reset()
        this.focusField()
        this.populateData()
        $('#modal-contentDetail').modal()
    }

    remove(obj) {

    }
    save() {
        swal({
            title: 'Are you sure?',
            text: "Your data will change permanently!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.alert.loading_start()
                let obj = Object.assign({}, this.paramDistributor, this.paramDistributorForm.value)
                obj.search_nik = undefined
                obj.request_id = Date.now()
                obj.receives = this.dataSelected.rows

                var content_cek = obj.receives.find(x => x.parameter_content_id == 0)
                if (content_cek == undefined) {
                    this.service.submit(obj).then(
                        res => {
                            this.alert.success_alert('Your data has been saved', this.id, this.displayData())
                        },
                        err => {
                            if (err.http_status === 422) {
                                this.alert.error_alert(err.message)
                            } else {
                                this.alert.warn_alert(err.message)
                            }
                        }
                    )
                } else {
                    this.alert.loading_stop()
                    this.alert.error_alert("Please fill the content name!")
                }

            }
        })

    }
}