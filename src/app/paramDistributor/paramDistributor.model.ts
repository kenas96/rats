export class ParamDistributor {
    id: string
    name: string

    constructor(data: any = {}) {
        this.id = data.id
        this.name = data.name
    }
}

export class ParamDistributorContent {
    id: string
    name: string
    parameter_content_id: number
    parameter_content_name: string
    parameter_content_description: string

    constructor(data: any = {}) {
        this.id = data.id
        this.name = data.name
        this.parameter_content_id = data.parameter_content_id
        this.parameter_content_name = data.parameter_content_name
        this.parameter_content_description = data.parameter_content_description
    }
}