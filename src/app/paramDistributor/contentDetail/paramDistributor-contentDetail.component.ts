import { Component, OnInit, Input, ViewChild } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { ParamDistributorContent } from './../paramDistributor.model'
import { ParamDistributorService } from './../paramDistributor.service'
import { Alert } from '../../control/util'
import 'rxjs/add/operator/debounceTime'

@Component({
    selector: 'paramDistributor-contentDetail',
    templateUrl: './paramDistributor-contentDetail.component.html',
    styleUrls: ['./paramDistributor-contentDetail.component.css']
})

export class ParamDistributorContentDetailComponent implements OnInit {
    constructor(
        private service: ParamDistributorService,
        private alert: Alert,
    ) { }

    @Input() id: string
    @Input() paramDistributorContent: ParamDistributorContent
    @Input() stateContent: number
    @Input() displayData: Function
    @Input() search: Function
    @Input() data = {
        rows: [],
        distributors: [],
        param_contents: []
    }
    @Input() paramDistributorContentForm: FormGroup
    @ViewChild('contentselect') public contentselect: any

    ngOnInit() {
    }

    public selectedContent(value: any): void {
        this.paramDistributorContentForm.patchValue({
            parameter_content_id: value.id,
            parameter_content_name: value.text
        })
    }

    save() {
        this.alert.loading_start()
        let obj = Object.assign({}, this.paramDistributorContent, this.paramDistributorContentForm.value)
        obj.is_active_status = undefined
        obj.request_id = Date.now()

        if (this.stateContent == 1) {
            //add
            obj.id = undefined
            this.service.insertContent(obj).then(
                res => {
                    this.alert.success_alert('Your data has been added', this.id, this.displayData())
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                }
            )
        }
        else {
            //edit
            this.service.updateContent(obj, obj.id).then(
                res => {
                    this.contentselect.active = []
                    this.paramDistributorContentForm.patchValue({
                        parameter_content_id: '',
                        parameter_content_name: ''
                    })
                    this.alert.success_alert('Your data has been saved', this.id, this.search())
                    this.displayData()
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                }
            )
        }
    }
}