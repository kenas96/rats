import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class ParamDistributorService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('webuser/parameterdistributors?' + query_string)
    }

    getDetail(query_string, id) {
        return this.service.get('webuser/parameterdistributors/' + id + '?' + query_string)
    }

    submit(data) {
        return this.service.post('webuser/parameterdistributors', data)
    }

    insertContent(data) {
        return this.service.post('webuser/parameterdistributors', data)
    }

    updateContent(data, id) {
        return this.service.put('webuser/parameterdistributors/' + id, data)
    }

    getParamContents() {
        return this.service.get('webuser/parametercontents')
    }
}