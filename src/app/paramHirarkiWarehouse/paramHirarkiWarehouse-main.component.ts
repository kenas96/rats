import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-paramHirarkiWarehouse',
    template: '<router-outlet></router-outlet>'
})

export class ParamHirarkiWarehouseMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}