import { Component, OnInit, Input, ViewChild } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { ParamHirarkiWarehouse, ParamHirarkiWarehouseContent } from './../paramHirarkiWarehouse.model'
import { ParamHirarkiWarehouseService } from './../paramHirarkiWarehouse.service'
import { Alert } from '../../control/util'
import 'rxjs/add/operator/debounceTime'
import swal from 'sweetalert2'

@Component({
    selector: 'paramHirarkiWarehouse-detail',
    templateUrl: './paramHirarkiWarehouse-detail.component.html',
    styleUrls: ['./paramHirarkiWarehouse-detail.component.css']
})

export class ParamHirarkiWarehouseDetailComponent implements OnInit {
    constructor(
        private service: ParamHirarkiWarehouseService,
        private alert: Alert
    ) { }

    @Input() id: string
    @Input() paramHirarkiWarehouse: ParamHirarkiWarehouse
    @Input() state: number
    @Input() displayData: Function
    @Input() getDetail: Function
    @Input() paramHirarkiWarehouseForm: FormGroup

    @Input() columns = []
    @Input() listCriteria = []
    @Input() data = {
        rows: [],
        internal_recoveries: []
    }
    @Input() dataSelected = {
        rows: []
    }
    @Input() option = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: null,
        disNext: null,
        order: { column: 'id', ascending: true }
    }

    paramHirarkiWarehouseContent: ParamHirarkiWarehouseContent
    viewCallback: Function
    editCallback: Function
    deleteCallback: Function
    getDetailCallback: Function
    // @ViewChild('internalrecoveryselect') public internalrecoveryselect: any

    @Input() permission = {}

    ngOnInit() {
        this.paramHirarkiWarehouseContent = new ParamHirarkiWarehouseContent()
        this.viewCallback = this.view.bind(this)
        this.editCallback = this.edit.bind(this)
        this.deleteCallback = this.remove.bind(this)
        this.getDetailCallback = this.getDetail.bind(this)
    }

    // public selectedInternalRecovery(value: any): void {
    //     this.paramHirarkiWarehouse.id = value.id
    //     this.paramHirarkiWarehouseForm.patchValue({
    //         search_internal_recovery: this.paramHirarkiWarehouse.id
    //     })
    // }

    search() {
        this.dataSelected.rows = []
        let tmp_query_all = [
            { param: "page_size", value: 1000000 },
            { param: "page_offset", value: 0 },
            { param: "order_column", value: "id" },
            { param: "order_ascending", value: true }
        ]
        let que_str_all = tmp_query_all.map(function (el) {
            return el.param + '=' + el.value
        }).join('&')
        var name = this.paramHirarkiWarehouseForm.get('search_internal_recovery').value
        var select: any = this.data.internal_recoveries.find(x => x.name.toLowerCase() == name.toLowerCase())
        if (select === undefined) {
            this.alert.error_alert("Data not found")
        } else {
            this.paramHirarkiWarehouse.id = select.id
            this.service.getDetail(que_str_all, this.paramHirarkiWarehouse.id).then(
                res => {
                    for (var i = 0; i < res.data.internal_recoveries.length; i++) {
                        if (res.data.internal_recoveries[i].checked) {
                            this.dataSelected.rows.push(res.data.internal_recoveries[i])
                        }
                    }
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                }
            )
            this.getDetail()
            //this.internalrecoveryselect.active = []
            this.paramHirarkiWarehouseForm.patchValue({
                search_internal_recovery: ''
            })
        }
    }

    view(obj) {
        this.paramHirarkiWarehouseContent = obj
        $('#modal-contentDetail').modal()
    }

    edit(obj) {

    }

    remove(obj) {

    }
    save() {
        swal({
            title: 'Are you sure?',
            text: "Your data will change permanently!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.alert.loading_start()
                let obj = Object.assign({}, this.paramHirarkiWarehouse, this.paramHirarkiWarehouseForm.value)
                obj.search_internal = undefined
                obj.request_id = Date.now()
                obj.internal_recoveries = this.dataSelected.rows
                this.service.submit(obj).then(
                    res => {
                        this.alert.success_alert('Your data has been saved', this.id, this.displayData())
                    },
                    err => {
                        if (err.http_status === 422) {
                            this.alert.error_alert(err.message)
                        } else {
                            this.alert.warn_alert(err.message)
                        }
                    }
                )
            }
        })

    }
}