import { Component, OnInit, Input } from '@angular/core'
import { ParamHirarkiWarehouseContent } from './../paramHirarkiWarehouse.model'

@Component({
    selector: 'paramHirarkiWarehouse-contentDetail',
    templateUrl: './paramHirarkiWarehouse-contentDetail.component.html',
    styleUrls: ['./paramHirarkiWarehouse-contentDetail.component.css']
})

export class ParamHirarkiWarehouseContentDetailComponent implements OnInit {
    constructor() { }

    @Input() id: string
    @Input() paramHirarkiWarehouseContent: ParamHirarkiWarehouseContent

    ngOnInit() {

    }
}