import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ParamHirarkiWarehouseMainComponent } from './paramHirarkiWarehouse-main.component'
import { ParamHirarkiWarehouseListComponent } from './list/paramHirarkiWarehouse-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: ParamHirarkiWarehouseMainComponent,
        children: [
            {
                path: '',
                component: ParamHirarkiWarehouseListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: ParamHirarkiWarehouseListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class ParamHirarkiWarehouseRoutingModule { }
