import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { ParamHirarkiWarehouseListComponent } from './list/paramHirarkiWarehouse-list.component'
import { ParamHirarkiWarehouseDetailComponent } from './detail/paramHirarkiWarehouse-detail.component'
import { ParamHirarkiWarehouseContentDetailComponent } from './contentDetail/paramHirarkiWarehouse-contentDetail.component'
import { ParamHirarkiWarehouseService } from './paramHirarkiWarehouse.service'
import { ParamHirarkiWarehouseRoutingModule } from './paramHirarkiWarehouse-routing.module'
import { ParamHirarkiWarehouseMainComponent } from './paramHirarkiWarehouse-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Validator } from '../control/validator'
import { Alert } from '../control/util'
import { SelectModule } from 'ng2-select'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule,
        ParamHirarkiWarehouseRoutingModule,
        SelectModule
    ],
    exports: [],
    declarations: [
        ParamHirarkiWarehouseListComponent,
        ParamHirarkiWarehouseContentDetailComponent,
        ParamHirarkiWarehouseMainComponent,
        ParamHirarkiWarehouseDetailComponent
    ],
    providers: [
        ParamHirarkiWarehouseService,
        HttpService,
        AuthGuard,
        Validator,
        Alert
    ],
})
export class ParamHirarkiWarehouseModule { }
