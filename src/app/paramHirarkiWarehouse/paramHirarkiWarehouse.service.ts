import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class ParamHirarkiWarehouseService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('webuser/hierarchywarehouse?' + query_string)
    }

    getInternalRecovery() {
        return this.service.get('webuser/internalrecoveries?is_active=true&is_warehouse=true')
    }

    getDetail(query_string, id) {
        return this.service.get('webuser/hierarchywarehouse/' + id + '?' + query_string)
    }

    submit(data) {
        return this.service.post('webuser/hierarchywarehouse', data)
    }

}
