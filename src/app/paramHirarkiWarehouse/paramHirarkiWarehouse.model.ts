export class ParamHirarkiWarehouse {
    id: number
    manager_id: number
    nik: string
    name: string
    handphone: string
    email: string
    is_active: boolean

    constructor(data: any = {}) {
        this.id = data.id
        this.manager_id = data.manager_id
        this.nik = data.nik
        this.name = data.name
        this.handphone = data.handphone
        this.email = data.email
        this.is_active = data.is_active
    }
}

export class ParamHirarkiWarehouseContent {
    id: number
    manager_id: number
    nik: string
    name: string
    handphone: string
    email: string
    is_active: boolean
    position_name: string

    constructor(data: any = {}) {
        this.id = data.id
        this.manager_id = data.manager_id
        this.nik = data.nik
        this.name = data.name
        this.handphone = data.handphone
        this.email = data.email
        this.is_active = data.is_active
        this.position_name = data.position_name
    }
}
