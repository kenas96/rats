import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class ReportParameterWarehouseService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('webuser/report/parameterwarehouse?' + query_string)
    }

    downloadReport(query_string) {
        return this.service.get('webuser/report/parameterwarehouse/generate?' + query_string)
    }

    downloadFullReport() {
        //maximum rows in excel
        return this.service.get('webuser/report/parameterwarehouse/generate?page_size=1048576')
    }
}