import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ReportParameterWarehouseMainComponent } from './reportParameterWarehouse-main.component'
import { ReportParameterWarehouseListComponent } from './list/reportParameterWarehouse-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: ReportParameterWarehouseMainComponent,
        children: [
            {
                path: '',
                component: ReportParameterWarehouseListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: ReportParameterWarehouseListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class ReportParameterWarehouseRoutingModule { }
