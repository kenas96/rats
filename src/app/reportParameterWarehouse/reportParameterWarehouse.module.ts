import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { ReportParameterWarehouseListComponent } from './list/reportParameterWarehouse-list.component'
import { ReportParameterWarehouseService } from './reportParameterWarehouse.service'
import { ReportParameterWarehouseRoutingModule } from './reportParameterWarehouse-routing.module'
import { ReportParameterWarehouseMainComponent } from './reportParameterWarehouse-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        HttpModule,
        RouterModule,
        ReportParameterWarehouseRoutingModule
    ],
    exports: [],
    declarations: [
        ReportParameterWarehouseListComponent,
        ReportParameterWarehouseMainComponent
    ],
    providers: [
        ReportParameterWarehouseService,
        HttpService,
        AuthGuard,
        Alert
    ],
})
export class ReportParameterWarehouseModule { }
