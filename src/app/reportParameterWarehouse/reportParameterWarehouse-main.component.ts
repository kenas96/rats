import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-reportParameterWarehouse',
    template: '<router-outlet></router-outlet>'
})

export class ReportParameterWarehouseMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}