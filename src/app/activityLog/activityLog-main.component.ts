import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-activityLog',
    template: '<router-outlet></router-outlet>'
})

export class ActivityLogMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}