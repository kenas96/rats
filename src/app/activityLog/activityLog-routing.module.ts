import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ActivityLogMainComponent } from './activityLog-main.component'
import { ActivityLogListComponent } from './list/activityLog-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: ActivityLogMainComponent,
        children: [
            {
                path: '',
                component: ActivityLogListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: ActivityLogListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class ActivityLogRoutingModule { }
