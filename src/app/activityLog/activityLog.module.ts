import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { ActivityLogListComponent } from './list/activityLog-list.component'
import { ActivityLogService } from './activityLog.service'
import { ActivityLogRoutingModule } from './activityLog-routing.module'
import { ActivityLogMainComponent } from './activityLog-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule,
        ActivityLogRoutingModule
    ],
    exports: [],
    declarations: [
        ActivityLogListComponent,
        ActivityLogMainComponent
    ],
    providers: [
        ActivityLogService,
        HttpService,
        AuthGuard,
        Alert
    ],
})
export class ActivityLogModule { }
