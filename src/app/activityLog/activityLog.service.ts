import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class ActivityLogService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('activitylogs?' + query_string)
    }

}