import { Component, OnInit, Input } from '@angular/core'
import { HirarkiExternalRecoveryContent } from './../hirarkiExternalRecovery.model'

@Component({
    selector: 'hirarkiExternalRecovery-contentDetail',
    templateUrl: './hirarkiExternalRecovery-contentDetail.component.html',
    styleUrls: ['./hirarkiExternalRecovery-contentDetail.component.css']
})

export class HirarkiExternalRecoveryContentDetailComponent implements OnInit {
    constructor() { }

    @Input() id: string
    @Input() hirarkiExternalRecoveryContent: HirarkiExternalRecoveryContent

    ngOnInit() {

    }
}