import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-hirarkiExternalRecovery',
    template: '<router-outlet></router-outlet>'
})

export class HirarkiExternalRecoveryMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}