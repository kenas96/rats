export class HirarkiExternalRecovery {
    id: number
    name: string
    nik: string
    position_id: string
    position_name: string
    handphone: string
    email: string
    is_active: boolean

    constructor(data: any = {}) {
        this.id = data.id
        this.position_id = data.position_id
        this.position_name = data.position_name
        this.nik = data.nik
        this.name = data.name
        this.handphone = data.handphone
        this.email = data.email
        this.is_active = data.is_active
    }
}

export class HirarkiExternalRecoveryContent {
    id: number
    name: string
    nik: string
    position_id: string
    position_name: string
    handphone: string
    email: string
    is_active: boolean
    category: string
    pic_name: string
    pic_handphone: string
    start_date: any
    end_date: any

    constructor(data: any = {}) {
        this.id = data.id
        this.position_id = data.position_id
        this.position_name = data.position_name
        this.nik = data.nik
        this.name = data.name
        this.handphone = data.handphone
        this.email = data.email
        this.is_active = data.is_active
        this.category = data.category
        this.pic_name = data.pic_name
        this.pic_handphone = data.pic_handphone
        this.start_date = data.start_date
        this.end_date = data.end_date
    }
}

