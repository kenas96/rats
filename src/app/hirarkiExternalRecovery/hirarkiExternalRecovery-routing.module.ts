import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { HirarkiExternalRecoveryMainComponent } from './hirarkiExternalRecovery-main.component'
import { HirarkiExternalRecoveryListComponent } from './list/hirarkiExternalRecovery-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: HirarkiExternalRecoveryMainComponent,
        children: [
            {
                path: '',
                component: HirarkiExternalRecoveryListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: HirarkiExternalRecoveryListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class HirarkiExternalRecoveryRoutingModule { }
