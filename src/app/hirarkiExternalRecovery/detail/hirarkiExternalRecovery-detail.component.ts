import { Component, OnInit, Input, ViewChild } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { HirarkiExternalRecovery, HirarkiExternalRecoveryContent } from './../hirarkiExternalRecovery.model'
import { HirarkiExternalRecoveryService } from './../hirarkiExternalRecovery.service'
import { Alert } from '../../control/util'
import 'rxjs/add/operator/debounceTime'
import swal from 'sweetalert2'

@Component({
    selector: 'hirarkiExternalRecovery-detail',
    templateUrl: './hirarkiExternalRecovery-detail.component.html',
    styleUrls: ['./hirarkiExternalRecovery-detail.component.css']
})

export class HirarkiExternalRecoveryDetailComponent implements OnInit {
    constructor(
        private service: HirarkiExternalRecoveryService,
        private alert: Alert
    ) { }

    @Input() id: string
    @Input() hirarkiExternalRecovery: HirarkiExternalRecovery
    @Input() state: number
    @Input() displayData: Function
    @Input() getDetail: Function
    @Input() hirarkiExternalRecoveryForm: FormGroup

    @Input() columns = []
    @Input() listCriteria = []
    @Input() data = {
        rows: [],
        //internal_recovery: []
    }
    @Input() dataSelected = {
        rows: []
    }
    @Input() option = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: null,
        disNext: null,
        order: { column: 'name', ascending: true }
    }

    hirarkiExternalRecoveryContent: HirarkiExternalRecoveryContent
    viewCallback: Function
    editCallback: Function
    deleteCallback: Function
    getDetailCallback: Function

    @Input() permission = {}
    // @ViewChild('internalrecoveryselect') public internalrecoveryselect: any

    // public selectedInternalRecovery(value: any): void {
    //     this.hirarkiExternalRecoveryForm.patchValue({
    //         search_nik: value.text
    //     })
    // }

    search() {
        this.dataSelected.rows = []
        let tmp_query_all = [
            { param: "page_size", value: 1000000 },
            { param: "page_offset", value: 0 },
            { param: "order_column", value: "name" },
            { param: "order_ascending", value: true }
        ]
        let que_str_all = tmp_query_all.map(function (el) {
            return el.param + '=' + el.value
        }).join('&')
        this.service.getDetail(que_str_all, this.hirarkiExternalRecoveryForm.get('search_nik').value).then(
            res => {
                for (var i = 0; i < res.data.mous.length; i++) {
                    if (res.data.mous[i].checked) {
                        this.dataSelected.rows.push(res.data.mous[i])
                    }
                }
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
            }
        )
        this.getDetail()
        //this.internalrecoveryselect.active = []
        this.hirarkiExternalRecoveryForm.patchValue({
            search_nik: ''
        })
    }

    ngOnInit() {
        this.hirarkiExternalRecoveryContent = new HirarkiExternalRecoveryContent()
        this.viewCallback = this.view.bind(this)
        this.editCallback = this.edit.bind(this)
        this.deleteCallback = this.remove.bind(this)
        this.getDetailCallback = this.getDetail.bind(this)
    }

    view(obj) {
        this.hirarkiExternalRecoveryContent = obj
        $('#modal-contentDetail').modal()
    }

    edit(obj) {

    }

    remove(obj) {

    }
    save() {
        swal({
            title: 'Are you sure?',
            text: "Your data will change permanently!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.alert.loading_start()
                let obj = Object.assign({}, this.hirarkiExternalRecovery, this.hirarkiExternalRecoveryForm.value)
                obj.search_nik = undefined
                obj.request_id = Date.now()
                obj.mous = this.dataSelected.rows
                this.service.submit(obj).then(
                    res => {
                        this.alert.success_alert('Your data has been saved', this.id, this.displayData())
                    },
                    err => {
                        if (err.http_status === 422) {
                            this.alert.error_alert(err.message)
                        } else {
                            this.alert.warn_alert(err.message)
                        }
                    }
                )
            }
        })

    }
}