import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { HirarkiExternalRecoveryListComponent } from './list/hirarkiExternalRecovery-list.component'
import { HirarkiExternalRecoveryDetailComponent } from './detail/hirarkiExternalRecovery-detail.component'
import { HirarkiExternalRecoveryContentDetailComponent } from './contentDetail/hirarkiExternalRecovery-contentDetail.component'
import { HirarkiExternalRecoveryService } from './hirarkiExternalRecovery.service'
import { HirarkiExternalRecoveryRoutingModule } from './hirarkiExternalRecovery-routing.module'
import { HirarkiExternalRecoveryMainComponent } from './hirarkiExternalRecovery-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Validator } from '../control/validator'
import { Alert } from '../control/util'
import { SelectModule } from 'ng2-select'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule,
        HirarkiExternalRecoveryRoutingModule,
        SelectModule
    ],
    exports: [],
    declarations: [
        HirarkiExternalRecoveryListComponent,
        HirarkiExternalRecoveryDetailComponent,
        HirarkiExternalRecoveryMainComponent,
        HirarkiExternalRecoveryContentDetailComponent
    ],
    providers: [
        HirarkiExternalRecoveryService,
        HttpService,
        AuthGuard,
        Validator,
        Alert
    ],
})
export class HirarkiExternalRecoveryModule { }
