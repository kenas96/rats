import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class HirarkiExternalRecoveryService {

    constructor(private service: HttpService) { }


    getPaging(query_string) {
        return this.service.get('webuser/hierarchyexternalrecoveries?' + query_string)
    }

    getDetail(query_string, id) {
        return this.service.get('webuser/hierarchyexternalrecoveries/' + id + '?' + query_string)
    }

    submit(data) {
        return this.service.post('webuser/hierarchyexternalrecoveries', data)
    }

    getActiveInternalRecovery() {
        return this.service.get('webuser/internalrecoveries?is_active=true')
    }

}