import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { HirarkiExternalRecoveryService } from '../hirarkiExternalRecovery.service'
import { HirarkiExternalRecovery } from '../hirarkiExternalRecovery.model'
import { Alert } from '../../control/util'

@Component({
    selector: 'hirarkiExternalRecovery-list',
    templateUrl: './hirarkiExternalRecovery-list.component.html',
    styleUrls: ['./hirarkiExternalRecovery-list.component.css']
})

export class HirarkiExternalRecoveryListComponent implements OnInit {
    constructor(
        private service: HirarkiExternalRecoveryService,
        private fb: FormBuilder,
        private alert: Alert
    ) { }

    adminType: string = ''
    state: number = 1
    hirarkiExternalRecovery: HirarkiExternalRecovery
    hirarkiExternalRecoveryForm: FormGroup
    lengthData: number = 0
    lengthDataInnerTable: number = 0
    displayCallback: Function
    getDetailCallback: Function
    viewCallback: Function
    editCallback: Function
    deleteCallback: Function

    data = {
        rows: []
    }

    dataInnerTable = {
        rows: [],
        // internal_recovery: [
        //     {
        //         id: '',
        //         text: ''
        //     }
        // ],
    }

    dataSelected = {
        rows: []
    }

    option: any = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: false,
        disNext: false,
        order: { column: 'id', ascending: true }
    }

    optionInnerTable: any = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: false,
        disNext: false,
        order: { column: 'name', ascending: true }
    }

    columns = [
        { text: 'NIK', field: 'nik', value: 'nik' },
        { text: 'Name', field: 'name', value: 'name' },
        { text: 'Email', field: 'email', value: 'email' },
        { text: 'Handphone', field: 'handphone', value: 'handphone' }
    ]

    columnsInnerTable = [
        { text: 'MOU / External Name', field: 'name', value: 'name' },
        { text: 'Category', field: 'category', value: 'category' },
        { text: 'KTP', field: 'ktp', value: 'ktp' },
        { text: 'PIC Name', field: 'pic_name', value: 'pic_name' },
        { text: 'Start Date', field: 'start_date', value: 'start_date' },
        { text: 'End Date', field: 'end_date', value: 'end_date' }
    ]

    listCriteria = [
        { text: 'NIK', value: 'nik' },
        { text: 'Name', value: 'name' },
        { text: 'Email', value: 'email' },
        { text: 'Handphone', value: 'handphone' }
    ]

    listCriteriaInnerTable = [
        { text: 'MOU / External Name', value: 'name' },
        { text: 'Category', value: 'category' },
        { text: 'PIC Name', value: 'pic_name' },
    ]

    permission: any = {
        create: true,
        view: true,
        update: false,
        delete: false,
        upload: false
    }

    permissionInnerTable: any = {
        create: false,
        view: true,
        update: false,
        delete: false,
        upload: false
    }

    CheckBtnPrevNext() {
        if (this.option.page === 1) {
            this.option.disPrev = true
        } else {
            this.option.disPrev = false
        }
        if ((this.option.page === 1 || this.option.page > 1) && this.lengthData <= this.option.pageSize) {
            this.option.disNext = true
        } else {
            this.option.disNext = false
        }
    }

    CheckBtnPrevNextInnerTable() {
        if (this.optionInnerTable.page === 1) {
            this.optionInnerTable.disPrev = true
        } else {
            this.optionInnerTable.disPrev = false
        }
        if ((this.optionInnerTable.page === 1 || this.optionInnerTable.page > 1) &&
            this.lengthDataInnerTable <= this.optionInnerTable.pageSize) {
            this.optionInnerTable.disNext = true
        } else {
            this.optionInnerTable.disNext = false
        }
    }

    ngOnInit() {
        this.hirarkiExternalRecovery = new HirarkiExternalRecovery()
        this.displayCallback = this.displayData.bind(this)
        this.getDetailCallback = this.searchNIK.bind(this)
        this.viewCallback = this.view.bind(this)
        this.editCallback = this.edit.bind(this)
        this.deleteCallback = this.remove.bind(this)
        this.hirarkiExternalRecoveryForm = this.fb.group({
            id: ['', [Validators.required]],
            position_id: [{ value: '', disabled: true }],
            position_name: [{ value: '', disabled: true }],
            nik: [{ value: '', disabled: true }],
            name: [{ value: '', disabled: true }],
            handphone: [{ value: '', disabled: true }],
            email: [{ value: '', disabled: true }],
            is_active: '',
            search_nik: ''
        })

        //admin permission validation
        let user = localStorage.getItem('auth-user')
        let decoded = atob(user)
        for (var i = 0; i < decoded.length; i++) {
            if (decoded[i] === ':') {
                this.adminType = decoded.substr(0, i)
                break
            }
        }

        //additional permission
        if (this.adminType === 'Reco Admin' || this.adminType === 'Warehouse Admin') {
            this.permission.create = false
        }
    }

    ngAfterViewInit() {
        this.displayData()
    }

    loadingAnimationGridStart() {
        document.getElementById('dataGrid').style.display = "none"
        document.getElementById('loadingGrid').style.display = "block"
    }

    loadingAnimationGridStop() {
        document.getElementById('dataGrid').style.display = "block"
        document.getElementById('loadingGrid').style.display = "none"
    }

    parseData(data) {
        for (let entry of data) {
            if (entry['is_active'] === true) {
                entry['is_active_string'] = "Active"
            } else {
                entry['is_active_string'] = "Non Active"
            }
        }
    }

    parseDataInnerTable(data) {
        for (let entry of data) {
            if (entry['is_active'] === true) {
                entry['is_active_string'] = "Active"
            } else {
                entry['is_active_string'] = "Non Active"
            }
        }
        setTimeout(() => {
            for (var i = 0; i < this.dataSelected.rows.length; i++) {
                $('#checkbox' + this.dataSelected.rows[i].id).prop('checked', true)
            }
        }, 500)
    }

    displayData() {
        this.loadingAnimationGridStart()
        let tmp_query = [
            { param: "page_size", value: parseInt(this.option.pageSize) + 1 },
            { param: "page_offset", value: parseInt(this.option.pageOffset) },
            { param: "order_column", value: this.option.order.column },
            { param: "order_ascending", value: this.option.order.ascending }
        ]
        if (Object.keys(this.option.criteria).length > 0) {
            tmp_query.push({ param: this.option.criteria.text, value: this.option.criteria.value })
        }
        let que_str = tmp_query.map(function (el) {
            return el.param + '=' + el.value
        }).join('&')
        this.service.getPaging(que_str).
            then(
                res => {
                    if (Array.isArray(res.data)) {
                        this.lengthData = res.data.length
                        this.data.rows = res.data
                        if (this.lengthData > this.option.pageSize) {
                            let idx = this.lengthData - 1
                            this.data.rows.splice(idx, 1)
                        }
                    } else {
                        this.data.rows = [res.data]
                    }
                    this.CheckBtnPrevNext()
                    this.parseData(this.data.rows)
                    this.loadingAnimationGridStop()
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                    this.loadingAnimationGridStop()
                }
            )
        // this.service.getActiveInternalRecovery().then(
        //     res => {
        //         if (Array.isArray(res.data)) {
        //             this.dataInnerTable.internal_recovery = []
        //             for (let int of res.data) {
        //                 int['text'] = int['nik']
        //                 this.dataInnerTable.internal_recovery.push(int)
        //             }
        //         } else {
        //             this.dataInnerTable.internal_recovery = [res.data]
        //         }
        //     },
        //     err => {
        //         if (err.http_status === 422) {
        //             this.alert.error_alert(err.message)
        //         } else {
        //             this.alert.warn_alert(err.message)
        //         }
        //     }
        // )
    }

    getDetail() {
        this.alert.loading_start()
        let tmp_query = [
            { param: "page_size", value: parseInt(this.optionInnerTable.pageSize) + 1 },
            { param: "page_offset", value: parseInt(this.optionInnerTable.pageOffset) },
            { param: "order_column", value: this.optionInnerTable.order.column },
            { param: "order_ascending", value: this.optionInnerTable.order.ascending }
        ]
        if (Object.keys(this.optionInnerTable.criteria).length > 0) {
            tmp_query.push({ param: this.optionInnerTable.criteria.text, value: this.optionInnerTable.criteria.value });
        }
        let que_str = tmp_query.map(function (el) {
            return el.param + '=' + el.value
        }).join('&')
        this.service.getDetail(que_str, this.hirarkiExternalRecovery.nik).then(
            res => {
                if (Array.isArray(res.data.mous)) {
                    this.hirarkiExternalRecovery = res.data
                    this.populateData()
                    this.lengthDataInnerTable = res.data.mous.length
                    this.dataInnerTable.rows = res.data.mous

                    if (this.lengthDataInnerTable > this.optionInnerTable.pageSize) {
                        let idx = this.lengthDataInnerTable - 1
                        this.dataInnerTable.rows.splice(idx, 1)
                    }
                } else {
                    this.dataInnerTable.rows = [res.data]
                }
                this.CheckBtnPrevNextInnerTable()
                this.parseDataInnerTable(this.dataInnerTable.rows)
                this.alert.loading_stop()
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
            }
        )
    }

    searchNIK() {
        this.alert.loading_start()
        if (this.state === 0) {
            //view custom
            this.dataInnerTable.rows = []
            let tmp_query_all = [
                { param: "page_size", value: 1000000 },
                { param: "page_offset", value: 0 },
                { param: "order_column", value: this.optionInnerTable.order.column },
                { param: "order_ascending", value: this.optionInnerTable.order.ascending }
            ]
            if (Object.keys(this.optionInnerTable.criteria).length > 0) {
                tmp_query_all.push({ param: this.optionInnerTable.criteria.text, value: this.optionInnerTable.criteria.value });
            }
            let que_str_all = tmp_query_all.map(function (el) {
                return el.param + '=' + el.value
            }).join('&')
            this.service.getDetail(que_str_all, this.hirarkiExternalRecovery.nik).then(
                res => {
                    for (var i = 0; i < res.data.mous.length; i++) {
                        if (res.data.mous[i].checked) {
                            this.dataInnerTable.rows.push(res.data.mous[i])
                        }
                    }
                    this.alert.loading_stop()
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                }
            )
        } else {
            let tmp_query = [
                { param: "page_size", value: parseInt(this.optionInnerTable.pageSize) + 1 },
                { param: "page_offset", value: parseInt(this.optionInnerTable.pageOffset) },
                { param: "order_column", value: this.optionInnerTable.order.column },
                { param: "order_ascending", value: this.optionInnerTable.order.ascending }
            ]
            if (Object.keys(this.optionInnerTable.criteria).length > 0) {
                tmp_query.push({ param: this.optionInnerTable.criteria.text, value: this.optionInnerTable.criteria.value });
            }
            let que_str = tmp_query.map(function (el) {
                return el.param + '=' + el.value
            }).join('&')
            this.service.getDetail(que_str, this.hirarkiExternalRecoveryForm.get('search_nik').value).then(
                res => {
                    if (Array.isArray(res.data.mous)) {
                        this.hirarkiExternalRecovery = res.data
                        this.populateData()
                        this.lengthDataInnerTable = res.data.mous.length
                        this.dataInnerTable.rows = res.data.mous

                        if (this.lengthDataInnerTable > this.optionInnerTable.pageSize) {
                            let idx = this.lengthDataInnerTable - 1
                            this.dataInnerTable.rows.splice(idx, 1)
                        }
                    } else {
                        this.dataInnerTable.rows = [res.data]
                    }
                    this.CheckBtnPrevNextInnerTable()
                    this.parseDataInnerTable(this.dataInnerTable.rows)
                    this.alert.loading_stop()
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                }
            )
        }

    }

    focusField() {
        setTimeout(() => {
            $("#searchnik").focus()
        }, 500)
    }

    populateData() {
        this.hirarkiExternalRecoveryForm.patchValue({
            id: this.hirarkiExternalRecovery.id,
            position_id: this.hirarkiExternalRecovery.position_id,
            position_name: this.hirarkiExternalRecovery.position_name,
            nik: this.hirarkiExternalRecovery.nik,
            name: this.hirarkiExternalRecovery.name,
            handphone: this.hirarkiExternalRecovery.handphone,
            email: this.hirarkiExternalRecovery.email,
            is_active: this.hirarkiExternalRecovery.is_active,
            search_nik: this.hirarkiExternalRecovery.nik
        })
    }

    resetOption() {
        this.optionInnerTable.page = 1
        this.optionInnerTable.pageOffset = 0
        this.optionInnerTable.pageSize = 10
        this.optionInnerTable.criteria = {}
        this.optionInnerTable.disPrev = false
        this.optionInnerTable.disNext = false
        this.optionInnerTable.order = { column: 'name', ascending: true }

        this.hirarkiExternalRecoveryForm.reset()
        this.dataInnerTable.rows = []
        this.dataSelected.rows = []
        this.optionInnerTable.criteria.value = ''
        $('#search_in').val("")
    }

    addNew() {
        this.resetOption()
        this.state = 1
        this.hirarkiExternalRecovery = new HirarkiExternalRecovery()
        this.focusField()
        $('#modal-detail').modal()
        $('input:checkbox').prop('checked', false)
    }

    view(obj) {
        this.resetOption()
        this.state = 0
        this.hirarkiExternalRecovery = obj

        let tmp_query_all = [
            { param: "page_size", value: 1000000 },
            { param: "page_offset", value: 0 },
            { param: "order_column", value: "name" },
            { param: "order_ascending", value: true }
        ]
        let que_str_all = tmp_query_all.map(function (el) {
            return el.param + '=' + el.value
        }).join('&')

        this.alert.loading_start()
        this.service.getDetail(que_str_all, this.hirarkiExternalRecovery.nik).then(
            res => {
                this.hirarkiExternalRecovery = res.data
                this.populateData()
                for (var i = 0; i < res.data.mous.length; i++) {
                    if (res.data.mous[i].checked) {
                        this.dataInnerTable.rows.push(res.data.mous[i])
                    }
                }
                this.alert.loading_stop()
                $('#modal-detail').modal()
                $('input:checkbox').prop('checked', false)
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
            }
        )
    }

    edit(obj) {
    }

    remove(obj) {
    }
}
