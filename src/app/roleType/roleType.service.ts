import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class RoleTypeService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('role_types?' + query_string)
    }

    getDetail(id) {
        return this.service.get('role_types/' + id)
    }

    insert(data) {
        return this.service.post('role_types', data)
    }

    update(data, id) {
        return this.service.put('role_types' + id, data)
    }

    delete(id) {
        return this.service.delete('role_types/' + id)
    }
}