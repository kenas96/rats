import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class ReportBranchCoverageService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('webuser/report/branchcoverage?' + query_string)
    }

    downloadReport(query_string) {
        return this.service.get('webuser/report/branchcoverage/generate?' + query_string)
    }

    downloadFullReport() {
        //maximum rows in excel
        return this.service.get('webuser/report/branchcoverage/generate?page_size=1048576')
    }

}