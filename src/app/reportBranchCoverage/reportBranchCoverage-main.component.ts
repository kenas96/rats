import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-reportBranchCoverage',
    template: '<router-outlet></router-outlet>'
})

export class ReportBranchCoverageMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}