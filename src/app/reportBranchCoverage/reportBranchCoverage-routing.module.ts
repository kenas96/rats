import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ReportBranchCoverageMainComponent } from './reportBranchCoverage-main.component'
import { ReportBranchCoverageListComponent } from './list/reportBranchCoverage-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: ReportBranchCoverageMainComponent,
        children: [
            {
                path: '',
                component: ReportBranchCoverageListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: ReportBranchCoverageListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class ReportBranchCoverageRoutingModule { }
