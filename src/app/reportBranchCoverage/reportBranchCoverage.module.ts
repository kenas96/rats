import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { ReportBranchCoverageListComponent } from './list/reportBranchCoverage-list.component'
import { ReportBranchCoverageService } from './reportBranchCoverage.service'
import { ReportBranchCoverageRoutingModule } from './reportBranchCoverage-routing.module'
import { ReportBranchCoverageMainComponent } from './reportBranchCoverage-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        HttpModule,
        RouterModule,
        ReportBranchCoverageRoutingModule
    ],
    exports: [],
    declarations: [
        ReportBranchCoverageListComponent,
        ReportBranchCoverageMainComponent
    ],
    providers: [
        ReportBranchCoverageService,
        HttpService,
        AuthGuard,
        Alert
    ],
})
export class ReportBranchCoverageModule { }
