import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-webUser',
    template: '<router-outlet></router-outlet>'
})

export class WebUserMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}