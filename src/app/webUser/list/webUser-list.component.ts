import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { WebUserService } from '../webUser.service'
import { WebUser, WebUserImage } from '../webUser.model'
import { JwtHelper } from 'angular2-jwt'
import { Validator } from '../../control/validator'
import { Alert } from '../../control/util'
import swal from 'sweetalert2'

@Component({
    selector: 'webUser-list',
    templateUrl: './webUser-list.component.html',
    styleUrls: ['./webUser-list.component.css']
})

export class WebUserListComponent implements OnInit {
    constructor(
        private service: WebUserService,
        private fb: FormBuilder,
        private validator: Validator,
        private alert: Alert
    ) { }

    adminType: string = ''
    state: number = 1
    webUser: WebUser
    webUserImage: WebUserImage
    webUserForm: FormGroup
    lengthData: number = 0
    displayCallback: Function
    viewCallback: Function
    editCallback: Function
    deleteCallback: Function
    downloadTemplateCallback: Function
    uploadCallback: Function
    jwtHelper: JwtHelper = new JwtHelper()

    data = {
        rows: []
    }

    option: any = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: false,
        disNext: false,
        order: { column: 'id', ascending: true }
    }

    columns = [
        { text: 'Name', field: 'name', value: 'name' },
        { text: 'Email', field: 'email', value: 'email' },
        { text: 'Handphone', field: 'handphone', value: 'handphone' },
        { text: 'Active Status', field: 'is_active', value: 'is_active' },
    ]

    listCriteria = [
        { text: 'Name', value: 'name' },
        { text: 'Email', value: 'email' },
        { text: 'Handphone', value: 'handphone' },
    ]

    permission: any = {
        create: true,
        view: true,
        update: true,
        delete: true,
        upload: false
    }

    CheckBtnPrevNext() {
        if (this.option.page === 1) {
            this.option.disPrev = true
        } else {
            this.option.disPrev = false
        }
        if ((this.option.page === 1 || this.option.page > 1) && this.lengthData <= this.option.pageSize) {
            this.option.disNext = true
        } else {
            this.option.disNext = false
        }
    }

    ngOnInit() {
        this.webUser = new WebUser()
        this.displayCallback = this.displayData.bind(this)
        this.viewCallback = this.view.bind(this)
        this.editCallback = this.edit.bind(this)
        this.deleteCallback = this.remove.bind(this)
        this.webUserForm = this.fb.group({
            request_id: '',
            id: '',
            web_user_id: '',
            name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
            picture: '',
            email: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')]],
            handphone: ['', [Validators.required, this.validator.isNumber, Validators.minLength(10), Validators.maxLength(20)]],
            password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]],
            confirm_password: '',
            role_type_id: ['', [Validators.required]],
            role: [{ value: '', disabled: true }, Validators.required],
            is_active: '',
            insert_date: '',
            insert_by: '',
            insert_by_name: '',
            update_date: '',
            update_by: '',
            update_by_name: ''
        })

        //admin permission validation
        let user = localStorage.getItem('auth-user')
        let decoded = atob(user)
        for (var i = 0; i < decoded.length; i++) {
            if (decoded[i] === ':') {
                this.adminType = decoded.substr(0, i)
                break
            }
        }
        if (this.adminType !== 'Super Admin') {
            this.permission.delete = false
            this.permission.create = false
        }

        //additional permission
        if (this.adminType === 'Reco Admin' || this.adminType === 'Warehouse Admin') {
            this.permission.update = false
        }
    }

    ngAfterViewInit() {
        this.displayData()
    }

    loadingAnimationGridStart() {
        document.getElementById('dataGrid').style.display = "none"
        document.getElementById('loadingGrid').style.display = "block"
    }

    loadingAnimationGridStop() {
        document.getElementById('dataGrid').style.display = "block"
        document.getElementById('loadingGrid').style.display = "none"
    }

    parseData() {
        let user = this.jwtHelper.decodeToken(localStorage.getItem('auth-token'))
        for (let entry of this.data.rows) {
            if (entry['id'] == user.id) {
                entry['noDelete'] = true
            }
            if (entry['is_active']) {
                entry['is_active'] = 'Active'
            } else {
                entry['is_active'] = 'Not Active'
            }
        }
    }

    displayData() {
        this.loadingAnimationGridStart()
        let tmp_query = [
            { param: "page_size", value: parseInt(this.option.pageSize) + 1 },
            { param: "page_offset", value: parseInt(this.option.pageOffset) },
            { param: "order_column", value: this.option.order.column },
            { param: "order_ascending", value: this.option.order.ascending }
        ]
        if (Object.keys(this.option.criteria).length > 0) {
            tmp_query.push({ param: this.option.criteria.text, value: this.option.criteria.value })
        }
        let que_str = tmp_query.map(function (el) {
            return el.param + '=' + el.value
        }).join('&')
        this.service.getPaging(que_str).
            then(
                res => {
                    if (Array.isArray(res.data)) {
                        this.lengthData = res.data.length
                        this.data.rows = res.data
                        if (this.lengthData > this.option.pageSize) {
                            let idx = this.lengthData - 1
                            this.data.rows.splice(idx, 1)
                        }
                    } else {
                        this.data.rows = [res.data]
                    }
                    this.CheckBtnPrevNext()
                    this.parseData()
                    this.loadingAnimationGridStop()
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                    this.loadingAnimationGridStop()
                }
            )
    }

    focusField() {
        setTimeout(() => {
            $("#name").focus()
        }, 500)
    }

    populateData() {
        var stringActive: string
        if (this.webUser.is_active === 'Active') {
            stringActive = "active"
        } else {
            stringActive = "notactive"
        }

        this.webUserForm.patchValue({
            request_id: this.webUser.request_id,
            id: this.webUser.id,
            web_user_id: this.webUser.web_user_id,
            name: this.webUser.name,
            picture: this.webUser.picture,
            email: this.webUser.email,
            handphone: this.webUser.handphone,
            password: this.webUser.password,
            role_type_id: this.webUser.role_type_id,
            role: this.webUser.role,
            is_active: stringActive,
            insert_date: this.webUser.insert_date,
            insert_by: this.webUser.insert_by,
            insert_by_name: this.webUser.insert_by_name,
            update_date: this.webUser.update_date,
            update_by: this.webUser.update_by,
            update_by_name: this.webUser.update_by_name,
        })
    }

    setPasswordValidator() {
        const webPasswordControl = this.webUserForm.get("password")
        if (this.state === -1) {
            webPasswordControl.clearValidators()
        } else {
            webPasswordControl.setValidators([Validators.required, Validators.minLength(6), Validators.maxLength(20)])
        }
        webPasswordControl.updateValueAndValidity()
    }

    addNew() {
        this.state = 1
        this.webUser = new WebUser()
        this.webUserImage = new WebUserImage()
        this.webUserForm.reset()
        this.focusField()
        this.setPasswordValidator()
        $("#imageUpload").val('')
        $('#modal-detail').modal()
    }

    view(obj) {
        this.state = 0
        this.webUser = obj
        this.webUserImage = new WebUserImage()
        this.webUserForm.reset()
        this.populateData()
        $("#imageUpload").val('')
        $('#modal-detail').modal()
    }

    edit(obj) {
        this.state = -1
        this.webUser = JSON.parse(JSON.stringify(obj))
        this.webUserImage = new WebUserImage()
        this.webUserForm.reset()
        this.focusField()
        this.populateData()
        this.setPasswordValidator()
        $("#imageUpload").val('')
        $('#modal-detail').modal()
    }

    remove(obj) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                this.alert.loading_start()
                this.service.delete(obj.id).then(
                    res => {
                        this.alert.success_alert('Your data has been removed!', '', this.displayData())
                    },
                    err => {
                        if (err.http_status === 422) {
                            this.alert.error_alert(err.message)
                        } else {
                            this.alert.warn_alert(err.message)
                        }
                    }
                )
            }
        })
    }
}