import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class WebUserService {

    constructor(private service: HttpService) { }


    getPaging(query_string) {
        return this.service.get('webuser/usermanagements?' + query_string)
    }

    getDetail(id) {
        return this.service.get('webuser/usermanagements/' + id)
    }

    insert(data) {
        return this.service.post('webuser/register', data)
    }

    update(data, id) {
        return this.service.put('webuser/usermanagements/' + id, data)
    }

    delete(id) {
        return this.service.delete('webuser/usermanagements/' + id)
    }

    postImage(data) {
        return this.service.postForm('users/picture', data)
    }

}