export class WebUser {
    request_id: number
    id: number
    web_user_id: number
    name: string
    picture: string
    email: string
    handphone: string
    password: string
    role_type_id: number
    role: string
    is_active: any
    insert_date: number
    insert_by: string
    insert_by_name: string
    update_date: number
    update_by: string
    update_by_name: string


    constructor(data: any = {}) {
        this.request_id = data.request_id
        this.id = data.id
        this.web_user_id = data.web_user_id
        this.name = data.name
        this.picture = data.picture
        this.email = data.email
        this.handphone = data.handphone
        this.password = data.password
        this.role_type_id = data.role_type_id
        this.role = data.role
        this.is_active = data.is_active
        this.insert_date = data.insert_date
        this.insert_by = data.insert_by
        this.insert_by_name = data.insert_by_name
        this.update_date = data.update_date
        this.update_by = data.update_by
        this.update_by_name = data.update_by_name

    }
}

export class WebUserImage {
    file: any
    name: string
    username: string

    constructor(data: any = {}) {
        this.file = data.file
        this.name = data.name
        this.username = data.username
    }
}