import { Component, OnInit, Input } from '@angular/core'
import { FormGroup, AbstractControl } from '@angular/forms'
import { WebUser, WebUserImage } from './../webUser.model'
import { WebUserService } from './../webUser.service'
import { Alert } from '../../control/util'
import { JwtHelper } from 'angular2-jwt'
import 'rxjs/add/operator/debounceTime'
import { AppConfig } from '../../config/app.config';

@Component({
    selector: 'webUser-detail',
    templateUrl: './webUser-detail.component.html',
    styleUrls: ['./webUser-detail.component.css']
})

export class WebUserDetailComponent implements OnInit {
    constructor(
        private service: WebUserService,
        private alert: Alert,
        private config: AppConfig) {
            this.base = this.config.getConfig('base_url')
        }

    @Input() id: string
    @Input() webUser: WebUser
    @Input() webUserImage: WebUserImage
    @Input() state: number
    @Input() displayData: Function
    @Input() webUserForm: FormGroup
    nameMessage: string
    emailMessage: string
    handphoneMessage: string
    roleMessage: string
    passwordMessage: string
    selectRoleTypeCallback: Function
    jwtHelper: JwtHelper = new JwtHelper()
    base: string

    private validationMessage = {
        required: "Please fill this field.",
        minlength: "Min. character allowed are 3 characters.",
        maxlength: "Max. character allowed are 50 characters.",
        string: "Please enter a valid character format.",
        pattern: "Please enter a valid email address."
    }

    private validationMessageNumber = {
        required: "Please fill this field.",
        minlength: "Min. character allowed are 10 characters.",
        maxlength: "Max. character allowed are 20 characters.",
        number: "Please enter a valid number format.",
    }

    private validationMessagePassword = {
        required: "Please fill this field.",
        minlength: "Min. character allowed are 6 characters.",
        maxlength: "Max. character allowed are 20 characters."
    }


    ngOnInit() {
        this.selectRoleTypeCallback = this.selectRoleType.bind(this)
        const nameControl = this.webUserForm.get('name')
        nameControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageName(nameControl)
        )

        const emailControl = this.webUserForm.get('email')
        emailControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageEmail(emailControl)
        )

        const handphoneControl = this.webUserForm.get('handphone')
        handphoneControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageHandphone(handphoneControl)
        )

        const roleControl = this.webUserForm.get('role')
        roleControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageRoleType(roleControl)
        )

        const passwordControl = this.webUserForm.get('password')
        passwordControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessagePassword(passwordControl)
        )
    }

    setMessageName(c: AbstractControl): void {
        this.nameMessage = ''
        if (this.state == 0) {
            this.webUserForm.get('name').disable()
        } else {
            this.webUserForm.get('name').enable()
            if ((c.touched || c.dirty) && c.errors) {
                this.nameMessage = Object.keys(c.errors).map(key =>
                    this.validationMessage[key]
                )[0]
            }
        }
    }

    setMessageEmail(c: AbstractControl): void {
        this.emailMessage = ''
        if (this.state == 0) {
            this.webUserForm.get('email').disable()
        } else {
            this.webUserForm.get('email').enable()
            if ((c.touched || c.dirty) && c.errors) {
                this.emailMessage = Object.keys(c.errors).map(key =>
                    this.validationMessage[key]
                )[0]
            }
        }
    }

    setMessageHandphone(c: AbstractControl): void {
        this.handphoneMessage = ''
        if (this.state == 0) {
            this.webUserForm.get('handphone').disable()
        } else {
            this.webUserForm.get('handphone').enable()
            if ((c.touched || c.dirty) && c.errors) {
                this.handphoneMessage = Object.keys(c.errors).map(key =>
                    this.validationMessageNumber[key]
                )[0]
            }
        }
    }

    setMessageRoleType(c: AbstractControl): void {
        this.roleMessage = ''
        if (c.value === undefined) {
            this.roleMessage = "Please select role type."
        }
    }

    setMessagePassword(c: AbstractControl): void {
        this.passwordMessage = ''
        if ((c.touched || c.dirty) && c.errors) {
            this.passwordMessage = Object.keys(c.errors).map(key =>
                this.validationMessagePassword[key]
            )[0]
        }
    }

    showRoleType() {
        $('#lookup-roleType').modal()
    }

    selectRoleType(obj) {
        // if (obj.id === 1 || obj.id === 2) {
        this.webUserForm.patchValue({
            role_type_id: obj.id,
            role: obj.type
        })
        $('#lookup-roleType').modal('hide')
        // } else {
        //     this.alert.warn_alert("The available role types for this module are Web Admin and User Web only!")
        // }

    }

    removeRoleType() {
        this.webUserForm.patchValue({
            role_type_id: undefined,
            role: undefined
        })
    }

    onFileChange(event) {
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0]
            if (file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'image/jpg') {
                this.webUserImage.file = file
                this.webUserImage.name = file.name
                //this.webUserImage.username = this.webUser.email
                this.webUserImage.username = this.webUserForm.get('email').value
            } else {
                $("#imageUpload").val('')
                this.alert.warn_alert('Please input jpg/jpeg/png picture format')
                this.webUserImage.file = undefined
                this.webUserImage.name = undefined
                this.webUserImage.username = undefined
            }
        }
    }

    initFormImage() {
        let input = new FormData()
        input.append('name', this.webUserImage.name)
        input.append('file', this.webUserImage.file)
        input.append('username', this.webUserImage.username)
        return input
    }

    save() {
        this.alert.loading_start()
        let obj = Object.assign({}, this.webUser, this.webUserForm.value)
        obj.request_id = Date.now()
        obj.role = undefined
        obj.insert_by_name = undefined
        obj.update_by_name = undefined
        if (obj.is_active == "notactive") {
            obj.is_active = false
        } else {
            obj.is_active = true
        }
        if (this.state == 1) {
            //add
            let user = this.jwtHelper.decodeToken(localStorage.getItem('auth-token'))
            obj.insert_by = user.id
            obj.insert_date = Date.now()
            obj.id = undefined
            obj.confirm_password = undefined
            obj.update_by = undefined
            obj.update_date = undefined
            this.service.insert(obj).then(
                res => {
                    if (this.webUserImage.file != null) {
                        this.service.postImage(this.initFormImage()).then(
                            res => {
                                this.alert.success_alert('Your data has been added', this.id, this.displayData())
                            },
                            err => {
                                if (err.http_status === 422) {
                                    this.alert.error_alert(err.message)
                                } else {
                                    this.alert.warn_alert(err.message)
                                }
                            }
                        )
                    } else {
                        this.alert.success_alert('Your data has been added', this.id, this.displayData())
                    }
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                }
            )
        }
        else {
            //edit
            let user = this.jwtHelper.decodeToken(localStorage.getItem('auth-token'))
            obj.update_by = user.id
            obj.update_date = Date.now()
            this.service.update(obj, obj.id).then(
                res => {
                    if (this.webUserImage.file != null) {
                        this.service.postImage(this.initFormImage()).then(
                            res => {
                                this.alert.success_alert('Your data has been added', this.id, this.displayData())
                            },
                            err => {
                                if (err.http_status === 422) {
                                    this.alert.error_alert(err.message)
                                } else {
                                    this.alert.warn_alert(err.message)
                                }
                            }
                        )
                    } else {
                        this.alert.success_alert('Your data has been saved', this.id, this.displayData())
                    }
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                }
            )

        }
    }
}
