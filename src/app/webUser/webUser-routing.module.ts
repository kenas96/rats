import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { WebUserMainComponent } from './webUser-main.component'
import { WebUserListComponent } from './list/webUser-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: WebUserMainComponent,
        children: [
            {
                path: '',
                component: WebUserListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: WebUserListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class WebUserRoutingModule { }
