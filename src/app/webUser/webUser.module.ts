import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { WebUserListComponent } from './list/webUser-list.component'
import { WebUserDetailComponent } from './detail/webUser-detail.component'
import { WebUserService } from './webUser.service'
import { WebUserRoutingModule } from './webUser-routing.module'
import { WebUserMainComponent } from './webUser-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Validator } from '../control/validator'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule,
        WebUserRoutingModule],
    exports: [],
    declarations: [
        WebUserListComponent,
        WebUserDetailComponent,
        WebUserMainComponent],
    providers: [
        WebUserService,
        HttpService,
        AuthGuard,
        Validator,
        Alert
    ],
})
export class WebUserModule { }
