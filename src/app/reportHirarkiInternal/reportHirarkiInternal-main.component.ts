import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-reportHirarkiInternal',
    template: '<router-outlet></router-outlet>'
})

export class ReportHirarkiInternalMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}