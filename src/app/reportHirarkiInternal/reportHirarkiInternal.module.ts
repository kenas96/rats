import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { ReportHirarkiInternalListComponent } from './list/reportHirarkiInternal-list.component'
import { ReportHirarkiInternalService } from './reportHirarkiInternal.service'
import { ReportHirarkiInternalRoutingModule } from './reportHirarkiInternal-routing.module'
import { ReportHirarkiInternalMainComponent } from './reportHirarkiInternal-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        HttpModule,
        RouterModule,
        ReportHirarkiInternalRoutingModule
    ],
    exports: [],
    declarations: [
        ReportHirarkiInternalListComponent,
        ReportHirarkiInternalMainComponent
    ],
    providers: [
        ReportHirarkiInternalService,
        HttpService,
        AuthGuard,
        Alert
    ],
})
export class ReportHirarkiInternalModule { }
