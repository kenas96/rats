import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ReportHirarkiInternalMainComponent } from './reportHirarkiInternal-main.component'
import { ReportHirarkiInternalListComponent } from './list/reportHirarkiInternal-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: ReportHirarkiInternalMainComponent,
        children: [
            {
                path: '',
                component: ReportHirarkiInternalListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: ReportHirarkiInternalListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class ReportHirarkiInternalRoutingModule { }
