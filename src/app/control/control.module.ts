import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { GridviewComponent } from './grid/grid-view.component'
import { ReportviewComponent } from './report/report-view.component'
import { NotifviewComponent } from './notif/notif-view.component'
import { GridSelectviewComponent } from './gridSelect/gridSelect-view.component'
import { GridSelectAreaRecviewComponent } from './gridSelectAreaRec/gridSelectAreaRec-view.component'
import { gridSelectParamviewComponent } from './gridSelectParam/gridSelectParam-view.component'
import { LookupComponent } from './lookup/lookup.component'
import { FormsModule } from '@angular/forms'

import { LookupAreaComponent } from './lookupDetail/lookup-area.component'
import { LookupRoleTypeComponent } from './lookupDetail/lookup-roleType.component'
import { LookupPositionComponent } from './lookupDetail/lookup-position.component'
import { AreaService } from '../area/area.service'
import { RoleTypeService } from '../roleType/roleType.service'
import { PositionService } from '../position/position.service'
import { Alert } from '../control/util'


@NgModule({
    imports: [
        FormsModule,
        CommonModule
    ],
    exports: [
        GridviewComponent,
        ReportviewComponent,
        NotifviewComponent,
        GridSelectviewComponent,
        GridSelectAreaRecviewComponent,
        gridSelectParamviewComponent,
        LookupComponent,
        LookupAreaComponent,
        LookupRoleTypeComponent,
        LookupPositionComponent,

    ],
    declarations: [
        GridviewComponent,
        ReportviewComponent,
        NotifviewComponent,
        GridSelectviewComponent,
        GridSelectAreaRecviewComponent,
        gridSelectParamviewComponent,
        LookupComponent,
        LookupAreaComponent,
        LookupRoleTypeComponent,
        LookupPositionComponent,

    ],
    providers: [
        AreaService,
        RoleTypeService,
        PositionService,
        Alert,
    ],
})
export class ControlModule { }

