import { Component, OnInit, Input } from '@angular/core'

@Component({
    selector: 'grid-view',
    templateUrl: './grid-view.component.html',
    styleUrls: ['./grid-view.component.css']
})

export class GridviewComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
    @Input() columns = []
    @Input() listCriteria = []
    @Input() permission: any = {
        create: false,
        view: false,
        update: false,
        delete: false,
        upload: false
    }
    @Input() data = {
        rows: []
    }
    @Input() options = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: null,
        disNext: null,
        order: { column: 'id', ascending: true }
    }
    @Input() display: Function

    @Input() view(obj) {
    }
    @Input() edit(obj) {
    }
    @Input() remove(obj) {
    }
    @Input() downloadTemplate() {
    }
    @Input() upload() {
    }
    placeholderCriteria: any = 'Search'
    criteria: any = ''
    value: any = ''
    ascending: boolean = null

    displayData() {
        this.display()
    }

    changeCriteria(crt) {
        this.criteria = crt.value
        this.placeholderCriteria = crt.text
        this.value = ''
    }

    search() {
        if (this.criteria !== '') {
            this.options.page = 1
            this.options.pageOffset = 0
            this.options.criteria = { text: this.criteria, value: this.value }
            this.options.page = 1
            this.display()
        }
        else {
            $("#myPopup").fadeIn(500)
            setTimeout(() => {
                $("#myPopup").fadeOut(2500)
            }, 3000)
        }
    }

    sort(field) {
        let direx = (this.ascending) ? true : false
        this.options.order = { "column": field, "ascending": direx }
        this.display()
        this.ascending = !this.ascending
    }

    changePageSize() {
        let tmp = {}
        this.options.page = 1
        this.options.pageOffset = 0
        this.display()
    }

    prev() {
        if (this.options.page - 1 >= 1) {
            this.options.page--
            this.options.pageOffset = (this.options.page - 1) * this.options.pageSize
        }
        this.display()
    }
    next() {
        this.options.page++
        this.options.pageOffset = (this.options.page - 1) * this.options.pageSize
        this.display()
    }

}