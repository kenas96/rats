import { Component, OnInit, Input } from '@angular/core'
import { Alert } from '../../control/util'

@Component({
  selector: 'gridSelectAreaRec-view',
  templateUrl: './gridSelectAreaRec-view.component.html',
  styleUrls: ['./gridSelectAreaRec-view.component.css']
})

export class GridSelectAreaRecviewComponent implements OnInit {
  constructor(
    private alert: Alert
  ) { }

  ngOnInit() { }
  @Input() state: number
  @Input() columns = []
  @Input() listCriteria = []
  @Input() permission: any = {
    create: false,
    view: false,
    update: false,
    delete: false,
    upload: false
  }
  @Input() data = {
    rows: []
  }
  @Input() dataSelected = {
    rows: []
  }
  @Input() options = {
    page: 1,
    pageOffset: 0,
    pageSize: 100,
    criteria: {},
    disPrev: null,
    disNext: null,
    order: { column: 'id', ascending: true }
  }
  @Input() display: Function

  @Input() view(obj) {
  }
  @Input() edit(obj) {
  }
  @Input() remove(obj) {
  }
  @Input() downloadTemplate() {
  }
  @Input() upload() {
  }
  placeholderCriteria: any = 'Search'
  criteria: any = ''
  value: any = ''
  ascending: boolean = null

  displayData() {
    this.display()
  }

  changeCriteria(crt) {
    this.criteria = crt.value
    this.placeholderCriteria = crt.text
    this.value = ''
  }

  search() {
    if (this.criteria !== '') {
      this.options.page = 1
      this.options.pageOffset = 0
      this.options.criteria = { text: this.criteria, value: this.value }
      this.options.page = 1
      this.display()
    } else {
      this.alert.warn_alert("Please choose the criteria")
    }
  }

  sort(field) {
    let direx = (this.ascending) ? true : false
    this.options.order = { "column": field, "ascending": direx }
    this.display()
    this.ascending = !this.ascending
  }

  changePageSize() {
    let tmp = {}
    this.options.page = 1
    this.options.pageOffset = 0
    this.display()
  }

  prev() {
    $('#checkAll').prop('checked', false)
    if (this.options.page - 1 >= 1) {
      this.options.page--
      this.options.pageOffset = (this.options.page - 1) * this.options.pageSize
    }
    this.display()
  }
  next() {
    $('#checkAll').prop('checked', false)
    this.options.page++
    this.options.pageOffset = (this.options.page - 1) * this.options.pageSize
    this.display()
  }

  check(obj) {
    var checkExist = false
    var tempIndex: number
    for (var i = 0; i < this.dataSelected.rows.length; i++) {
      if (this.dataSelected.rows[i].id == obj.id) {
        checkExist = true
        tempIndex = i
        break
      }
    }
    if (checkExist == true) {
      //jika data sudah ada di checkList
      let index: number = this.dataSelected.rows.indexOf(obj);
      if (index !== -1) {
        this.dataSelected.rows.splice(index, 1);
      } else {
        this.dataSelected.rows.splice(tempIndex, 1);
      }
    } else {
      //jika data belum ada di checkList
      this.dataSelected.rows.push(obj);
    }
  }

  checkAll() {
    if ($('#checkAll').prop('checked')) {
      for (var i = 0; i < this.data.rows.length; i++) {
        $('#checkbox' + this.data.rows[i].id).prop('checked', true)
        var obj: any = this.dataSelected.rows.find(x => x.id == this.data.rows[i].id)
        if (obj === undefined) {
          this.dataSelected.rows.push(this.data.rows[i])
        }
      }
    } else {
      for (var i = 0; i < this.data.rows.length; i++) {
        $('#checkbox' + this.data.rows[i].id).prop('checked', false)

        for (var j = 0; j < this.dataSelected.rows.length; j++) {
          if (this.dataSelected.rows[j].id === this.data.rows[i].id) {
            this.dataSelected.rows.splice(j, 1);
          }
        }
      }
    }
    console.log(this.dataSelected.rows)
  }

  changeAble(type, obj) {
    for (var i = 0; i < this.dataSelected.rows.length; i++) {
      if (this.dataSelected.rows[i].id == obj.id) {
        if (type === 0) {
          if (this.dataSelected.rows[i].ableObjectMotor) {
            this.dataSelected.rows[i].ableObjectMotor = false
          } else {
            this.dataSelected.rows[i].ableObjectMotor = true
          }
        } else if (type === 1) {
          if (this.dataSelected.rows[i].ableObjectMobile) {
            this.dataSelected.rows[i].ableObjectMobile = false
          } else {
            this.dataSelected.rows[i].ableObjectMobile = true
          }
        } else if (type === 2) {
          if (this.dataSelected.rows[i].ableObjectDurable) {
            this.dataSelected.rows[i].ableObjectDurable = false
          } else {
            this.dataSelected.rows[i].ableObjectDurable = true
          }
        }
      }
    }
  }

}
