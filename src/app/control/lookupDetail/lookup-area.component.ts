import { Component, OnInit, Input } from '@angular/core'
import { AreaService } from '../../area/area.service'
import { Alert } from '../../control/util'

@Component({
    selector: 'lookup-area',
    template: `
    <div class="modal-dialog modal-md">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <span class="close" data-dismiss="modal" style=";color:white;">&times;</span>
                Select Area
            </div>
            <div class="panel-body">
                <lookup 
                [columns]="columns"
                [data]="data" 
                [display]="displayCallback" 
                [select]="selectCallback"
                [options]="option"
                [listCriteriaLookup]="listCriteriaLookup"
                ></lookup>
            </div>
            <div class="panel-footer" style="text-align: right">
                <button class="btn btn-default" (click)="close()"><i class="fa fa-times-circle" style="margin-right: 5px"></i>Close</button>
            </div>
        </div>
    </div>  
    `,
    styles: [`
       .panel-primary > .panel-heading {
        background-color: #3E4551;
        }
  `]
})

export class LookupAreaComponent implements OnInit {
    constructor(
        private service: AreaService,
        private alert: Alert
    ) { }

    @Input() areaRecovery
    @Input() select(obj) {
    }
    lengthData: number = 0
    displayCallback: Function
    selectCallback: Function

    data = {
        rows: [],
    }

    columns = [
        { text: 'No', field: 'id' },
        { text: 'Name', field: 'name' }
    ]

    listCriteriaLookup = [
        { text: 'Name', value: 'name' },
    ]

    option: any = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: false,
        disNext: false,
        order: { column: 'id', ascending: true }
    }

    CheckBtnPrevNext() {
        if (this.option.page === 1) {
            this.option.disPrev = true
        } else {
            this.option.disPrev = false;
        }
        if ((this.option.page === 1 || this.option.page > 1) && this.lengthData <= this.option.pageSize) {
            this.option.disNext = true;
        } else {
            this.option.disNext = false;
        }
    }

    ngOnInit() {
        this.displayCallback = this.displayData.bind(this)
        this.selectCallback = this.select.bind(this)
    }

    ngAfterViewInit() {
        this.displayData()
    }

    loadingAnimationGridStart() {
        this.alert.loading_start()
    }

    loadingAnimationGridStop() {
        this.alert.loading_stop()
    }

    displayData() {
        this.loadingAnimationGridStart()
        let tmp_query = [
            { param: "page_size", value: parseInt(this.option.pageSize) + 1 },
            { param: "page_offset", value: parseInt(this.option.pageOffset) },
            { param: "order_column", value: this.option.order.column },
            { param: "order_ascending", value: this.option.order.ascending }
        ];
        if (Object.keys(this.option.criteria).length > 0) {
            tmp_query.push({ param: this.option.criteria.text, value: this.option.criteria.value });
        }
        let que_str = tmp_query.map(function (el) {
            return el.param + '=' + el.value;
        }).join('&');
        this.service.getPaging(que_str).
            then(
                res => {
                    if (Array.isArray(res.data)) {
                        this.lengthData = res.data.length;
                        this.data.rows = res.data;
                        if (this.lengthData > this.option.pageSize) {
                            let idx = this.lengthData - 1;
                            this.data.rows.splice(idx, 1);
                        }
                    } else {
                        this.data.rows = [res.data];
                    }
                    this.CheckBtnPrevNext();
                    this.loadingAnimationGridStop()
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                    this.loadingAnimationGridStop()
                }
            )
    }

    close() {
        $('#lookup-area').modal('hide')
        $('body').addClass('modal-open')
    }
}