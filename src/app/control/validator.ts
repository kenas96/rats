import { AbstractControl, ValidatorFn } from '@angular/forms'

export class Validator {

    // static range(min: number, max: number): ValidatorFn {
    //     return (c: AbstractControl): { [key: string]: boolean } | null => {
    //         if (c.value && (isNaN(c.value) || c.value < min || c.value > max)) {
    //             return { 'range': true };
    //         }
    //         return null;
    //     };
    // }

    isString(c: AbstractControl): { [key: string]: boolean } | null {
        if (!isNaN(c.value)) {
            return { 'string': true }
        }
        return null
    }

    isNumber(c: AbstractControl): { [key: string]: boolean } | null {
        if (isNaN(c.value)) {
            return { 'number': true }
        }
        return null
    }

    // passwordMatcher(c: AbstractControl): { [key: string]: boolean } | null {
    //     let passwordControl = c.get('new_password')
    //     let confirmPasswordControl = c.get('confirm_new_password')

    //     if (passwordControl.pristine || confirmPasswordControl.pristine) {
    //         return null
    //     }
    //     if (passwordControl === confirmPasswordControl) {
    //         return null
    //     }
    //     return { 'match': true }
    // }
}
