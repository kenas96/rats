import { Component, OnInit, Input } from '@angular/core'

@Component({
    selector: 'report-view',
    templateUrl: './report-view.component.html',
    styleUrls: ['./report-view.component.css']
})

export class ReportviewComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
    @Input() columns = []
    @Input() listCriteria = []
    @Input() data = {
        rows: [],
        noReport: false
    }
    @Input() options = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: [{}],
        disPrev: null,
        disNext: null,
        order: { column: 'id', ascending: true }
    }
    @Input() display: Function

    @Input() downloadReport() {
    }
    @Input() downloadFullReport() {
    }
    ascending: boolean = null

    displayData() {
        this.display()
    }

    download() {
        for (let crt of this.listCriteria) {
            if (crt.value !== '') {
                this.options.criteria.push({ text: crt.value, value: crt.filter })
            }
        }
        this.downloadReport()
    }

    search() {
        for (let crt of this.listCriteria) {
            if (crt.value !== '') {
                this.options.criteria.push({ text: crt.value, value: crt.filter })
            }
        }
        this.options.page = 1
        this.options.pageOffset = 0
        this.options.page = 1
        this.display()
    }

    sort(field) {
        let direx = (this.ascending) ? true : false
        this.options.order = { "column": field, "ascending": direx }
        this.display()
        this.ascending = !this.ascending
    }

    changePageSize() {
        let tmp = {}
        this.options.page = 1
        this.options.pageOffset = 0
        this.display()
    }

    prev() {
        if (this.options.page - 1 >= 1) {
            this.options.page--
            this.options.pageOffset = (this.options.page - 1) * this.options.pageSize
        }
        this.display()
    }
    next() {
        this.options.page++
        this.options.pageOffset = (this.options.page - 1) * this.options.pageSize
        this.display()
    }

}