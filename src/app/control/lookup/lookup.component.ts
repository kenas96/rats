import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'lookup',
    templateUrl: './lookup.component.html',
    styleUrls: ['./lookup.component.css']
})

export class LookupComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
    @Input() columns = []
    @Input() listCriteriaLookup = []
    @Input() data = {
        rows: [],
    }

    @Input() options:any = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: null,
        disNext: null,
        order: { column: 'id', ascending: true }
    }

    @Input() display: Function

    @Input() select(obj) {
    }

    placeholderCriteria: any = 'Search'
    criteria: any = ''
    value: any = ''
    ascending: boolean = null

    displayData() {
        this.display();
    }

    changeCriteria(crt) {
        this.criteria = crt.value
        this.placeholderCriteria = crt.text
        this.value = ''
    }

    searchLookup() {
        if (this.criteria !== '') {
            this.options.page = 1;
            this.options.pageOffset = 0;
            this.options.criteria = { text: this.criteria, value: this.value }
            this.options.page = 1;
            this.display()
        }
        else {
            $(".popuptext").fadeIn(500);
            setTimeout(() => {
                $(".popuptext").fadeOut(2500);
            }, 3000)
        }
    }

    sort(field) {
        let direx = (this.ascending) ? true : false;
        this.options.order = { "column": field, "ascending": direx };
        this.display();
        this.ascending = !this.ascending;
    }

    prev() {
        if (this.options.page - 1 >= 1) {
            this.options.page--
            this.options.pageOffset = (this.options.page - 1) * this.options.pageSize
        }
        this.display()
    }
    next() {
        this.options.page++
        this.options.pageOffset = (this.options.page - 1) * this.options.pageSize
        this.display()
    }

}