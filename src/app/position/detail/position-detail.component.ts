import { Component, OnInit, Input } from '@angular/core'
import { Position } from './../position.model'

@Component({
    selector: 'position-detail',
    templateUrl: './position-detail.component.html',
    styleUrls: ['./position-detail.component.css']
})

export class PositionDetailComponent implements OnInit {
    constructor(
    ) { }

    @Input() id: string;
    @Input() position: Position

    ngOnInit() {
    }
}