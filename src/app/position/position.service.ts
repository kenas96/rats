import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class PositionService {
    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('positions?' + query_string)
    }

    getDetail(id) {
        return this.service.get('positions/' + id)
    }

}