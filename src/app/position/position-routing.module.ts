import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { PositionMainComponent } from './position-main.component'
import { PositionListComponent } from './list/position-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: PositionMainComponent,
        children: [
            {
                path: '',
                component: PositionListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: PositionListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class PositionRoutingModule { }
