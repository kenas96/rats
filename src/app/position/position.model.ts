export class Position {
    request_id: number
    id: number
    name: string

    constructor(data: any = {}) {
        this.request_id = data.request_id
        this.id = data.id
        this.name = data.name
    }
}