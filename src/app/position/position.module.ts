import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { PositionListComponent } from './list/position-list.component'
import { PositionDetailComponent } from './detail/position-detail.component'
import { PositionService } from './position.service'
import { PositionRoutingModule } from './position-routing.module'
import { PositionMainComponent } from './position-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        HttpModule,
        RouterModule,
        PositionRoutingModule
    ],
    exports: [],
    declarations: [
        PositionListComponent,
        PositionDetailComponent,
        PositionMainComponent
    ],
    providers: [
        PositionService,
        HttpService,
        AuthGuard,
        Alert
    ],
})
export class PositionModule { }
