import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-position',
    template: '<router-outlet></router-outlet>'
})

export class PositionMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}