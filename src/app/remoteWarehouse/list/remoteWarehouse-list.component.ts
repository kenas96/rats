import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { RemoteWarehouseService } from '../remoteWarehouse.service'
import { RemoteWarehouse, RemoteWarehouseUpload } from '../remoteWarehouse.model'
import { AppConfig } from '../../config/app.config';
import { Alert } from '../../control/util'

@Component({
  selector: 'remoteWarehouse-list',
  templateUrl: './remoteWarehouse-list.component.html',
  styleUrls: ['./remoteWarehouse-list.component.css']
})

export class RemoteWarehouseListComponent implements OnInit {
  constructor(
    private service: RemoteWarehouseService,
    private fb: FormBuilder,
    private alert: Alert,
    private config: AppConfig) {
    this.base = this.config.getConfig('base_url')
  }

  base: string
  adminType: string = ''
  state: number = 1
  remoteWarehouse: RemoteWarehouse
  remoteWarehouseUpload: RemoteWarehouseUpload
  remoteWarehouseForm: FormGroup
  lengthData: number = 0
  displayCallback: Function
  viewCallback: Function
  editCallback: Function
  deleteCallback: Function
  downloadTemplateCallback: Function
  uploadCallback: Function

  data = {
    rows: []
  }

  option: any = {
    page: 1,
    pageOffset: 0,
    pageSize: 10,
    criteria: {},
    disPrev: false,
    disNext: false,
    order: { column: 'id', ascending: true }
  }

  columns = [
    { text: 'No', field: 'id', value: 'id' },
    { text: 'Name', field: 'name', value: 'name' },
    { text: 'Status', field: 'is_active_string', value: 'is_active' }
  ]

  listCriteria = [
    { text: 'Name', value: 'name' },
  ]

  permission: any = {
    create: false,
    view: true,
    update: false,
    delete: false,
    upload: true
  }

  CheckBtnPrevNext() {
    if (this.option.page === 1) {
      this.option.disPrev = true
    } else {
      this.option.disPrev = false
    }
    if ((this.option.page === 1 || this.option.page > 1) && this.lengthData <= this.option.pageSize) {
      this.option.disNext = true
    } else {
      this.option.disNext = false
    }
  }

  ngOnInit() {
    this.remoteWarehouse = new RemoteWarehouse()
    this.remoteWarehouseUpload = new RemoteWarehouseUpload()
    this.displayCallback = this.displayData.bind(this)
    this.viewCallback = this.view.bind(this)
    this.editCallback = this.edit.bind(this)
    this.deleteCallback = this.remove.bind(this)
    this.downloadTemplateCallback = this.downloadTemplate.bind(this)
    this.uploadCallback = this.upload.bind(this)
    this.remoteWarehouseForm = this.fb.group({
      request_id: '',
      id: '',
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      isReplace: ''
    })

    let user = localStorage.getItem('auth-user')
    let decoded = atob(user)
    for (var i = 0; i < decoded.length; i++) {
      if (decoded[i] === ':') {
        this.adminType = decoded.substr(0, i)
        break
      }
    }

    //additional permission
    if (this.adminType === 'Reco Admin' || this.adminType === 'Warehouse Admin') {
      this.permission.upload = false
    }
  }

  ngAfterViewInit() {
    this.displayData()
  }

  loadingAnimationGridStart() {
    document.getElementById('dataGrid').style.display = "none"
    document.getElementById('loadingGrid').style.display = "block"
  }

  loadingAnimationGridStop() {
    document.getElementById('dataGrid').style.display = "block"
    document.getElementById('loadingGrid').style.display = "none"
  }

  parseData() {
    for (let entry of this.data.rows) {
      if (entry['is_active'] === true) {
        entry['is_active_string'] = "Active"
      } else {
        entry['is_active_string'] = "Not Active"
      }
      var new_id = entry['id'].substring(3, 11)
      new_id = Number(new_id).toString()
      entry['id'] = new_id
    }
  }

  displayData() {
    this.loadingAnimationGridStart()
    let tmp_query = [
      { param: "page_size", value: parseInt(this.option.pageSize) + 1 },
      { param: "page_offset", value: parseInt(this.option.pageOffset) },
      { param: "order_column", value: this.option.order.column },
      { param: "order_ascending", value: this.option.order.ascending }
    ]
    if (Object.keys(this.option.criteria).length > 0) {
      tmp_query.push({ param: this.option.criteria.text, value: this.option.criteria.value })
    }
    let que_str = tmp_query.map(function (el) {
      return el.param + '=' + el.value
    }).join('&')
    this.service.getPaging(que_str).then(
      res => {
        if (Array.isArray(res.data)) {
          this.lengthData = res.data.length
          this.data.rows = res.data
          if (this.lengthData > this.option.pageSize) {
            let idx = this.lengthData - 1
            this.data.rows.splice(idx, 1)
          }
        } else {
          this.data.rows = [res.data]
        }
        this.CheckBtnPrevNext()
        this.parseData()
        this.loadingAnimationGridStop()
      },
      err => {
        if (err.http_status === 422) {
          this.alert.error_alert(err.message)
        } else {
          this.alert.warn_alert(err.message)
        }
        this.loadingAnimationGridStop()
      }
    )
  }

  downloadTemplate() {
    window.open(this.base + 'remotewarehouses/Template_remote_warehouse.xlsx')
  }

  upload() {
    this.state = 1
    this.remoteWarehouseUpload = new RemoteWarehouseUpload()
    this.remoteWarehouseForm.patchValue({
      isReplace: false
    })
    $("#excelUpload").val('')
    $('#modal-detail').modal()
  }

  populateData() {
    this.remoteWarehouseForm.patchValue({
      request_id: this.remoteWarehouse.request_id,
      id: this.remoteWarehouse.id,
      name: this.remoteWarehouse.name,
      insert_date: this.remoteWarehouse.insert_date,
      insert_by: this.remoteWarehouse.insert_by,
      insert_by_name: this.remoteWarehouse.insert_by_name,
      update_date: this.remoteWarehouse.update_date,
      update_by: this.remoteWarehouse.update_by,
      update_by_name: this.remoteWarehouse.update_by_name,
      is_active: this.remoteWarehouse.is_active,
    })
  }

  addNew() {
  }

  view(obj) {
    this.state = 0
    this.remoteWarehouse = obj
    this.remoteWarehouseForm.reset()
    this.populateData()
    $('#modal-detail').modal()
  }

  edit(obj) {
  }

  remove(obj) {
  }
}
