import { NgModule, Component } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { RemoteWarehouseMainComponent } from './remoteWarehouse-main.component'
import { RemoteWarehouseListComponent } from './list/remoteWarehouse-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: RemoteWarehouseMainComponent,
        children: [
            {
                path: '',
                component: RemoteWarehouseListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: RemoteWarehouseListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class RemoteWarehouseRoutingModule { }
