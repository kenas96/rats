import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-remoteWarehouse',
    template: '<router-outlet></router-outlet>'
})

export class RemoteWarehouseMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}