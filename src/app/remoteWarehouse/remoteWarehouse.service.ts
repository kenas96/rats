import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class RemoteWarehouseService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('remote_warehouses?' + query_string)
    }

    getDetail(id) {
        return this.service.get('remote_warehouses/' + id)
    }

    uploadExcel(data) {
        return this.service.postForm('remotewarehouses', data)
    }

}
