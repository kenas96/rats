import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { RemoteWarehouseListComponent } from './list/remoteWarehouse-list.component'
import { RemoteWarehouseDetailComponent } from './detail/remoteWarehouse-detail.component'
import { RemoteWarehouseService } from './remoteWarehouse.service'
import { RemoteWarehouseRoutingModule } from './remoteWarehouse-routing.module'
import { RemoteWarehouseMainComponent } from './remoteWarehouse-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Validator } from '../control/validator'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule,
        RemoteWarehouseRoutingModule
    ],
    exports: [],
    declarations: [
        RemoteWarehouseListComponent,
        RemoteWarehouseDetailComponent,
        RemoteWarehouseMainComponent
    ],
    providers: [
        RemoteWarehouseService,
        HttpService,
        AuthGuard,
        Validator,
        Alert
    ],
})
export class RemoteWarehouseModule { }
