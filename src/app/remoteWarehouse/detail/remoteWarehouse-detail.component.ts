import { Component, OnInit, Input } from '@angular/core'
import { FormGroup, AbstractControl } from '@angular/forms'
import { Router } from '@angular/router'
import { RemoteWarehouse, RemoteWarehouseUpload } from './../remoteWarehouse.model'
import { RemoteWarehouseService } from './../remoteWarehouse.service'
import { Alert } from '../../control/util'
import 'rxjs/add/operator/debounceTime'

@Component({
    selector: 'remoteWarehouse-detail',
    templateUrl: './remoteWarehouse-detail.component.html',
    styleUrls: ['./remoteWarehouse-detail.component.css']
})

export class RemoteWarehouseDetailComponent implements OnInit {
    constructor(
        private service: RemoteWarehouseService,
        private alert: Alert,
        private router: Router
    ) { }

    @Input() id: string
    @Input() remoteWarehouse: RemoteWarehouse
    @Input() remoteWarehouseUpload: RemoteWarehouseUpload
    @Input() state: number
    @Input() displayData: Function
    @Input() remoteWarehouseForm: FormGroup
    nameMessage: string

    private validationMessage = {
        required: "Please fill this field.",
        minlength: "Min. character allowed are 3 characters.",
        maxlength: "Max. character allowed are 50 characters.",
        string: "Please enter a valid character format.",
        pattern: "Please enter a valid emaill address."
    }

    ngOnInit() {
        const nameControl = this.remoteWarehouseForm.get('name')
        nameControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageName(nameControl)
        )
    }

    setMessageName(c: AbstractControl): void {
        this.nameMessage = ''
        if ((c.touched || c.dirty) && c.errors) {
            this.nameMessage = Object.keys(c.errors).map(key =>
                this.validationMessage[key]
            )[0]
        }

        if (this.state == 0) {
            this.remoteWarehouseForm.get('name').disable()
        } else {
            this.remoteWarehouseForm.get('name').enable()
        }
    }

    onFileChange(event) {
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0]
            if (file.type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                this.alert.warn_alert('Please input correct file format(.xlsx)')
                $("#excelUpload").val('')
            } else {
                this.remoteWarehouseUpload.file = file
                this.remoteWarehouseUpload.name = 'Template_remote_warehouse.xlsx'
            }
        }
    }

    initFormImage() {
        let input = new FormData()
        input.append('name', this.remoteWarehouseUpload.name)
        input.append('file', this.remoteWarehouseUpload.file)
        input.append('isReplace', this.remoteWarehouseForm.get('isReplace').value)
        return input
    }

    save() {
        this.alert.loading_start()
        this.service.uploadExcel(this.initFormImage()).then(
            res => {
                this.alert.success_alert('Upload Successed', this.id, this.displayData())
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }

            }
        )
    }
}