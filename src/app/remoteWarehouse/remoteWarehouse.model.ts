export class RemoteWarehouse {
    request_id: number
    id: number
    name: string
    insert_date: number
    insert_by: string
    insert_by_name: string
    update_date: number
    update_by: string
    update_by_name: string
    is_active: boolean

    constructor(data: any = {}) {
        this.request_id = data.request_id
        this.id = data.id
        this.name = data.name
    }
}

export class RemoteWarehouseUpload {
    file: any
    name: string

    constructor(data: any = {}) {
        this.file = data.file
        this.name = data.name
    }
}