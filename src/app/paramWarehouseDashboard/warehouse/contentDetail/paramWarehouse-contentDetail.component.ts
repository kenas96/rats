import { Component, OnInit, Input } from '@angular/core'
import { ParamWarehouseDashboardContent } from './../../paramWarehouseDashboard.model'

@Component({
    selector: 'paramWarehouse-contentDetail',
    templateUrl: './paramWarehouse-contentDetail.component.html',
    styleUrls: ['./paramWarehouse-contentDetail.component.css']
})

export class ParamWarehouseContentDetailComponent implements OnInit {
    constructor() { }

    @Input() id: string
    @Input() warehouseContent: ParamWarehouseDashboardContent

    ngOnInit() {

    }
}