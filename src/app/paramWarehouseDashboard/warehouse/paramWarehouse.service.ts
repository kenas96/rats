import { Injectable } from '@angular/core'
import { HttpService } from '../../http.service'

@Injectable()
export class ParamWarehouseService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('webuser/warehouse?' + query_string)
    }

    getPagingAll() {
        return this.service.get('warehouses')
    }

    getDetail(query_string, id) {
        return this.service.get('webuser/warehouse/' + id + '?' + query_string)
    }

    submit(data) {
        return this.service.post('webuser/warehouse', data)
    }

}
