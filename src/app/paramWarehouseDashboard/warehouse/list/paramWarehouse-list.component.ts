import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { ParamWarehouseService } from '../paramWarehouse.service'
import { ParamWarehouseDashboard } from '../../paramWarehouseDashboard.model'
import { Alert } from '../../../control/util'


@Component({
    selector: 'paramWarehouse-list',
    templateUrl: './paramWarehouse-list.component.html',
    styleUrls: ['./paramWarehouse-list.component.css']
})

export class ParamWarehouseListComponent implements OnInit {
    constructor(
        private service: ParamWarehouseService,
        private fb: FormBuilder,
        private alert: Alert
    ) { }

    adminType: string = ''
    state: number = 1
    warehouse: ParamWarehouseDashboard
    warehouseForm: FormGroup
    lengthData: number = 0
    lengthDataInnerTable: number = 0
    displayCallback: Function
    getDetailCallback: Function
    viewCallback: Function
    editCallback: Function
    deleteCallback: Function

    data = {
        rows: []
    }

    dataInnerTable = {
        rows: [],
        warehouses: [
            {
                id: '',
                text: ''
            }
        ],
    }

    dataSelected = {
        rows: []
    }

    option: any = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: false,
        disNext: false,
        order: { column: 'id', ascending: true }
    }

    optionInnerTable: any = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: false,
        disNext: false,
        order: { column: 'id', ascending: true }
    }

    columns = [
        { text: 'Name', field: 'name', value: 'name' }
    ]

    columnsInnerTable = [
        { text: 'Name', field: 'name', value: 'name' },
        { text: 'NIK', field: 'nik', value: 'nik' },
        { text: 'Position', field: 'position_name', value: 'position_name' }
    ]

    listCriteria = [
        { text: 'Name', value: 'name' },
    ]

    listCriteriaInnerTable = [
        { text: 'Name', value: 'name' },
        { text: 'NIK', value: 'nik' },
        { text: 'Position', value: 'position_name' }
    ]

    permission: any = {
        create: true,
        view: true,
        update: false,
        delete: false,
        upload: false
    }

    permissionInnerTable: any = {
        create: false,
        view: true,
        update: false,
        delete: false,
        upload: false
    }

    CheckBtnPrevNext() {
        if (this.option.page === 1) {
            this.option.disPrev = true
        } else {
            this.option.disPrev = false
        }
        if ((this.option.page === 1 || this.option.page > 1) && this.lengthData <= this.option.pageSize) {
            this.option.disNext = true
        } else {
            this.option.disNext = false
        }
    }

    CheckBtnPrevNextInnerTable() {
        if (this.optionInnerTable.page === 1) {
            this.optionInnerTable.disPrev = true
        } else {
            this.optionInnerTable.disPrev = false
        }
        if ((this.optionInnerTable.page === 1 || this.optionInnerTable.page > 1) &&
            this.lengthDataInnerTable <= this.optionInnerTable.pageSize) {
            this.optionInnerTable.disNext = true
        } else {
            this.optionInnerTable.disNext = false
        }
    }

    ngOnInit() {
        this.warehouse = new ParamWarehouseDashboard()
        this.displayCallback = this.displayData.bind(this)
        this.getDetailCallback = this.getDetail.bind(this)
        this.viewCallback = this.view.bind(this)
        this.editCallback = this.edit.bind(this)
        this.deleteCallback = this.remove.bind(this)
        this.warehouseForm = this.fb.group({
            id: ['', [Validators.required]],
            warehouse_name: [{ value: '', disabled: true }],
            search_warehouse: '',
        })

        //admin permission validation
        let user = localStorage.getItem('auth-user')
        let decoded = atob(user)
        for (var i = 0; i < decoded.length; i++) {
            if (decoded[i] === ':') {
                this.adminType = decoded.substr(0, i)
                break
            }
        }

        //additional permission
        if (this.adminType === 'Reco Admin') {
            this.permission.create = false
        }
    }

    ngAfterViewInit() {
        this.displayData()
    }

    loadingAnimationGridStart() {
        document.getElementById('dataGrid').style.display = "none"
        document.getElementById('loadingGrid').style.display = "block"
    }

    loadingAnimationGridStop() {
        document.getElementById('dataGrid').style.display = "block"
        document.getElementById('loadingGrid').style.display = "none"
    }

    parseData(data) {
    }

    parseDataInnerTable(data) {
        setTimeout(() => {
            for (var i = 0; i < this.dataSelected.rows.length; i++) {
                $('#checkbox' + this.dataSelected.rows[i].id).prop('checked', true)
            }
        }, 500)
    }

    displayData() {
        this.loadingAnimationGridStart()
        let tmp_query = [
            { param: "page_size", value: parseInt(this.option.pageSize) + 1 },
            { param: "page_offset", value: parseInt(this.option.pageOffset) },
            { param: "order_column", value: this.option.order.column },
            { param: "order_ascending", value: this.option.order.ascending }
        ]
        if (Object.keys(this.option.criteria).length > 0) {
            tmp_query.push({ param: this.option.criteria.text, value: this.option.criteria.value })
        }
        let que_str = tmp_query.map(function (el) {
            return el.param + '=' + el.value
        }).join('&')
        this.service.getPaging(que_str).then(
            res => {
                if (Array.isArray(res.data)) {
                    this.lengthData = res.data.length
                    this.data.rows = res.data
                    if (this.lengthData > this.option.pageSize) {
                        let idx = this.lengthData - 1
                        this.data.rows.splice(idx, 1)
                    }
                    this.service.getPagingAll().then(
                        res => {
                            if (Array.isArray(res.data)) {
                                this.dataInnerTable.warehouses = []
                                for (let warehouse of res.data) {
                                    warehouse['text'] = warehouse['name']
                                    this.dataInnerTable.warehouses.push(warehouse)
                                }
                            } else {
                                this.dataInnerTable.warehouses = [res.data]
                            }
                        },
                        err => {
                            if (err.http_status === 422) {
                                this.alert.error_alert(err.message)
                            } else {
                                this.alert.warn_alert(err.message)
                            }
                        }
                    )
                } else {
                    this.data.rows = [res.data]
                }
                this.CheckBtnPrevNext()
                this.parseData(this.data.rows)
                this.loadingAnimationGridStop()
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
                this.loadingAnimationGridStop()
            }
        )

    }

    getDetail() {
        this.alert.loading_start()
        if (this.state === 0) {
            //view custom
            this.dataInnerTable.rows = []
            let tmp_query_all = [
                { param: "page_size", value: 1000000 },
                { param: "page_offset", value: 0 },
                { param: "order_column", value: this.optionInnerTable.order.column },
                { param: "order_ascending", value: this.optionInnerTable.order.ascending }
            ]
            if (Object.keys(this.optionInnerTable.criteria).length > 0) {
                tmp_query_all.push({ param: this.optionInnerTable.criteria.text, value: this.optionInnerTable.criteria.value });
            }
            let que_str_all = tmp_query_all.map(function (el) {
                return el.param + '=' + el.value
            }).join('&')
            this.service.getDetail(que_str_all, this.warehouse.id).then(
                res => {
                    for (var i = 0; i < res.data.internal_recovery.length; i++) {
                        if (res.data.internal_recovery[i].checked) {
                            this.dataInnerTable.rows.push(res.data.internal_recovery[i])
                        }
                    }
                    this.alert.loading_stop()
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                }
            )
        } else {
            let tmp_query = [
                { param: "page_size", value: parseInt(this.optionInnerTable.pageSize) + 1 },
                { param: "page_offset", value: parseInt(this.optionInnerTable.pageOffset) },
                { param: "order_column", value: this.optionInnerTable.order.column },
                { param: "order_ascending", value: this.optionInnerTable.order.ascending }
            ]
            if (Object.keys(this.optionInnerTable.criteria).length > 0) {
                tmp_query.push({ param: this.optionInnerTable.criteria.text, value: this.optionInnerTable.criteria.value });
            }
            let que_str = tmp_query.map(function (el) {
                return el.param + '=' + el.value
            }).join('&')
            this.service.getDetail(que_str, this.warehouse.id).then(
                res => {
                    if (Array.isArray(res.data.internal_recovery)) {
                        this.warehouse = res.data
                        this.populateData()
                        this.lengthDataInnerTable = res.data.internal_recovery.length
                        this.dataInnerTable.rows = res.data.internal_recovery

                        if (this.lengthDataInnerTable > this.optionInnerTable.pageSize) {
                            let idx = this.lengthDataInnerTable - 1
                            this.dataInnerTable.rows.splice(idx, 1)
                        }
                    } else {
                        this.dataInnerTable.rows = [res.data]
                    }
                    this.CheckBtnPrevNextInnerTable()
                    this.parseDataInnerTable(this.dataInnerTable.rows)
                    this.alert.loading_stop()
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                }
            )
        }

    }

    focusField() {
    }

    populateData() {
        this.warehouseForm.patchValue({
            id: this.warehouse.id,
            warehouse_name: this.warehouse.warehouse_name,
            search_warehouse: ''
        })
    }

    resetOption() {
        this.optionInnerTable.page = 1
        this.optionInnerTable.pageOffset = 0
        this.optionInnerTable.pageSize = 10
        this.optionInnerTable.criteria = {}
        this.optionInnerTable.disPrev = false
        this.optionInnerTable.disNext = false
        this.optionInnerTable.order = { column: 'id', ascending: true }

        this.warehouseForm.reset()
        this.dataInnerTable.rows = []
        this.dataSelected.rows = []
        this.optionInnerTable.criteria.value = ''
        $('#search_in').val("")
    }

    addNew() {
        this.resetOption()
        this.state = 1
        this.warehouse = new ParamWarehouseDashboard()
        this.focusField()
        $('#modal-detail').modal()
        $('input:checkbox').prop('checked', false)
    }

    view(obj) {
        this.resetOption()
        this.state = 0
        this.warehouse = obj
        let tmp_query_all = [
            { param: "page_size", value: 1000000 },
            { param: "page_offset", value: 0 },
            { param: "order_column", value: "id" },
            { param: "order_ascending", value: true }
        ]
        let que_str_all = tmp_query_all.map(function (el) {
            return el.param + '=' + el.value
        }).join('&')

        this.alert.loading_start()
        this.service.getDetail(que_str_all, this.warehouse.id).then(
            res => {
                this.warehouse = res.data
                this.populateData()
                for (var i = 0; i < res.data.internal_recovery.length; i++) {
                    if (res.data.internal_recovery[i].checked) {
                        this.dataInnerTable.rows.push(res.data.internal_recovery[i])
                    }
                }
                this.alert.loading_stop()
                $('#modal-detail').modal()
                $('input:checkbox').prop('checked', false)

            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
            }
        )
    }

    edit(obj) {

    }

    remove(obj) {

    }
}