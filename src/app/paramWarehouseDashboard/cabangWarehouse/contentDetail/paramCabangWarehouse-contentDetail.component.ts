import { Component, OnInit, Input } from '@angular/core'
import { ParamWarehouseDashboardContent } from './../../paramWarehouseDashboard.model'

@Component({
    selector: 'paramCabangWarehouse-contentDetail',
    templateUrl: './paramCabangWarehouse-contentDetail.component.html',
    styleUrls: ['./paramCabangWarehouse-contentDetail.component.css']
})

export class ParamCabangWarehouseContentDetailComponent implements OnInit {
    constructor() { }

    @Input() id: string
    @Input() cabangWarehouseContent: ParamWarehouseDashboardContent

    ngOnInit() {

    }
}