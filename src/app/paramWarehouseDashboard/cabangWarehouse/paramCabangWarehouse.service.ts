import { Injectable } from '@angular/core'
import { HttpService } from '../../http.service'

@Injectable()
export class ParamCabangWarehouseService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('webuser/branches?' + query_string)
    }

    getPagingAll() {
        return this.service.get('branches')
    }

    getDetail(query_string, id) {
        return this.service.get('webuser/branches/' + id + '?' + query_string)
    }

    submit(data) {
        return this.service.post('webuser/branches', data)
    }

}
