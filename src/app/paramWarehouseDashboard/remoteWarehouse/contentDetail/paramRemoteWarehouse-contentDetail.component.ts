import { Component, OnInit, Input } from '@angular/core'
import { ParamWarehouseDashboardContent } from './../../paramWarehouseDashboard.model'

@Component({
    selector: 'paramRemoteWarehouse-contentDetail',
    templateUrl: './paramRemoteWarehouse-contentDetail.component.html',
    styleUrls: ['./paramRemoteWarehouse-contentDetail.component.css']
})

export class ParamRemoteWarehouseContentDetailComponent implements OnInit {
    constructor() { }

    @Input() id: string
    @Input() remoteWarehouseContent: ParamWarehouseDashboardContent

    ngOnInit() {

    }
}