import { Component, OnInit, Input, ViewChild } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { ParamWarehouseDashboard, ParamWarehouseDashboardContent } from '../../paramWarehouseDashboard.model'
import { ParamRemoteWarehouseService } from './../paramRemoteWarehouse.service'
import { Alert } from '../../../control/util'
import 'rxjs/add/operator/debounceTime'
import swal from 'sweetalert2'

@Component({
    selector: 'paramRemoteWarehouse-detail',
    templateUrl: './paramRemoteWarehouse-detail.component.html',
    styleUrls: ['./paramRemoteWarehouse-detail.component.css']
})

export class ParamRemoteWarehouseDetailComponent implements OnInit {
    constructor(
        private service: ParamRemoteWarehouseService,
        private alert: Alert
    ) { }

    @Input() id: string
    @Input() remoteWarehouse: ParamWarehouseDashboard
    @Input() state: number
    @Input() displayData: Function
    @Input() getDetail: Function
    @Input() remoteWarehouseForm: FormGroup

    @Input() columns = []
    @Input() listCriteria = []
    @Input() data = {
        rows: [],
        warehouses: []
    }
    @Input() dataSelected = {
        rows: []
    }
    @Input() option = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: null,
        disNext: null,
        order: { column: 'id', ascending: true }
    }

    remoteWarehouseContent: ParamWarehouseDashboardContent
    viewCallback: Function
    editCallback: Function
    deleteCallback: Function
    getDetailCallback: Function
    @ViewChild('warehouseselect') public warehouseselect: any

    @Input() permission = {}

    ngOnInit() {
        this.remoteWarehouseContent = new ParamWarehouseDashboardContent()
        this.viewCallback = this.view.bind(this)
        this.editCallback = this.edit.bind(this)
        this.deleteCallback = this.remove.bind(this)
        this.getDetailCallback = this.getDetail.bind(this)
    }

    public selectedWarehouse(value: any): void {
        this.remoteWarehouse.id = value.id
        this.remoteWarehouseForm.patchValue({
            search_warehouse: this.remoteWarehouse.id
        })
    }

    search() {
        this.dataSelected.rows = []
        let tmp_query_all = [
            { param: "page_size", value: 1000000 },
            { param: "page_offset", value: 0 },
            { param: "order_column", value: "id" },
            { param: "order_ascending", value: true }
        ]
        let que_str_all = tmp_query_all.map(function (el) {
            return el.param + '=' + el.value
        }).join('&')
        this.service.getDetail(que_str_all, this.remoteWarehouse.id).then(
            res => {
                for (var i = 0; i < res.data.internal_recovery.length; i++) {
                    if (res.data.internal_recovery[i].checked) {
                        this.dataSelected.rows.push(res.data.internal_recovery[i])
                    }
                }
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
            }
        )
        this.getDetail()
        this.warehouseselect.active = []
        this.remoteWarehouseForm.patchValue({
            search_warehouse: ''
        })
    }

    view(obj) {
        this.remoteWarehouseContent = obj
        $('#modal-contentDetail').modal()
    }

    edit(obj) {

    }

    remove(obj) {

    }
    save() {
        swal({
            title: 'Are you sure?',
            text: "Your data will change permanently!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.alert.loading_start()
                let obj = Object.assign({}, this.remoteWarehouse, this.remoteWarehouseForm.value)
                obj.search_warehouse = undefined
                obj.request_id = Date.now()
                obj.internal_recovery = this.dataSelected.rows
                this.service.submit(obj).then(
                    res => {
                        this.alert.success_alert('Your data has been saved', this.id, this.displayData())
                    },
                    err => {
                        if (err.http_status === 422) {
                            this.alert.error_alert(err.message)
                        } else {
                            this.alert.warn_alert(err.message)
                        }
                    }
                )
            }
        })

    }
}