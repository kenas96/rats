import { Injectable } from '@angular/core'
import { HttpService } from '../../http.service'

@Injectable()
export class ParamRemoteWarehouseService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('webuser/remotewarehouse?' + query_string)
    }

    getPagingAll() {
        return this.service.get('remotewarehouse')
    }

    getDetail(query_string, id) {
        return this.service.get('webuser/remotewarehouse/' + id + '?' + query_string)
    }

    submit(data) {
        return this.service.post('webuser/remotewarehouse', data)
    }

}
