import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'

@Component({
    selector: 'paramWarehouseDashboard-component',
    templateUrl: './paramWarehouseDashboard.component.html',
})
export class ParamWarehouseDashboardComponent implements OnInit {
    constructor(private router: Router) { }
    oldNav: string
    ngOnInit() {
        let urlTarget = this.router.url.split('/')
        if (urlTarget[3] == undefined) {
            urlTarget[3] = "warehouse"
        }
        $('#' + urlTarget[3]).addClass('active')
    }
    clickNav(id) {
        if (this.oldNav !== undefined || this.oldNav !== null) {
            $('#warehouse').removeClass('active')
            $('#' + this.oldNav).removeClass('active')
        }
        $('#' + id).addClass('active')
        this.oldNav = id
    }

}