import { NgModule } from '@angular/core'
import { ReactiveFormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'
import { HttpModule } from '@angular/http'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { HttpService } from '../http.service'
import { AuthGuard } from '../pages/pages-guard.provider'
import { ParamWarehouseDashboardMainComponent } from './paramWarehouseDashboard-main.component'
import { ParamWarehouseDashboardComponent } from './paramWarehouseDashboard.component'
import { ParamWarehouseDashboardRoutingModule } from './paramWarehouseDashboard.routing'
import { ParamWarehouseListComponent } from './warehouse/list/paramWarehouse-list.component'
import { ParamWarehouseDetailComponent } from './warehouse/detail/paramWarehouse-detail.component'
import { ParamWarehouseContentDetailComponent } from './warehouse/contentDetail/paramWarehouse-contentDetail.component'
import { ParamWarehouseService } from './warehouse/paramWarehouse.service'
import { ParamRemoteWarehouseListComponent } from './remoteWarehouse/list/paramRemoteWarehouse-list.component'
import { ParamRemoteWarehouseDetailComponent } from './remoteWarehouse/detail/paramRemoteWarehouse-detail.component'
import { ParamRemoteWarehouseContentDetailComponent } from './remoteWarehouse/contentDetail/paramRemoteWarehouse-contentDetail.component'
import { ParamRemoteWarehouseService } from './remoteWarehouse/paramRemoteWarehouse.service'
import { ParamCabangWarehouseListComponent } from './cabangWarehouse/list/paramCabangWarehouse-list.component'
import { ParamCabangWarehouseDetailComponent } from './cabangWarehouse/detail/paramCabangWarehouse-detail.component'
import { ParamCabangWarehouseContentDetailComponent } from './cabangWarehouse/contentDetail/paramCabangWarehouse-contentDetail.component'
import { ParamCabangWarehouseService } from './cabangWarehouse/paramCabangWarehouse.service'
import { Validator } from '../control/validator'
import { Alert } from '../control/util'
import { SelectModule } from 'ng2-select'

@NgModule({
    imports: [
        ReactiveFormsModule,
        HttpModule,
        ParamWarehouseDashboardRoutingModule,
        RouterModule,
        CommonModule,
        ControlModule,
        SelectModule
    ],
    declarations: [
        ParamWarehouseDashboardComponent,
        ParamWarehouseDashboardMainComponent,
        ParamWarehouseListComponent,
        ParamWarehouseDetailComponent,
        ParamWarehouseContentDetailComponent,
        ParamRemoteWarehouseListComponent,
        ParamRemoteWarehouseDetailComponent,
        ParamRemoteWarehouseContentDetailComponent,
        ParamCabangWarehouseListComponent,
        ParamCabangWarehouseDetailComponent,
        ParamCabangWarehouseContentDetailComponent
    ],
    providers: [
        HttpService,
        ParamWarehouseService,
        ParamRemoteWarehouseService,
        ParamCabangWarehouseService,
        AuthGuard,
        Validator,
        Alert
    ],
})
export class ParamWarehouseDashboardModule { }
