import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'paramWarehouseDashboard-main',
    template: `
        <paramWarehouseDashboard-component></paramWarehouseDashboard-component>
        <router-outlet></router-outlet>
    `
})

export class ParamWarehouseDashboardMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }

}