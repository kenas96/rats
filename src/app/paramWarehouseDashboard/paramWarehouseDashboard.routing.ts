import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { AuthGuard } from '../pages/pages-guard.provider'
import { ParamWarehouseDashboardMainComponent } from './paramWarehouseDashboard-main.component'
import { ParamWarehouseListComponent } from './warehouse/list/paramWarehouse-list.component'
import { ParamRemoteWarehouseListComponent } from './remoteWarehouse/list/paramRemoteWarehouse-list.component'
import { ParamCabangWarehouseListComponent } from './cabangWarehouse/list/paramCabangWarehouse-list.component'


const routes: Routes = [
    {
        path: '',
        component: ParamWarehouseDashboardMainComponent,
        children: [
            {
                path: '',
                component: ParamWarehouseListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'warehouse',
                component: ParamWarehouseListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'remote_warehouse',
                component: ParamRemoteWarehouseListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'branch_warehouse',
                component: ParamCabangWarehouseListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class ParamWarehouseDashboardRoutingModule { }
