export class ParamWarehouseDashboard {
    id: string
    name: string
    warehouse_name: string
    is_active: boolean

    constructor(data: any = {}) {
        this.id = data.id
        this.name = data.name
        this.warehouse_name = data.warehouse_name
        this.is_active = data.is_active
    }
}

export class ParamWarehouseDashboardContent {
    id: string
    name: string
    warehouse_name: string
    is_active: boolean
    nik: string
    email: string
    handphone: string
    position_name: string

    constructor(data: any = {}) {
        this.id = data.id
        this.name = data.name
        this.warehouse_name = data.warehouse_name
        this.is_active = data.is_active
        this.nik = data.nik
        this.email = data.email
        this.handphone = data.handphone
        this.position_name = data.position_name
    }
}