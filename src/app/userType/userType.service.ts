import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class UserTypeService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('user_types?' + query_string)
    }

    getDetail(id) {
        return this.service.get('user_types/' + id)
    }

    insert(data) {
        return this.service.post('user_types', data)
    }

    update(data, id) {
        return this.service.put('user_types' + id, data)
    }

    delete(id) {
        return this.service.delete('user_types/' + id)
    }
}