import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class HirarkiInternalRecoveryService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('webuser/hierarchyrecoveries?' + query_string)
    }

    getDetail(query_string, id) {
        return this.service.get('webuser/hierarchyrecoveries/' + id + '?' + query_string)
    }

    submit(data) {
        return this.service.post('webuser/hierarchyrecoveries', data)
    }

    getActiveInternalRecovery() {
        return this.service.get('webuser/internalrecoveries?is_active=true&is_warehouse=false')
    }

}