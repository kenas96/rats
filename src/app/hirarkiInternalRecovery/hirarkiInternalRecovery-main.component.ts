import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-hirarkiInternalRecovery',
    template: '<router-outlet></router-outlet>'
})

export class HirarkiInternalRecoveryMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}