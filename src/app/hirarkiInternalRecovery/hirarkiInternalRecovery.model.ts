export class HirarkiInternalRecovery {
    id: number
    position_id: number
    position_name: string
    nik: string
    name: string
    handphone: string
    email: string
    is_active: boolean


    constructor(data: any = {}) {
        this.id = data.id
        this.position_id = data.position_id
        this.position_name = data.position_name
        this.nik = data.nik
        this.name = data.name
        this.handphone = data.handphone
        this.email = data.email
        this.is_active = data.is_active
    }
}

export class HirarkiInternalRecoveryContent {
    id: number
    position_id: number
    position_name: string
    nik: string
    name: string
    handphone: string
    email: string
    is_active: boolean
    child_nik: string
    child_name: string
    child_email: string
    child_handphone: string
    child_position_name: string

    constructor(data: any = {}) {
        this.id = data.id
        this.position_id = data.position_id
        this.position_name = data.position_name
        this.nik = data.nik
        this.name = data.name
        this.handphone = data.handphone
        this.email = data.email
        this.is_active = data.is_active
        this.child_nik = data.child_nik
        this.child_name = data.child_name
        this.child_email = data.child_email
        this.child_handphone = data.child_handphone
        this.child_position_name = data.child_position_name
    }
}
