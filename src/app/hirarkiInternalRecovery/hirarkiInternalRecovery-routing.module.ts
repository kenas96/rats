import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { HirarkiInternalRecoveryMainComponent } from './hirarkiInternalRecovery-main.component'
import { HirarkiInternalRecoveryListComponent } from './list/hirarkiInternalRecovery-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: HirarkiInternalRecoveryMainComponent,
        children: [
            {
                path: '',
                component: HirarkiInternalRecoveryListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: HirarkiInternalRecoveryListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class HirarkiInternalRecoveryRoutingModule { }
