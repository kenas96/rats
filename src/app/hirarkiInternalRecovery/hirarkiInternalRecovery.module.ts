import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { HirarkiInternalRecoveryListComponent } from './list/hirarkiInternalRecovery-list.component'
import { HirarkiInternalRecoveryDetailComponent } from './detail/hirarkiInternalRecovery-detail.component'
import { HirarkiInternalRecoveryContentDetailComponent } from './contentDetail/hirarkiInternalRecovery-contentDetail.component'
import { HirarkiInternalRecoveryService } from './hirarkiInternalRecovery.service'
import { HirarkiInternalRecoveryRoutingModule } from './hirarkiInternalRecovery-routing.module'
import { HirarkiInternalRecoveryMainComponent } from './hirarkiInternalRecovery-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Validator } from '../control/validator'
import { Alert } from '../control/util'
import { SelectModule } from 'ng2-select'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule,
        HirarkiInternalRecoveryRoutingModule,
        SelectModule
    ],
    exports: [],
    declarations: [
        HirarkiInternalRecoveryListComponent,
        HirarkiInternalRecoveryDetailComponent,
        HirarkiInternalRecoveryMainComponent,
        HirarkiInternalRecoveryContentDetailComponent
    ],
    providers: [
        HirarkiInternalRecoveryService,
        HttpService,
        AuthGuard,
        Validator,
        Alert
    ],
})
export class HirarkiInternalRecoveryModule { }
