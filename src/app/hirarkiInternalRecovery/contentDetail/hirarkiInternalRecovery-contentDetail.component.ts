import { Component, OnInit, Input } from '@angular/core'
import { FormGroup, AbstractControl } from '@angular/forms'
import { HirarkiInternalRecoveryContent } from './../hirarkiInternalRecovery.model'

@Component({
    selector: 'hirarkiInternalRecovery-contentDetail',
    templateUrl: './hirarkiInternalRecovery-contentDetail.component.html',
    styleUrls: ['./hirarkiInternalRecovery-contentDetail.component.css']
})

export class HirarkiInternalRecoveryContentDetailComponent implements OnInit {
    constructor() { }

    @Input() id: string
    @Input() hirarkiInternalRecoveryContent: HirarkiInternalRecoveryContent

    ngOnInit() {

    }
}