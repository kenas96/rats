import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { ParamCoverageRecoveryListComponent } from './list/paramCoverageRecovery-list.component'
import { ParamCoverageRecoveryDetailComponent } from './detail/paramCoverageRecovery-detail.component'
import { ParamCoverageRecoveryContentDetailComponent } from './contentDetail/paramCoverageRecovery-contentDetail.component'
import { ParamCoverageRecoveryService } from './paramCoverageRecovery.service'
import { ParamCoverageRecoveryRoutingModule } from './paramCoverageRecovery-routing.module'
import { ParamCoverageRecoveryMainComponent } from './paramCoverageRecovery-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Validator } from '../control/validator'
import { Alert } from '../control/util'
import { SelectModule } from 'ng2-select'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule,
        ParamCoverageRecoveryRoutingModule,
        SelectModule
    ],
    exports: [],
    declarations: [
        ParamCoverageRecoveryListComponent,
        ParamCoverageRecoveryContentDetailComponent,
        ParamCoverageRecoveryMainComponent,
        ParamCoverageRecoveryDetailComponent
    ],
    providers: [
        ParamCoverageRecoveryService,
        HttpService,
        AuthGuard,
        Validator,
        Alert
    ],
})
export class ParamCoverageRecoveryModule { }
