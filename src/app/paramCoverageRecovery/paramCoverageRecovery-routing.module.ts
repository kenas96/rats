import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ParamCoverageRecoveryMainComponent } from './paramCoverageRecovery-main.component'
import { ParamCoverageRecoveryListComponent } from './list/paramCoverageRecovery-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: ParamCoverageRecoveryMainComponent,
        children: [
            {
                path: '',
                component: ParamCoverageRecoveryListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: ParamCoverageRecoveryListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class ParamCoverageRecoveryRoutingModule { }
