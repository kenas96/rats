import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-paramCoverageRecovery',
    template: '<router-outlet></router-outlet>'
})

export class ParamCoverageRecoveryMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}