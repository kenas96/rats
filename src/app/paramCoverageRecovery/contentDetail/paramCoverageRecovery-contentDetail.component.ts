import { Component, OnInit, Input } from '@angular/core'
import { ParamCoverageRecoveryContent } from './../paramCoverageRecovery.model'

@Component({
    selector: 'paramCoverageRecovery-contentDetail',
    templateUrl: './paramCoverageRecovery-contentDetail.component.html',
    styleUrls: ['./paramCoverageRecovery-contentDetail.component.css']
})

export class ParamCoverageRecoveryContentDetailComponent implements OnInit {
    constructor() { }

    @Input() id: string
    @Input() paramCoverageRecoveryContent: ParamCoverageRecoveryContent

    ngOnInit() {

    }
}