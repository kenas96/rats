import { Component, OnInit, Input, ViewChild } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { ParamCoverageRecovery, ParamCoverageRecoveryContent } from './../paramCoverageRecovery.model'
import { ParamCoverageRecoveryService } from './../paramCoverageRecovery.service'
import { Alert } from '../../control/util'
import 'rxjs/add/operator/debounceTime'
import swal from 'sweetalert2'

@Component({
  selector: 'paramCoverageRecovery-detail',
  templateUrl: './paramCoverageRecovery-detail.component.html',
  styleUrls: ['./paramCoverageRecovery-detail.component.css']
})

export class ParamCoverageRecoveryDetailComponent implements OnInit {
  constructor(
    private service: ParamCoverageRecoveryService,
    private alert: Alert
  ) { }

  @Input() id: string
  @Input() paramCoverageRecovery: ParamCoverageRecovery
  @Input() state: number
  @Input() displayData: Function
  @Input() getDetail: Function
  @Input() paramCoverageRecoveryForm: FormGroup

  @Input() columns = []
  @Input() listCriteria = []
  @Input() data = {
    rows: [],
    area_recoveries: [],
    internal_recoveries: [],
  }
  @Input() dataSelected = {
    rows: []
  }
  @Input() option = {
    page: 1,
    pageOffset: 0,
    pageSize: 10,
    criteria: {},
    disPrev: null,
    disNext: null,
    order: { column: 'id', ascending: true }
  }

  paramCoverageRecoveryContent: ParamCoverageRecoveryContent
  viewCallback: Function
  editCallback: Function
  deleteCallback: Function
  getDetailCallback: Function
  @ViewChild('arearecoveryselect') public arearecoveryselect: any
  @ViewChild('internalrecoveryselect') public internalrecoveryselect: any

  @Input() permission = {}

  ngOnInit() {
    this.paramCoverageRecoveryContent = new ParamCoverageRecoveryContent()
    this.viewCallback = this.view.bind(this)
    this.editCallback = this.edit.bind(this)
    this.deleteCallback = this.remove.bind(this)
    this.getDetailCallback = this.getDetail.bind(this)
  }

  public selectedAreaRecovery(value: any): void {
    this.paramCoverageRecovery.area_recovery_id = value.id
    this.alert.loading_start()
    this.service.getInternalRecovery(value.id).then(
      res => {
        if (Array.isArray(res.data)) {
          this.data.internal_recoveries = []
          for (let internal_recovery of res.data) {
            internal_recovery['text'] = internal_recovery['name']
            this.data.internal_recoveries.push(internal_recovery)
          }
        } else {
          this.data.internal_recoveries = [res.data]
        }
        this.alert.loading_stop()
      },
      err => {
        if (err.http_status === 422) {
          this.alert.error_alert(err.message)
        } else {
          this.alert.warn_alert(err.message)
        }
        this.alert.loading_stop()
      }
    )
  }

  public selectedInternalRecovery(value: any): void {
    this.paramCoverageRecovery.internal_recovery_id = value.id
    this.paramCoverageRecoveryForm.patchValue({
      search_area_recovery: this.paramCoverageRecovery.area_recovery_id,
      search_internal_recovery: this.paramCoverageRecovery.internal_recovery_id
    })
  }

  search() {
    this.dataSelected.rows = []
    let tmp_query_all = [
      { param: "page_size", value: 1000000 },
      { param: "page_offset", value: 0 },
      { param: "order_column", value: "id" },
      { param: "order_ascending", value: true }
    ]
    let que_str_all = tmp_query_all.map(function (el) {
      return el.param + '=' + el.value
    }).join('&')
    this.service.getDetail(que_str_all, this.paramCoverageRecovery.area_recovery_id, this.paramCoverageRecovery.internal_recovery_id).then(
      res => {
        for (var i = 0; i < res.data.branches.length; i++) {
          if (res.data.branches[i].checked) {
            this.dataSelected.rows.push(res.data.branches[i])
          }
        }
      },
      err => {
        if (err.http_status === 422) {
          this.alert.error_alert(err.message)
        } else {
          this.alert.warn_alert(err.message)
        }
      }
    )
    this.getDetail()
    this.arearecoveryselect.active = []
    this.internalrecoveryselect.active = []
    this.paramCoverageRecoveryForm.patchValue({
      search_area_recovery: '',
      search_internal_recovery: ''
    })
  }

  view(obj) {
    this.paramCoverageRecoveryContent = obj
    $('#modal-contentDetail').modal()
  }

  edit(obj) {

  }

  remove(obj) {

  }
  save() {
    swal({
      title: 'Are you sure?',
      text: "Your data will change permanently!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes!'
    }).then((result) => {
      if (result.value) {
        this.alert.loading_start()
        let obj = Object.assign({}, this.paramCoverageRecovery, this.paramCoverageRecoveryForm.value)
        obj.search_nik = undefined
        obj.request_id = Date.now()
        obj.branches = this.dataSelected.rows
        this.service.submit(obj).then(
          res => {
            this.alert.success_alert('Your data has been saved', this.id, this.displayData())
          },
          err => {
            if (err.http_status === 422) {
              this.alert.error_alert(err.message)
            } else {
              this.alert.warn_alert(err.message)
            }
          }
        )
      }
    })

  }
}
