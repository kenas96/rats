export class ParamCoverageRecovery {
  id: number
  area_recovery_id: number
  area_recovery_name: string
  internal_recovery_id: number
  internal_recovery_name: string

  constructor(data: any = {}) {
    this.id = data.id
    this.area_recovery_id = data.area_recovery_id
    this.area_recovery_name = data.area_recovery_name
    this.internal_recovery_id = data.internal_recovery_id
    this.internal_recovery_name = data.internal_recovery_name
  }
}

export class ParamCoverageRecoveryContent {
  id: number
  area_recovery_id: number
  area_recovery_name: string
  internal_recovery_id: number
  internal_recovery_name: string
  name: string

  constructor(data: any = {}) {
    this.id = data.id
    this.area_recovery_id = data.area_recovery_id
    this.area_recovery_name = data.area_recovery_name
    this.internal_recovery_id = data.internal_recovery_id
    this.internal_recovery_name = data.internal_recovery_name
    this.name = data.name
  }
}

