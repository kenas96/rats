import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class ParamCoverageRecoveryService {

  constructor(private service: HttpService) { }

  getPaging(query_string) {
    return this.service.get('webuser/coveragerecoveries?' + query_string)
  }

  getAreaRecovery() {
    return this.service.get('area_recoveries/mapped')
  }

  getInternalRecovery(area_id) {
    return this.service.get('webuser/internalrecoveries/coverage/' + area_id)
  }

  // getActiveInternalRecovery() {
  //   return this.service.get('webuser/internalrecoveries?is_active=true')
  // }

  getDetail(query_string, area_recovery_id, internal_recovery_id) {
    return this.service.get('webuser/coveragerecoveries/mapping?area_recovery_id=' + area_recovery_id + '&internal_recovery_id=' +
      internal_recovery_id + '&' + query_string)
  }

  submit(data) {
    return this.service.post('webuser/coveragerecoveries/mapping', data)
  }

}
