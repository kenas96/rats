import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class DashboardService {

    constructor(private service: HttpService) { }

    getDashboard() {
        return this.service.get('dashboard')
    }

}