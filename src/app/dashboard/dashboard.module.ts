import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'
import { HttpModule } from '@angular/http'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { HttpService } from '../http.service'
import { AuthGuard } from '../pages/pages-guard.provider'
import { DashboardMainComponent } from './dashboard-main.component'
import { DashboardComponent } from './dashboard.component'
import { DashboardRoutingModule } from './dashboard.routing'
import { DashboardService } from './dashboard.service'
import { ChartModule } from 'angular2-chartjs'

@NgModule({
    imports: [
        FormsModule,
        HttpModule,
        DashboardRoutingModule,
        RouterModule,
        CommonModule,
        ControlModule,
        ChartModule
    ],
    declarations: [
        DashboardComponent,
        DashboardMainComponent
    ],
    providers: [
        DashboardService,
        HttpService,
        AuthGuard
    ],
})
export class DashboardModule { }
