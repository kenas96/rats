import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'dashboard-main',
    template: `
        <dashboard-component></dashboard-component>
        <router-outlet></router-outlet>
    `
})

export class DashboardMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }

}