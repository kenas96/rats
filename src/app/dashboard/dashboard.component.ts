import { Component, OnInit } from '@angular/core'
import { DashboardService } from './dashboard.service'
import { Alert } from '../control/util'

@Component({
    selector: 'dashboard-component',
    templateUrl: './dashboard.component.html',
    styleUrls: ["dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
    constructor(
        private service: DashboardService,
        private alert: Alert
    ) { }

    dataDashboard = {
        userTotalExternalRecovery: 0,
        userTotalExternalRecoveryDevinitive: 0,
        userTotalExternalRecoveryShadow: 0,
        Assignments: [
            {
                total_assignment: 0
            }
        ],
        InternalRecovery: []
    }

    typeChart = 'pie';
    dataChart = {
        labels: ["Waiting for assignment", "Assigned", "Done", "Send to Warehouse"],
        datasets: [
            {
                label: "Assignment",
                data: [0, 0, 0, 0],
                backgroundColor: [
                    "rgba(183, 0, 0, 0.2)",
                    "rgba(229, 137, 0, 0.2)",
                    "rgba(0, 135, 4, 0.2)",
                    "rgba(255, 255, 0, 0.2)"
                ],
                borderColor: [
                    "rgb(183, 0, 0)",
                    "rgb(229, 137, 86)",
                    "rgb(0, 135, 4)",
                    "rgb(255, 255, 0)"
                ],
            }
        ],
    };
    optionsChart = {
        responsive: true,
        maintainAspectRatio: false
    };

    columnsTable = [
        { text: 'Nama', field: 'name' },
        { text: 'NIK', field: 'nik' },
        { text: 'Email', field: 'email' },
        { text: 'Position', field: 'position_name' },
        { text: 'Status', field: 'is_active' }
    ];

    ngOnInit() {

    }

    ngAfterViewInit() {
        this.displayData()
    }

    loadingAnimationGridStart() {
        document.getElementById('dataGrid').style.display = "none"
        document.getElementById('loadingGrid').style.display = "block"
    }

    loadingAnimationGridStop() {
        document.getElementById('dataGrid').style.display = "block"
        document.getElementById('loadingGrid').style.display = "none"
    }

    displayData() {
        this.loadingAnimationGridStart()
        this.service.getDashboard().then(
            res => {
                this.dataDashboard = res.data
                this.dataChart.datasets[0].data = []
                this.dataChart.datasets[0].data.push(this.dataDashboard.Assignments[0].total_assignment)
                this.dataChart.datasets[0].data.push(this.dataDashboard.Assignments[1].total_assignment)
                this.dataChart.datasets[0].data.push(this.dataDashboard.Assignments[2].total_assignment)
                this.dataChart.datasets[0].data.push(this.dataDashboard.Assignments[3].total_assignment)
                for (let entry of this.dataDashboard.InternalRecovery) {
                    if (entry['is_active']) {
                        entry['is_active'] = 'Active'
                    } else {
                        entry['is_active'] = 'Not Active'
                    }
                }
                this.loadingAnimationGridStop()
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
                this.loadingAnimationGridStop()
            }
        )
    }

}

