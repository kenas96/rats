import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { DashboardMainComponent } from './dashboard-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: DashboardMainComponent,
        children: [
            {
                path: '',
                component: DashboardMainComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: DashboardMainComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class DashboardRoutingModule { }
