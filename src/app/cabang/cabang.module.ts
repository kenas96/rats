import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { CabangListComponent } from './list/cabang-list.component'
import { CabangDetailComponent } from './detail/cabang-detail.component'
import { CabangService } from './cabang.service'
import { CabangRoutingModule } from './cabang-routing.module'
import { CabangMainComponent } from './cabang-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        HttpModule,
        RouterModule,
        CabangRoutingModule
    ],
    exports: [],
    declarations: [
        CabangListComponent,
        CabangDetailComponent,
        CabangMainComponent
    ],
    providers: [
        CabangService,
        HttpService,
        AuthGuard,
        Alert
    ],
})
export class CabangModule { }
