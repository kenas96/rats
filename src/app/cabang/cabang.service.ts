import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class CabangService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('branches?' + query_string)
    }

    getDetail(id) {
        return this.service.get('branches/' + id)
    }
}