import { Component, OnInit, Input } from '@angular/core'
import { Cabang } from './../cabang.model'

@Component({
    selector: 'cabang-detail',
    templateUrl: './cabang-detail.component.html',
    styleUrls: ['./cabang-detail.component.css']
})

export class CabangDetailComponent implements OnInit {
    constructor(
    ) { }

    @Input() id: string
    @Input() cabang: Cabang

    ngOnInit() {
    }
}