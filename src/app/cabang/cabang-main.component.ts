import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-cabang',
    template: '<router-outlet></router-outlet>'
})

export class CabangMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}