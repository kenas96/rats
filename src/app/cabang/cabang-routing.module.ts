import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { CabangMainComponent } from './cabang-main.component'
import { CabangListComponent } from './list/cabang-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: CabangMainComponent,
        children: [
            {
                path: '',
                component: CabangListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: CabangListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class CabangRoutingModule { }
