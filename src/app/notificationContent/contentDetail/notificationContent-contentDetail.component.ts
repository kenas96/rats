import { Component, OnInit, Input } from '@angular/core'
import { DataContent } from './../notificationContent.model'

@Component({
    selector: 'notificationContent-contentDetail',
    templateUrl: './notificationContent-contentDetail.component.html',
    styleUrls: ['./notificationContent-contentDetail.component.css']
})

export class NotificationContentContentDetailComponent implements OnInit {
    constructor() { }

    @Input() id: string
    @Input() dataContent: DataContent

    ngOnInit() {

    }
}