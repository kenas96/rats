import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-notificationContent',
    template: '<router-outlet></router-outlet>'
})

export class NotificationContentMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}