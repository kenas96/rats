export class NotificationContent {
    request_id: number
    id: number
    name: string
    description: string

    constructor(data: any = {}) {
        this.request_id = data.request_id
        this.id = data.id
        this.name = data.name
        this.description = data.description
    }
}

export class DataContent {
    request_id: number
    id: number
    text: string
    column: string
    table : string
    code: string
    description: string

    constructor(data: any = {}) {
        this.request_id = data.request_id
        this.id = data.id
        this.text = data.text
        this.column = data.column
        this.table = data.table
        this.code = data.code
        this.description = data.description
    }
}

