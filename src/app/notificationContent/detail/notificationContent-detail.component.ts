import { Component, OnInit, Input } from '@angular/core'
import { FormGroup, AbstractControl } from '@angular/forms'
import { NotificationContent, DataContent } from './../notificationContent.model'
import { NotificationContentService } from './../notificationContent.service'
import { Alert } from '../../control/util'
import 'rxjs/add/operator/debounceTime'
import swal from 'sweetalert2'

@Component({
    selector: 'notificationContent-detail',
    templateUrl: './notificationContent-detail.component.html',
    styleUrls: ['./notificationContent-detail.component.css']
})

export class NotificationContentDetailComponent implements OnInit {
    constructor(
        private service: NotificationContentService,
        private alert: Alert
    ) { }

    @Input() id: string
    @Input() notificationContent: NotificationContent
    @Input() state: number
    @Input() displayData: Function
    @Input() getDetail: Function
    @Input() notificationContentForm: FormGroup

    @Input() columns = []
    @Input() listCriteria = []
    @Input() data = {
        rows: []
    }
    @Input() dataSelected = {
        rows: []
    }
    @Input() option = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: null,
        disNext: null,
        order: { column: 'id', ascending: true }
    }

    dataContent: DataContent
    viewCallback: Function
    editCallback: Function
    deleteCallback: Function
    getDetailCallback: Function
    nameMessage: string
    descriptionMessage: string

    private validationMessage = {
        required: "Please fill this field.",
        minlength: "Min. character allowed are 3 characters.",
        maxlength: "Max. character allowed are 255 characters.",
        string: "Please enter a valid character format.",
        pattern: "Please enter a valid emaill address."
    }

    @Input() permission = {}

    ngOnInit() {
        this.dataContent = new DataContent()
        this.viewCallback = this.view.bind(this)
        this.editCallback = this.edit.bind(this)
        this.deleteCallback = this.remove.bind(this)
        this.getDetailCallback = this.getDetail.bind(this)

        const nameControl = this.notificationContentForm.get('name')
        nameControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageName(nameControl)
        )

        const descriptionControl = this.notificationContentForm.get('description')
        descriptionControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageDescription(descriptionControl)
        )
    }

    setMessageName(c: AbstractControl): void {
        this.nameMessage = ''
        if ((c.touched || c.dirty) && c.errors) {
            this.nameMessage = Object.keys(c.errors).map(key =>
                this.validationMessage[key]
            )[0]
        }

        if (this.state === 0) {
            this.notificationContentForm.get('name').disable()
        } else {
            this.notificationContentForm.get('name').enable()
        }
    }

    setMessageDescription(c: AbstractControl): void {
        this.descriptionMessage = ''
        if ((c.touched || c.dirty) && c.errors) {
            this.descriptionMessage = Object.keys(c.errors).map(key =>
                this.validationMessage[key]
            )[0]
        }

        if (this.state === 0) {
            this.notificationContentForm.get('description').disable()
        } else {
            this.notificationContentForm.get('description').enable()
        }
    }

    view(obj) {
        this.dataContent = obj
        $('#modal-contentDetail').modal()
    }

    edit(obj) {

    }

    remove(obj) {

    }

    save() {
        if (this.state === 1) {
            this.alert.loading_start()
            let obj = Object.assign({}, this.notificationContent, this.notificationContentForm.value)
            obj.request_id = Date.now()
            obj.id = undefined
            this.service.submitContent(obj).then(
                res => {
                    this.alert.success_alert('Your data has been added', this.id, this.displayData())
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                }
            )
        } else {
            swal({
                title: 'Are you sure?',
                text: "Your data will change permanently!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
            }).then((result) => {
                if (result.value) {
                    this.alert.loading_start()
                    let obj = Object.assign({}, this.notificationContent, this.notificationContentForm.value)
                    obj.request_id = Date.now()
                    obj.data_contents = this.dataSelected.rows
                    this.service.submitMapping(obj).then(
                        res => {
                            this.alert.success_alert('Your data has been saved', this.id, this.displayData())
                        },
                        err => {
                            if (err.http_status === 422) {
                                this.alert.error_alert(err.message)
                            } else {
                                this.alert.warn_alert(err.message)
                            }
                        }
                    )
                }
            })
        }


    }
}