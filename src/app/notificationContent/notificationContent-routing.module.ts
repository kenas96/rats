import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { NotificationContentMainComponent } from './notificationContent-main.component'
import { NotificationContentListComponent } from './list/notificationContent-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: NotificationContentMainComponent,
        children: [
            {
                path: '',
                component: NotificationContentListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: NotificationContentListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class NotificationContentRoutingModule { }
