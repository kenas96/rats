import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { NotificationContentListComponent } from './list/notificationContent-list.component'
import { NotificationContentDetailComponent } from './detail/notificationContent-detail.component'
import { NotificationContentContentDetailComponent } from './contentDetail/notificationContent-contentDetail.component'
import { NotificationContentService } from './notificationContent.service'
import { NotificationContentRoutingModule } from './notificationContent-routing.module'
import { NotificationContentMainComponent } from './notificationContent-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Validator } from '../control/validator'
import { Alert } from '../control/util'
import { SelectModule } from 'ng2-select'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule,
        NotificationContentRoutingModule,
        SelectModule
    ],
    exports: [],
    declarations: [
        NotificationContentListComponent,
        NotificationContentContentDetailComponent,
        NotificationContentMainComponent,
        NotificationContentDetailComponent
    ],
    providers: [
        NotificationContentService,
        HttpService,
        AuthGuard,
        Validator,
        Alert
    ],
})
export class NotificationContentModule { }
