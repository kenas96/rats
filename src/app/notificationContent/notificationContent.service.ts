import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class NotificationContentService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('webuser/parametercontents?' + query_string)
    }

    getDetail(query_string, id) {
        return this.service.get('webuser/parametercontents/mapping/' + id + '?' + query_string)
    }

    submitMapping(data) {
        return this.service.post('webuser/parametercontents/mapping', data)
    }

    submitContent(data) {
        return this.service.post('webuser/parametercontents', data)
    }

    deleteContent(id) {
        return this.service.delete('webuser/parametercontents/' + id)
    }

}