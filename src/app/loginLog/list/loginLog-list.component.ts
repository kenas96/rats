import { Component, OnInit } from '@angular/core'
import { LoginLogService } from '../loginLog.service'
import { Alert } from '../../control/util'

@Component({
    selector: 'loginLog-list',
    templateUrl: './loginLog-list.component.html',
    styleUrls: ['./loginLog-list.component.css']
})

export class LoginLogListComponent implements OnInit {
    constructor(
        private service: LoginLogService,
        private alert: Alert
    ) { }

    lengthData: number = 0
    displayCallback: Function
    viewCallback: Function
    editCallback: Function
    deleteCallback: Function

    data = {
        rows: []
    }

    option: any = {
        page: 1,
        pageOffset: 0,
        pageSize: 10,
        criteria: {},
        disPrev: false,
        disNext: false,
        order: { column: 'id', ascending: true }
    }

    columns = [
        { text: 'Name', field: 'name', value: 'name' },
        { text: 'Login Time', field: 'insert_date', value: 'insert_date' }
    ]

    listCriteria = [
        { text: 'Name', value: 'name' }
    ]

    permission: any = {
        create: false,
        view: false,
        update: false,
        delete: false,
        upload: false
    }

    CheckBtnPrevNext() {
        if (this.option.page === 1) {
            this.option.disPrev = true
        } else {
            this.option.disPrev = false
        }
        if ((this.option.page === 1 || this.option.page > 1) && this.lengthData <= this.option.pageSize) {
            this.option.disNext = true
        } else {
            this.option.disNext = false
        }
    }

    ngOnInit() {
        this.displayCallback = this.displayData.bind(this)
        this.viewCallback = this.view.bind(this)
        this.editCallback = this.edit.bind(this)
        this.deleteCallback = this.remove.bind(this)
    }

    ngAfterViewInit() {
        this.displayData()
    }

    loadingAnimationGridStart() {
        document.getElementById('dataGrid').style.display = "none"
        document.getElementById('loadingGrid').style.display = "block"
    }

    loadingAnimationGridStop() {
        document.getElementById('dataGrid').style.display = "block"
        document.getElementById('loadingGrid').style.display = "none"
    }

    parseData() {
        for (let entry of this.data.rows) {
            entry['insert_date'] = new Date(entry['insert_date']).toLocaleDateString() + ' ' + new Date(entry['insert_date']).toLocaleTimeString()
        }
    }

    displayData() {
        this.loadingAnimationGridStart()
        let tmp_query = [
            { param: "page_size", value: parseInt(this.option.pageSize) + 1 },
            { param: "page_offset", value: parseInt(this.option.pageOffset) },
            { param: "order_column", value: this.option.order.column },
            { param: "order_ascending", value: this.option.order.ascending }
        ]
        if (Object.keys(this.option.criteria).length > 0) {
            tmp_query.push({ param: this.option.criteria.text, value: this.option.criteria.value })
        }
        let que_str = tmp_query.map(function (el) {
            return el.param + '=' + el.value
        }).join('&')
        this.service.getPaging(que_str).
            then(
                res => {
                    if (Array.isArray(res.data)) {
                        this.lengthData = res.data.length
                        this.data.rows = res.data
                        if (this.lengthData > this.option.pageSize) {
                            let idx = this.lengthData - 1
                            this.data.rows.splice(idx, 1)
                        }
                    } else {
                        this.data.rows = [res.data]
                    }
                    this.CheckBtnPrevNext()
                    this.parseData()
                    this.loadingAnimationGridStop()
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                    this.loadingAnimationGridStop()
                }
            )
    }

    view(obj) {
    }

    edit(obj) {
    }

    remove(obj) {
    }
}