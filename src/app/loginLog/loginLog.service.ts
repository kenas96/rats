import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class LoginLogService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('loginlogs?' + query_string)
    }

}