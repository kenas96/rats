import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { LoginLogMainComponent } from './loginLog-main.component'
import { LoginLogListComponent } from './list/loginLog-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: LoginLogMainComponent,
        children: [
            {
                path: '',
                component: LoginLogListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: LoginLogListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class LoginLogRoutingModule { }
