import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { LoginLogListComponent } from './list/loginLog-list.component'
import { LoginLogService } from './loginLog.service'
import { LoginLogRoutingModule } from './loginLog-routing.module'
import { LoginLogMainComponent } from './loginLog-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        HttpModule,
        RouterModule,
        LoginLogRoutingModule
    ],
    exports: [],
    declarations: [
        LoginLogListComponent,
        LoginLogMainComponent
    ],
    providers: [
        LoginLogService,
        HttpService,
        AuthGuard,
        Alert
    ],
})
export class LoginLogModule { }
