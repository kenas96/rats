import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-loginLog',
    template: '<router-outlet></router-outlet>'
})

export class LoginLogMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}