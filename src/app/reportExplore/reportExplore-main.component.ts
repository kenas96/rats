import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-reportExplore',
    template: '<router-outlet></router-outlet>'
})

export class ReportExploreMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}