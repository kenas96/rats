import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { ReportExploreListComponent } from './list/reportExplore-list.component'
import { ReportExploreService } from './reportExplore.service'
import { ReportExploreRoutingModule } from './reportExplore-routing.module'
import { ReportExploreMainComponent } from './reportExplore-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        HttpModule,
        RouterModule,
        ReportExploreRoutingModule
    ],
    exports: [],
    declarations: [
        ReportExploreListComponent,
        ReportExploreMainComponent
    ],
    providers: [
        ReportExploreService,
        HttpService,
        AuthGuard,
        Alert
    ],
})
export class ReportExploreModule { }
