import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ReportExploreMainComponent } from './reportExplore-main.component'
import { ReportExploreListComponent } from './list/reportExplore-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: ReportExploreMainComponent,
        children: [
            {
                path: '',
                component: ReportExploreListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: ReportExploreListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class ReportExploreRoutingModule { }
