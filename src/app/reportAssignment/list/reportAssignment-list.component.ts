import { Component, OnInit } from '@angular/core'
import { ReportAssignmentService } from '../reportAssignment.service'
import { Alert } from '../../control/util'
import { AppConfig } from '../../config/app.config';

@Component({
  selector: 'reportAssignment-list',
  templateUrl: './reportAssignment-list.component.html',
  styleUrls: ['./reportAssignment-list.component.css']
})

export class ReportAssignmentListComponent implements OnInit {
  constructor(
    private service: ReportAssignmentService,
    private alert: Alert,
    private config: AppConfig) {
    this.base = this.config.getConfig('base_url')
  }

  lengthData: number = 0
  displayCallback: Function
  downloadReportCallback: Function
  downloadFullReportCallback: Function
  base: string

  data = {
    rows: []
  }

  option: any = {
    page: 1,
    pageOffset: 0,
    pageSize: 10,
    criteria: [{}],
    disPrev: false,
    disNext: false,
    order: { column: 'id', ascending: true }
  }

  columns = [
    { text: 'No Contract', field: 'no_kontrak', value: 'no_kontrak' },
    { text: 'No SK', field: 'no_sk', value: 'no_sk' },
    { text: 'Assigned Date', field: 'assigned_date', value: 'assigned_date' },
    { text: 'Serah Terima Date', field: 'serah_terima_date', value: 'serah_terima_date' },
    { text: 'Assignment Status', field: 'assignment_status', value: 'assignment_status' }
  ]

  listCriteria = [
    { text: 'No Contract', value: 'no_kontrak', filter: '', type: 'text' },
    { text: 'No SK', value: 'no_sk', filter: '', type: 'text' },
    { text: 'Assigned Date', value: 'assigned_date', filter: '', type: 'date' },
    { text: 'Serah Terima Date', value: 'serah_terima_date', filter: '', type: 'date' },
  ]

  CheckBtnPrevNext() {
    if (this.option.page === 1) {
      this.option.disPrev = true
    } else {
      this.option.disPrev = false
    }
    if ((this.option.page === 1 || this.option.page > 1) && this.lengthData <= this.option.pageSize) {
      this.option.disNext = true
    } else {
      this.option.disNext = false
    }
  }

  ngOnInit() {
    this.displayCallback = this.displayData.bind(this)
    this.downloadReportCallback = this.downloadReport.bind(this)
    this.downloadFullReportCallback = this.downloadFullReport.bind(this)
  }

  ngAfterViewInit() {
    this.displayData()
  }

  loadingAnimationGridStart() {
    document.getElementById('dataGrid').style.display = "none"
    document.getElementById('loadingGrid').style.display = "block"
  }

  loadingAnimationGridStop() {
    document.getElementById('dataGrid').style.display = "block"
    document.getElementById('loadingGrid').style.display = "none"
  }

  parseData() {
    for (let entry of this.data.rows) {
      if (entry['assigned_date'] !== null) {
        entry['assigned_date'] = new Date(entry['assigned_date'] - 25200000).toLocaleDateString()
      } else {
        entry['assigned_date'] = '-'
      }

      if (entry['serah_terima_date'] !== null) {
        entry['serah_terima_date'] = new Date(entry['serah_terima_date'] - 25200000).toLocaleDateString()
      } else {
        entry['serah_terima_date'] = '-'
      }

      if (entry['no_kontrak'] === null) {
        entry['no_kontrak'] = '-'
      }

      if (entry['no_sk'] === null) {
        entry['no_sk'] = '-'
      }
    }
  }

  refresh() {
    this.listCriteria = [
      { text: 'No Contract', value: 'no_kontrak', filter: '', type: 'text' },
      { text: 'No SK', value: 'no_sk', filter: '', type: 'text' },
      { text: 'Assigned Date', value: 'assigned_date', filter: '', type: 'date' },
      { text: 'Serah Terima Date', value: 'serah_terima_date', filter: '', type: 'date' },
    ]
    this.option.criteria = []
    this.displayData()
  }

  displayData() {
    this.loadingAnimationGridStart()
    let tmp_query = [
      { param: "page_size", value: parseInt(this.option.pageSize) + 1 },
      { param: "page_offset", value: parseInt(this.option.pageOffset) },
      { param: "order_column", value: this.option.order.column },
      { param: "order_ascending", value: this.option.order.ascending }
    ]
    var flag = true
    if (Object.keys(this.option.criteria).length > 0) {
      for (let crt of this.option.criteria) {
        // if (crt.value !== "") {
        if (crt.text === 'assigned_date') {
          var date_part = crt.value.split("-")
          if (date_part[0].length > 4) {
            this.alert.error_alert("Please input correct assigned date format!")
            flag = false
          }
        }
        if (crt.text === 'serah_terima_date') {
          var date_part = crt.value.split("-")
          if (date_part[0].length > 4) {
            this.alert.error_alert("Please input correct serah terima date format!")
            flag = false
          }
        }

        var exist = false
        for (let query of tmp_query) {
          if (query.param === crt.text) {
            query.value = crt.value
            exist = true
          }
        }
        if (!exist) {
          tmp_query.push({ param: crt.text, value: crt.value })
        }
        //}
      }
    }
    //this.option.criteria = []
    if (flag) {
      let que_str = tmp_query.map(function (el) {
        return el.param + '=' + el.value
      }).join('&')
      this.service.getPaging(que_str).then(
        res => {
          if (Array.isArray(res.data)) {
            this.lengthData = res.data.length
            this.data.rows = res.data
            if (this.lengthData > this.option.pageSize) {
              let idx = this.lengthData - 1
              this.data.rows.splice(idx, 1)
            }
          } else {
            this.data.rows = [res.data]
          }
          this.CheckBtnPrevNext()
          this.parseData()
          this.loadingAnimationGridStop()
        },
        err => {
          if (err.http_status === 422) {
            this.alert.error_alert(err.message)
          } else {
            this.alert.warn_alert(err.message)
          }
          this.loadingAnimationGridStop()
        }
      )
    } else {
      this.loadingAnimationGridStop()
    }
  }

  downloadReport() {
    let tmp_query = [
      { param: "page_size", value: parseInt(this.option.pageSize) },
      { param: "page_offset", value: parseInt(this.option.pageOffset) },
      { param: "order_column", value: this.option.order.column },
      { param: "order_ascending", value: this.option.order.ascending }
    ]
    var flag = true
    if (Object.keys(this.option.criteria).length > 0) {
      for (let crt of this.option.criteria) {
        if (crt.value !== "") {
          if (crt.text === 'assigned_date') {
            var date_part = crt.value.split("-")
            if (date_part[0].length > 4) {
              this.alert.error_alert("Please input correct assigned date format!")
              flag = false
            }
          }
          if (crt.text === 'serah_terima_date') {
            var date_part = crt.value.split("-")
            if (date_part[0].length > 4) {
              this.alert.error_alert("Please input correct serah terima date format!")
              flag = false
            }
          }
          tmp_query.push({ param: crt.text, value: crt.value })
        }
      }
    }
    this.option.criteria = []
    if (flag) {
      let que_str = tmp_query.map(function (el) {
        return el.param + '=' + el.value
      }).join('&')
      this.service.downloadReport(que_str).then(
        res => {
          window.open(this.base + res.data.url)
        },
        err => {
          if (err.http_status === 422) {
            this.alert.error_alert(err.message)
          } else {
            this.alert.warn_alert(err.message)
          }
        }
      )
    }
  }

  downloadFullReport() {
    this.service.downloadFullReport().then(
      res => {
        window.open(this.base + res.data.url)
      },
      err => {
        if (err.http_status === 422) {
          this.alert.error_alert(err.message)
        } else {
          this.alert.warn_alert(err.message)
        }
      }
    )
  }
}
