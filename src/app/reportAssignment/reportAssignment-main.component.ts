import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-reportAssignment',
    template: '<router-outlet></router-outlet>'
})

export class ReportAssignmentMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}