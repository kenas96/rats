import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ReportAssignmentMainComponent } from './reportAssignment-main.component'
import { ReportAssignmentListComponent } from './list/reportAssignment-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: ReportAssignmentMainComponent,
        children: [
            {
                path: '',
                component: ReportAssignmentListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: ReportAssignmentListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class ReportAssignmentRoutingModule { }
