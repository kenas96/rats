import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { ReportAssignmentListComponent } from './list/reportAssignment-list.component'
import { ReportAssignmentService } from './reportAssignment.service'
import { ReportAssignmentRoutingModule } from './reportAssignment-routing.module'
import { ReportAssignmentMainComponent } from './reportAssignment-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        HttpModule,
        RouterModule,
        ReportAssignmentRoutingModule
    ],
    exports: [],
    declarations: [
        ReportAssignmentListComponent,
        ReportAssignmentMainComponent
    ],
    providers: [
        ReportAssignmentService,
        HttpService,
        AuthGuard,
        Alert
    ],
})
export class ReportAssignmentModule { }
