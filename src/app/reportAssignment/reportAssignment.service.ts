import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class ReportAssignmentService {

    constructor(private service: HttpService) { }

    getPaging(query_string) {
        return this.service.get('webuser/report/assignment?' + query_string)
    }

    downloadReport(query_string) {
        return this.service.get('webuser/report/assignment/generate?' + query_string)
    }

    downloadFullReport() {
        //maximum rows in excel
        return this.service.get('webuser/report/assignment/generate?page_size=1048576')
    }

}