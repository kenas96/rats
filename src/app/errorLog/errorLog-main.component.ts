import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-errorLog',
    template: '<router-outlet></router-outlet>'
})

export class ErrorLogMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}