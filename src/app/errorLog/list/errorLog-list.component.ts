import { Component, OnInit } from '@angular/core'
import { AppConfig } from '../../config/app.config';

@Component({
    selector: 'errorLog-list',
    templateUrl: './errorLog-list.component.html',
    styleUrls: ['./errorLog-list.component.css']
})

export class ErrorLogListComponent implements OnInit {
    constructor(private config: AppConfig) {
        this.base = this.config.getConfig('base_url')
    }

    base: string

    ngOnInit() {
    }

    ngAfterViewInit() {
        this.displayData()
    }

    loadingAnimationGridStart() {
        document.getElementById('dataGrid').style.display = "none"
        document.getElementById('loadingGrid').style.display = "block"
    }

    loadingAnimationGridStop() {
        document.getElementById('dataGrid').style.display = "block"
        document.getElementById('loadingGrid').style.display = "none"
    }

    displayData() {
        this.loadingAnimationGridStart()
        this.loadingAnimationGridStop()
    }

    downloadError() {
        window.open(this.base + 'errorlogs/error.log')
    }
}