import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ErrorLogMainComponent } from './errorLog-main.component'
import { ErrorLogListComponent } from './list/errorLog-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: ErrorLogMainComponent,
        children: [
            {
                path: '',
                component: ErrorLogListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: ErrorLogListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class ErrorLogRoutingModule { }
