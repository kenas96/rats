import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { ErrorLogListComponent } from './list/errorLog-list.component'
import { ErrorLogService } from './errorLog.service'
import { ErrorLogRoutingModule } from './errorLog-routing.module'
import { ErrorLogMainComponent } from './errorLog-main.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        HttpModule,
        RouterModule,
        ErrorLogRoutingModule
    ],
    exports: [],
    declarations: [
        ErrorLogListComponent,
        ErrorLogMainComponent
    ],
    providers: [
        ErrorLogService,
        HttpService,
        AuthGuard,
        Alert
    ],
})
export class ErrorLogModule { }
