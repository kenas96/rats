export class Admin {
    web_user_id: number
    name: string
    email: string
    web_user_type: number
    web_password: string

    constructor(webuserid?, name?, email?, webusertype?) {
        this.web_user_id = webuserid
        this.name = name
        this.email = email
        this.web_user_type = webusertype
    }
}

export class Login {
    username: string
    password: string

    constructor(data: any = {}) {
        this.username = data.username
        this.password = data.password
    }
}

export class LocalUser {
    role: string
    name: string
    email: string
    picture: string

    constructor(data: any = {}) {
        this.role = data.role
        this.name = data.name
        this.email = data.email
        this.picture = data.picture
    }
}

export class ChangePassword {
    username: string
    old_password: string
    new_password: string

    constructor(data: any = {}) {
        this.username = data.username
        this.old_password = data.old_password
        this.new_password = data.new_password
    }
}