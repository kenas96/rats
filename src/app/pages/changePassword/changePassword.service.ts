import { Injectable } from '@angular/core'
import { HttpService } from '../../http.service'

@Injectable()
export class ChangePasswordService {

    constructor(private service: HttpService) { }

    changePassword(data) {
        return this.service.post('change_password', data)
    }
}