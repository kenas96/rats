import { Component, OnInit, Input } from '@angular/core'
import { FormGroup, AbstractControl } from '@angular/forms'
import { ChangePasswordService } from './changePassword.service'
import { Alert } from '../../control/util'
import { ChangePassword } from '../admin.model'
import 'rxjs/add/operator/debounceTime'

@Component({
    selector: 'change-password',
    templateUrl: './changePassword.component.html',
    styleUrls: ['./changePassword.component.css']
})

export class changePasswordComponent implements OnInit {
    constructor(
        private service: ChangePasswordService,
        private alert: Alert) { }

    @Input() id: string
    @Input() initAdminData: Function
    @Input() changePasswordForm: FormGroup
    changePassword: ChangePassword
    webPasswordMessage: string
    newPasswordMessage: string
    confirmNewPasswordMessage: string

    private validationMessage = {
        required: "Please fill this field.",
        minlength: "Min. character allowed are 6 characters.",
        maxlength: "Max. character allowed are 20 characters."
    }

    ngOnInit() {
        const webPasswordControl = this.changePasswordForm.get('old_password')
        webPasswordControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageWebPassword(webPasswordControl)
        )

        const newPasswordControl = this.changePasswordForm.get('new_password')
        newPasswordControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageNewPassword(newPasswordControl)
        )

        const confirmNewPasswordControl = this.changePasswordForm.get('confirm_new_password')
        confirmNewPasswordControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageConfirmNewPassword(confirmNewPasswordControl)
        )
    }

    setMessageWebPassword(c: AbstractControl): void {
        this.webPasswordMessage = ''
        if ((c.touched || c.dirty) && c.errors) {
            this.webPasswordMessage = Object.keys(c.errors).map(key =>
                this.validationMessage[key]
            )[0]
        }
    }

    setMessageNewPassword(c: AbstractControl): void {
        this.newPasswordMessage = ''
        if ((c.touched || c.dirty) && c.errors) {
            this.newPasswordMessage = Object.keys(c.errors).map(key =>
                this.validationMessage[key]
            )[0]
        }
    }

    setMessageConfirmNewPassword(c: AbstractControl): void {
        this.confirmNewPasswordMessage = ''
        if ((c.touched || c.dirty) && c.errors) {
            this.confirmNewPasswordMessage = Object.keys(c.errors).map(key =>
                this.validationMessage[key]
            )[0]
        }
    }

    save() {
        this.alert.loading_start()
        let user = localStorage.getItem('auth-user')
        let obj = Object.assign({}, this.changePassword, this.changePasswordForm.value)
        let decoded: any = atob(user)
        for (var i = 0; i < decoded.length; i++) {
            if (decoded[i] === ':') {
                for (var j = i + 1; j < decoded.length; j++) {
                    if (decoded[j] === ':') {
                        for (var k = j + 1; k < decoded.length; k++) {
                            if (decoded[k] === ':') {
                                obj.username = decoded.substr(j + 1, k - j - 1)
                                break
                            }
                        }
                        break
                    }
                }
                break
            }
        }
        obj.confirm_new_password = undefined
        this.service.changePassword(obj).then(
            res => {
                this.alert.success_alert('Password successfully changed', this.id, this.initAdminData())
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
            }
        )

    }
}