import { Injectable } from '@angular/core'
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router } from '@angular/router'
import { JwtHelper } from 'angular2-jwt'

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router) { }
    jwtHelper: JwtHelper = new JwtHelper()
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let user = localStorage.getItem('auth-token')
        if (user === undefined || user === null || user === '') {
            this.router.navigate(['/login'])
            return false
        }
        else {
            if (this.jwtHelper.isTokenExpired(user) == true) {
                alert('Your session has been expired. Please Login Again.')
                this.router.navigate(['/login'])
                return false
            }
            else {
                return true
            }
        }
    }
}