import { NgModule } from '@angular/core'
import { PageMainComponent } from './page-main.component'
import { RouterModule, Routes } from '@angular/router'
import { AuthGuard } from './pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: PageMainComponent,
        children: [
            {
                path: 'dashboard',
                loadChildren: 'app/dashboard/dashboard.module#DashboardModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'position',
                loadChildren: 'app/position/position.module#PositionModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'warehouse',
                loadChildren: 'app/warehouse/warehouse.module#WarehouseModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'branches',
                loadChildren: 'app/cabang/cabang.module#CabangModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'remote_warehouse',
                loadChildren: 'app/remoteWarehouse/remoteWarehouse.module#RemoteWarehouseModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'area',
                loadChildren: 'app/area/area.module#AreaModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'area_recovery',
                loadChildren: 'app/areaRecovery/areaRecovery.module#AreaRecoveryModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'internal_recovery',
                loadChildren: 'app/internalRecovery/internalRecovery.module#InternalRecoveryModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'external_recovery',
                loadChildren: 'app/externalRecovery/externalRecovery.module#ExternalRecoveryModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'hirarki_internal_recovery',
                loadChildren: 'app/hirarkiInternalRecovery/hirarkiInternalRecovery.module#HirarkiInternalRecoveryModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'hirarki_external_recovery',
                loadChildren: 'app/hirarkiExternalRecovery/hirarkiExternalRecovery.module#HirarkiExternalRecoveryModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'parameter_area_recovery',
                loadChildren: 'app/paramAreaRecovery/paramAreaRecovery.module#ParamAreaRecoveryModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'parameter_coverage_recovery',
                loadChildren: 'app/paramCoverageRecovery/paramCoverageRecovery.module#ParamCoverageRecoveryModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'parameter_distribution',
                loadChildren: 'app/paramDistributor/paramDistributor.module#ParamDistributorModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'parameter_warehouse',
                loadChildren: 'app/paramWarehouseDashboard/paramWarehouseDashboard.module#ParamWarehouseDashboardModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'hirarki_warehouse',
                loadChildren: 'app/paramHirarkiWarehouse/paramHirarkiWarehouse.module#ParamHirarkiWarehouseModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'notification_content',
                loadChildren: 'app/notificationContent/notificationContent.module#NotificationContentModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'login_audit_trail',
                loadChildren: 'app/loginLog/loginLog.module#LoginLogModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'activity_audit_trail',
                loadChildren: 'app/activityLog/activityLog.module#ActivityLogModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'error_audit_trail',
                loadChildren: 'app/errorLog/errorLog.module#ErrorLogModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'report_assignment',
                loadChildren: 'app/reportAssignment/reportAssignment.module#ReportAssignmentModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'report_branch_coverage',
                loadChildren: 'app/reportBranchCoverage/reportBranchCoverage.module#ReportBranchCoverageModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'report_explore',
                loadChildren: 'app/reportExplore/reportExplore.module#ReportExploreModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'report_external_recovery',
                loadChildren: 'app/reportExternalRecovery/reportExternalRecovery.module#ReportExternalRecoveryModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'report_internal_recovery',
                loadChildren: 'app/reportInternalRecovery/reportInternalRecovery.module#ReportInternalRecoveryModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'report_internal_recovery_non_active',
                loadChildren: 'app/reportInternalRecoveryNonActive/reportInternalRecoveryNonActive.module#ReportInternalRecoveryNonActiveModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'report_hirarki_internal',
                loadChildren: 'app/reportHirarkiInternal/reportHirarkiInternal.module#ReportHirarkiInternalModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'report_hirarki_external',
                loadChildren: 'app/reportHirarkiExternal/reportHirarkiExternal.module#ReportHirarkiExternalModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'report_parameter_area_recovery',
                loadChildren: 'app/reportParameterAreaRecovery/reportParameterAreaRecovery.module#ReportParameterAreaRecoveryModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'report_parameter_warehouse',
                loadChildren: 'app/reportParameterWarehouse/reportParameterWarehouse.module#ReportParameterWarehouseModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'notification',
                loadChildren: 'app/notification/notification.module#NotificationModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'assignment_correction',
                loadChildren: 'app/correctionAssignment/correctionAssignment.module#CorrectionAssignmentModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'warehouse_correction',
                loadChildren: 'app/correctionWarehouse/correctionWarehouse.module#CorrectionWarehouseModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'user_mobile',
                loadChildren: 'app/userMobile/userMobile.module#UserMobileModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'account_management',
                loadChildren: 'app/webUser/webUser.module#WebUserModule',
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                redirectTo: '/pages',
                pathMatch: 'full',
                canActivate: [AuthGuard]
            }
        ]
    }
]



@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class PageRoutingModule { }
