import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Login } from '../../pages/admin.model'
import { LocalUser } from '../admin.model'
import { LoginService } from './../../auth/login/login.service'
import { Alert } from '../../control/util'
import swal from 'sweetalert2'

@Component({
  selector: 'nav-component',
  templateUrl: 'nav.component.html',
  styleUrls: ['nav.component.css']
})

export class NavComponent implements OnInit {
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private service: LoginService,
    private alert: Alert
  ) { }

  localUser: LocalUser
  initAdminDataCallback: Function
  changePasswordForm: FormGroup
  logout_obj: Login

  option = {
    page: 1,
    pageSize: 10,
    criteria: [],
    order: {
      column: 'id', direction: 'DESC'
    }
  }

  ngOnInit() {
    this.initAdminData()
    this.logout_obj = new Login()
    this.initAdminDataCallback = this.initAdminData.bind(this)
    this.changePasswordForm = this.fb.group({
      old_password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]],
      new_password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]],
      confirm_new_password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]]
    })
  }

  initAdminData() {
    let user = localStorage.getItem('auth-user')
    this.localUser = new LocalUser()
    let decoded: any = atob(user)
    for (var i = 0; i < decoded.length; i++) {
      if (decoded[i] === ':') {
        this.localUser.role = decoded.substr(0, i)
        for (var j = i + 1; j < decoded.length; j++) {
          if (decoded[j] === ':') {
            this.localUser.name = decoded.substr(i + 1, j - i - 1)
            for (var k = j + 1; k < decoded.length; k++) {
              if (decoded[k] === ':') {
                this.localUser.email = decoded.substr(j + 1, k - j - 1)
                this.localUser.picture = decoded.substr(k + 1, decoded.length - k)
                break
              }
            }
            break
          }
        }
        break
      }
    }
  }

  logout() {
    swal({
      title: 'Are you sure want to logout?',
      text: "You'll be automatically signed out!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.logout_obj.username = this.localUser.email
        this.logout_obj.password = undefined
        this.service.logout(this.logout_obj).then(
          res => {
            localStorage.clear()
            this.router.navigate(['/login'])
          },
          err => {
            if (err.http_status === 422) {
              this.alert.error_alert(err.message)
            } else {
              this.alert.warn_alert(err.message)
            }
          }
        )
      }
    })
  }

  focusField() {
    setTimeout(() => {
      $("#webpassword").focus();
    }, 500)
  }

  changePassword() {
    this.changePasswordForm.reset()
    this.initAdminData()
    this.focusField()
    $('#change-password').modal()
  }

}